#include "esp8266.h"
#include "uartwifi.h"
#include "SystemData.h"
#include "projectSettings.h"
#include "indicators.h"
#include "stm32f1xx_hal.h"
#include "stdbool.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

xTaskHandle xHandleTaskESP8266config, xHandleTaskCommandsFromServer, xHandleTaskSystemDataTransmiter;
extern UART_HandleTypeDef UART_DEBUG, UART_WIFI;
extern xSemaphoreHandle xSemaphoreComReceived;
extern uint8_t bufferESP8266commands[58];
extern SystemData_InitTypeDef SystemData;


void vStartESP8266Task(uint8_t Priority)
{
  vSemaphoreCreateBinary(xSemaphoreComReceived);

  	//Creating a task for flashing LedRED with f = 1Hz
	xTaskCreate(ESP8266config,"esp8266",configMINIMAL_STACK_SIZE,NULL,	///test
			Priority,&xHandleTaskESP8266config);
}

void ESP8266config(void *pvParameters)
{
  HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"Hello, we start AirVolt application!\r\n",38,5);
  HAL_GPIO_WritePin(WIFI_HW_RESET_PORT, WIFI_HW_RESET_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(WIFI_PORT, WIFI_SW_RESET_PIN, GPIO_PIN_SET);


  for(;;)
  {
  vTaskDelay(1000/portTICK_PERIOD_MS);
  ///////////////////////1 -> Check if there is communication with ESP8266////////////////////////
    while(ESP8266commands(CheckReady) != true)//keep reseting each 1 seconds
      {
      HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIGURATION->HARDWARE RESTART!!!\r\n",40,10);
      EPS8266hwReset();
      vTaskDelay(3000/portTICK_PERIOD_MS);
      }
  ///////////////////////2 -> //Set up module as client//////////////////////////////////////////
  ESP8266commands(SetAsClient);
  ///////////////////////3 -> //Check does IP address present//////////////////////////////////
  if(ESP8266commands(GetIP) == false)
  	  {
	  ///////////////////////3.1 -> //Join to predefined access point//////////////////////////////////
	  ESP8266commands(JoinAP);
	  ///////////////////////3.2 -> //Get and display it's IP adress//////////////////////////////////
	  ESP8266commands(GetIP);
  	  }
  ///////////////////////4 -> ////Deny multiple connections///////////////////////////////////
  ESP8266commands(MultipleConnections);
  /////////////////////////5 -> ////Open connection on ID=0, type=UDP, IP=broadcast, port=45454///
  ESP8266commands(OpenUDPbroadcast); //con ID 0
    
  xTaskCreate(vTaskESP8266Commands,"CommandsFromServer",configMINIMAL_STACK_SIZE,NULL,4,&xHandleTaskCommandsFromServer);
  vESP8266receiveITon();
  xTaskCreate(vTaskSendSystemData,"SystemDataTransmiter",configMINIMAL_STACK_SIZE,NULL,3,&xHandleTaskSystemDataTransmiter);

 vTaskDelete(NULL);
  }
}//end funtion

void vTaskSendSystemData(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
  bool OpenHostOnce = true;
	for(;;)
	{

	if(isIPempty(&SystemData.host0IP) == true)
		{
		/////////////////////////Open connection on ID=0, type=UDP, IP=broadcast, port=45454///
		HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"No Host IP\r\n",12,10);
		}
	else
		{
		HAL_UART_DMAStop(&UART_WIFI);
		HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"OK Host IP\r\n",12,10);
		/////////////////////////7 -> //////Send data on ID=0 of Name,IP adress, VolBatInt,VolBatNorm, VolSupInt,VolSupNorm,TemperatureCelc
		if (OpenHostOnce == true)
			{
			  ESP8266commands(OpenUDPhost0);
			  OpenHostOnce = false;
			}

		ESP8266commands(SendSystemData1);

		__HAL_UART_CLEAR_OREFLAG(&UART_WIFI);
		  HAL_UART_Receive_DMA(&UART_WIFI, bufferESP8266commands, 58);
		}
	vTaskDelayUntil(&xLastFlashTime,1000);
	}
}

void vTaskESP8266Commands(void * pvParameters)
{
  //creating semaphore
  int CommandNumber;
  char *pCommandNumber;
//that's the way to empty it,because right after creation is always full
//and cause problems
xSemaphoreTake(xSemaphoreComReceived,0);

//infinite loop
		for(;;)
		{
    //it goes further only when semaphore is set
    //when semaphore is not set than it awaits for given sempahore
    //from interrupt
		xSemaphoreTake(xSemaphoreComReceived,portMAX_DELAY);

		pCommandNumber = strchr((char*)bufferESP8266commands,'C') + 1;
		CommandNumber = *pCommandNumber - '0';
   
    switch(CommandNumber)
      {
        case 0://Host address
        {
          ipHost1_translator(pCommandNumber+2);
          sprintf((char*)bufferESP8266commands, "The host address is: %d.%d.%d.%d\r\n", SystemData.host0IP.firstByte,
          SystemData.host0IP.secondByte, SystemData.host0IP.therdByte, SystemData.host0IP.forthByte);
          HAL_UART_Transmit(&UART_DEBUG,bufferESP8266commands,strlen((char*)bufferESP8266commands),10);
          vESP8266receiveITon();
          break;
        }
        case 1://Cell thresholds adjust
        {
          char stringtofloat[4];
          //First threshold
          pCommandNumber = strchr((char*)bufferESP8266commands,'K') + 1;
          strncpy(stringtofloat,pCommandNumber,4);
          SystemData.fVthreshold1 = atof(stringtofloat);
          //Second threshold
          pCommandNumber = strchr((char*)bufferESP8266commands,'L') + 1;
          strncpy(stringtofloat,pCommandNumber,4);
          SystemData.fVthreshold2 = atof(stringtofloat);
          //Third threshold
          pCommandNumber = strchr((char*)bufferESP8266commands,'M') + 1;
          strncpy(stringtofloat,pCommandNumber,4);
          SystemData.fVthreshold3 = atof(stringtofloat);
          //Fourth threshold
          pCommandNumber = strchr((char*)bufferESP8266commands,'N') + 1;
          strncpy(stringtofloat,pCommandNumber,4);
          SystemData.fVthreshold4 = atof(stringtofloat);
          //Fivth threshold
          pCommandNumber = strchr((char*)bufferESP8266commands,'O') + 1;
          strncpy(stringtofloat,pCommandNumber,4);
          SystemData.fVthreshold5 = atof(stringtofloat);

          int threshold11 = SystemData.fVthreshold1;			 // Get the integer part (1)
          float thresholdf = SystemData.fVthreshold1 - threshold11;     // Get fractional part (1.15 - 1 = 0.15)
          int thresholdf12 = (int)(thresholdf * 10);				 // Turn into integer.

          int threshold21 = SystemData.fVthreshold2;			 // Get the integer part (1)
          thresholdf = SystemData.fVthreshold2 - threshold21;     // Get fractional part (1.15 - 1 = 0.15)
          int thresholdf22 = (int)(thresholdf * 10);				 // Turn into integer.

		int threshold31 = SystemData.fVthreshold3;			 // Get the integer part (1)
		thresholdf = SystemData.fVthreshold3 - threshold31;     // Get fractional part (1.15 - 1 = 0.15)
		int thresholdf32 = (int)(thresholdf * 10);				 // Turn into integer.

		int threshold41 = SystemData.fVthreshold4;			 // Get the integer part (1)
		thresholdf = SystemData.fVthreshold4 - threshold41;     // Get fractional part (1.15 - 1 = 0.15)
		int thresholdf42 = (int)(thresholdf * 10);				 // Turn into integer.

		int threshold51 = SystemData.fVthreshold5;			 // Get the integer part (1)
		thresholdf = SystemData.fVthreshold5 - threshold51;     // Get fractional part (1.15 - 1 = 0.15)
		int thresholdf52 = (int)(thresholdf * 10);				 // Turn into integer.

          sprintf((char*)bufferESP8266commands, "Thresholds are: %d.%d,%d.%d,%d.%d,%d.%d,%d.%d\r\n",
        		  threshold11,thresholdf12,threshold21,thresholdf22,threshold31,thresholdf32,
        		  threshold41,thresholdf42,threshold51,thresholdf52);
          HAL_UART_Transmit(&UART_DEBUG,bufferESP8266commands,strlen((char*)bufferESP8266commands),40);
            vESP8266receiveITon();
          break;
        }
        default:
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"ERROR->Command not interpreated!\r\n",34,10);
        break;
        }
      }//end switch-> case loop 
     vESP8266receiveITon();
    }//end for loop
}//end function


bool EPS8266hwReset(void)
{
  	HAL_GPIO_WritePin(WIFI_HW_RESET_PORT, WIFI_HW_RESET_PIN, GPIO_PIN_SET);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    HAL_GPIO_WritePin(WIFI_HW_RESET_PORT, WIFI_HW_RESET_PIN, GPIO_PIN_RESET);
    
  if(HAL_GPIO_ReadPin(WIFI_HW_RESET_PORT,WIFI_HW_RESET_PIN) == GPIO_PIN_RESET)
    {
    return true;
    }
  else
    {
    return false;
    } 
}

bool EPS8266swReset(void)
{
  	HAL_GPIO_WritePin(WIFI_PORT, WIFI_SW_RESET_PIN, GPIO_PIN_RESET);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    HAL_GPIO_WritePin(WIFI_PORT, WIFI_SW_RESET_PIN, GPIO_PIN_SET);
    
  if(HAL_GPIO_ReadPin(WIFI_PORT,WIFI_SW_RESET_PIN) == GPIO_PIN_SET)
    {
    return true;
    }
  else
    {
    return false;
    } 
}
