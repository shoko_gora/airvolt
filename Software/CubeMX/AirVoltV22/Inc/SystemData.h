#ifndef _SYSTEMDATA_H
#define _SYSTEMDATA_H	1

typedef enum
{
 Off		  =0x00,
 On			  =0x01,
}OnOff_TypeDef;


typedef enum
{
 NotStarted		=0x00,
 Running	  	=0x01,
 Stopped	  	=0x02,
 Completed		=0x03,
}ProgramStatus_TypeDef;

typedef struct
{
 unsigned char firstByte;
 unsigned char secondByte; 
 unsigned char therdByte;
 unsigned char forthByte;
}IPaddress_TypeDef;

typedef struct
{
//Always updated system info
float fVcell;
float fVsupply;					
float fVblockNr;					
float fVtensNr;					
float fVentityNr;					
float fVtemp;					
float fVref;				
float fQ;
float fspare0;
float fspare1;
float fVthreshold1;
float fVthreshold2;
float fVthreshold3;
float fVthreshold4;
float fVthreshold5;
float fVrefPinVal;
unsigned int iVcell;
unsigned int iVsupply;					
unsigned int iVblockNr;	
unsigned char cBlockNr;  
unsigned int iVtensNr;					
unsigned int iVentityNr;					
unsigned int iVtemp;					
unsigned int iVref;		
unsigned int iVrefPinVal;
unsigned int iQ;
unsigned int ispare0;
unsigned int ispare1;
unsigned int iVthreshold1;
unsigned int iVthreshold2;
unsigned int iVthreshold3;
unsigned int iVthreshold4;
unsigned int iVthreshold5;

ProgramStatus_TypeDef ProgramStatus;
//Options
OnOff_TypeDef USBstate; 			//USB on or off
OnOff_TypeDef Buz;				//Buzzer state
unsigned char cDay;
unsigned char cMonth;
unsigned char cYear;
unsigned char cDayOfTheWeek;
unsigned char cSeconds;
unsigned char cMinuts;
unsigned char cHours;
unsigned char cspare0;
unsigned char cspare1;

IPaddress_TypeDef myIP;
IPaddress_TypeDef host0IP;
IPaddress_TypeDef host1IP;
IPaddress_TypeDef host2IP;
}SystemData_InitTypeDef;


void SystemDataInitialize(void);
unsigned char tellMeTensID(void);
unsigned char tellMeEntityID(void);



#endif //SYSTEMDATA_H_
