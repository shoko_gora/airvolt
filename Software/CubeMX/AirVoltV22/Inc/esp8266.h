//ESP8266 library for STM32 CubeMX
//22.09.2015

//              STM32F103VG
//            -----------------
//           |                 |
//           |             PB9 |-->WiFi Reset

#ifndef ESP8266_H_
#define ESP8266_H_


#include <stdint.h>
#include <stdbool.h>


void vStartESP8266Task(uint8_t Priority);

void ESP8266config(void *pvParameters);
void vTaskSendSystemData(void * pvParameters);
void vTaskESP8266Commands(void * pvParameters);

bool EPS8266hwReset(void);
bool EPS8266swReset(void);


#endif //ESP8266
