//UART library like LEDs,buzzer,etc.
//13.09.2015
//              STM32F103VG
//            -----------------
//           |                 |
//           |                 |
//           |             PA9 |-->TX
//           |             PB10|<--RX


#ifndef UARTDEBUG_H_
#define UARTDEBUG_H_

#include <stdint.h>

/* Define the structure type that will be passed on the queue. */

typedef enum
{
 SendLogo1,
 SendLogo2,
 SendMeasurements,
 SendSystemData,
 PassChar
}DebugCommand_TypeDef;

typedef struct
{
DebugCommand_TypeDef command;
uint8_t cValue1;
}xUARTdebug;


void vStartUARTdebugTask(uint8_t Priority);

void vTaskUARTdebug(void *pvParameters);
void vhSend_Data(uint8_t *data, uint16_t size);
void vhDebugSendChar(uint8_t *character);
















#endif //UARTDEBUG_H_
