//UART library like LEDs,buzzer,etc.
//13.09.2015
//              STM32F103VG
//            -----------------
//           |                 |
//           |                 |
//           |             PA2 |-->TX
//           |             PA3 |<--RX


#ifndef UARTWIFI_H_
#define UARTWIFI_H_

#include <stdint.h>
#include <stdbool.h>
#include "SystemData.h"
/* Define the structure type that will be passed on the queue. */

typedef enum
{
 CheckReady,                          //Check is module available and ready (timeout 100ms)     
 SetAsClient,                        //Set up module as client
 JoinAP,                            //Join to predefined access point-> please wait abbut 5 sec. Please refer to project settings
 GetIP,                           //Ask for IP adress and and use it for Broadcast
 isConnected,                     //check if is already connected to the network
 MultipleConnections,                      //Accept multiple connections
 OpenUDPbroadcast,               //Send Broadcast message, e.g. <A1>,<192.168.43.62>,<1484>,<1,21V>,<4048>,<3.3V>,<23,1>
 OpenUDPhost0,
 OpenTCPhost0,
 SendSystemData1,
 CloseUDP,
 CloseTCP,
 PassCharacter,                 //Passes character from one UART2->RX to UART1->TX
}WiFiCommand_TypeDef;

typedef struct
{
WiFiCommand_TypeDef command;
uint8_t cValue1;
uint8_t *cpValue2;
}xUARTwifi;


bool ESP8266commands(uint8_t command);

bool vESP8266receiveITon(void);


unsigned int ip_to_int (const char * ip);
unsigned int ipHost1_translator(const char * ip);
bool isIPempty(IPaddress_TypeDef* IPtoCheck);



#endif //UARTWIFI_H_
