//Indicators library like LEDs,buzzer,etc.
//08.09.2015
//              STM32F103VG
//            -----------------
//           |                 |
//           |                 |
//           |             PB11|-->LED GREEN
//           |             PB10|-->LED ORANGE
//           |              PB8|-->LED RED
//           |              PB2|-->LED WiFi
//           |              PB0|-->Buzzer

#ifndef INDICATORS_H_
#define INDICATORS_H_

#include <stdint.h>




#define IND_PORT	GPIOB
#define GREEN			GPIO_PIN_11
#define ORANGE		GPIO_PIN_10
#define RED				GPIO_PIN_12
#define WiFi			GPIO_PIN_13
#define Buzzer		GPIO_PIN_0


#define BatIndUpdateBackUp 	0x00
#define BatIndReadBackUp 		0x01
#define BatIndStart	  	  	0x02
#define BatIndStop					0x03


//void vhIndicatorsInitialize(void);

void vStartLEDTasksTest(uint8_t Priority);
void vKillLEDTasksTest(void);
void vBatInd(uint8_t task, uint8_t Priority);
void vTaskBatInd(void * pvParameters);
void vTaskBatIndSimple(void * pvParameters);

void vhLedGREEN_on(void);
void vhLedGREEN_off(void);
void vhLedGREEN_toggle(void);

void vhLedORANGE_on(void);
void vhLedORANGE_off(void);
void vhLedORANGE_toggle(void);

void vhLedRED_on(void);
void vhLedRED_off(void);
void vhLedRED_toggle(void);

void vhLedWiFi_on(void);
void vhLedWiFi_off(void);
void vhLedWiFi_toggle(void);

#endif //INDICATORSF_H_
