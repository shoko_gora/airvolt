#include "uartwifi.h"
#include "uartdebug.h"
#include "indicators.h"
#include "stm32f1xx_hal.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "SystemData.h"
#include "adc.h"
#include "projectSettings.h"


/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

extern UART_HandleTypeDef UART_DEBUG, UART_WIFI;
extern SystemData_InitTypeDef SystemData;
//uint8_t quickChar;
//extern xQueueHandle xQueueUARTdebug;

static uint8_t commandString[60] = "AT\r\n";
static uint8_t expectedAnswer[20] = "OK";
static uint8_t answer[65];
static uint8_t bufferSysData[58];
uint8_t bufferESP8266commands[58];

xSemaphoreHandle xSemaphoreComReceived;

bool ESP8266commands(uint8_t command)
{
	switch (command)
		{
		case CheckReady:
			{
        memset(commandString,0,strlen((char*)commandString));
        memset(expectedAnswer,0,strlen((char*)expectedAnswer));
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)commandString, "AT\r\n");
        sprintf((char*)expectedAnswer, "OK");

        //strcpy((char*)commandString, "AT\r\n");
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->CHECK AT RESPONSE-> ",27,10);
        HAL_UART_Transmit(&UART_DEBUG,commandString,4,10);
        HAL_UART_Transmit(&UART_WIFI,commandString, 4,10);
        HAL_UART_Receive(&UART_WIFI,answer,10,1000);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
        {
        vhLedWiFi_on();
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->CHECK AT RESPONSE-> SUCCESSFUL!!!\r\n",43,10);
        return true;
        }
        else
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->CHECK AT RESPONSE-> ERROR!!!\r\n",38,10);
        vhLedWiFi_off();
        return false;
        }
			}//end case of CheckReady
		case SetAsClient:
			{
        memset(commandString,0,strlen((char*)commandString));
        memset(expectedAnswer,0,strlen((char*)expectedAnswer));
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)commandString, "AT+CWMODE=1\r\n");
        sprintf((char*)expectedAnswer, "OK");
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->EPS8266 AS CLIENT-> ",28,10);
        HAL_UART_Transmit(&UART_DEBUG,commandString,13,10);
        HAL_UART_Transmit(&UART_WIFI,commandString, 13,10);
        HAL_UART_Receive(&UART_WIFI,answer,sizeof(answer), 300);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
        {
        vhLedWiFi_on();
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->EPS8266 AS CLIENT-> SUCCESSFUL!!!\r\n",43,10);
        return true;
        }
        else
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->EPS8266 AS CLIENT-> ERROR!!!\r\n",38,10);
        vhLedWiFi_off();
        return false;
        }        
			}//end of case SetAsClient
		case JoinAP:
			{
        memset(commandString,0,strlen((char*)commandString));
        memset(expectedAnswer,0,strlen((char*)expectedAnswer));
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)commandString, "AT+CWJAP=\"%s\",\"%s\"\r\n",WiFi_SSID,WiFi_PASSWORD);
        sprintf((char*)expectedAnswer, "OK");
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->SSID AND PASSWORD-> ",28,10);
        HAL_UART_Transmit(&UART_DEBUG,commandString, 36,10);
        HAL_UART_Transmit(&UART_WIFI,commandString, 36,10);
        HAL_UART_Receive(&UART_WIFI,answer,42,6000);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->SSID AND PASSWORD-> SUCCESSFUL!!!\r\n",43,10);
        vhLedWiFi_on();
        return true;
        }
        else
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->SSID AND PASSWORD-> ERROR!!!\r\n",43,10);
        vhLedWiFi_off();
        return false;
        }        
			}
		case GetIP:
			{
        memset(commandString,0,strlen((char*)commandString));
        memset(expectedAnswer,0,strlen((char*)expectedAnswer));
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)commandString, "AT+CIPSTA?\r\n");
        sprintf((char*)expectedAnswer, "+CIPSTA:\"");
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->GET IP-> ",17,10);
        HAL_UART_Transmit(&UART_DEBUG,commandString,12,10);
        HAL_UART_Transmit(&UART_WIFI,commandString, 12,10);
        HAL_UART_Receive(&UART_WIFI,answer,37,1000);
        //Check if in the answer there is string called "+CPISTA" if yes then carry on
        if(strstr((char*)answer, (char*)expectedAnswer))
          {
            char *pFirstFound = strchr((char*)answer,'\"') + 1;
            char *pLastFound = strrchr((char*)answer,'\"');
            uint8_t IPadress[pLastFound - pFirstFound];
            IPadress[0] = *pFirstFound;
            strncpy((char*)IPadress, pFirstFound, sizeof(IPadress));                      
            uint32_t iIPadress = ip_to_int((char*)IPadress);
            if(iIPadress != 0)
            	{
				HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->GET IP-> ",17,10);
				sprintf((char*)commandString, "%d.%d.%d.%d\r\n", SystemData.myIP.firstByte,
				SystemData.myIP.secondByte, SystemData.myIP.therdByte, SystemData.myIP.forthByte);
				HAL_UART_Transmit(&UART_DEBUG,commandString,strlen((char*)commandString),10);
				HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->GET IP-> SUCCESSFUL!!!\r\n",32,10);
				vhLedWiFi_on();
				return true;
            	}
            else
            	{
            	return false;
            	}
          }
        else
          {
            HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->GET IP-> ERROR!!!\r\n",32,10);
            vhLedWiFi_off();
           	return false;
          }       
			}
    case MultipleConnections:
			{				
        memset(commandString,0,strlen((char*)commandString));
        memset(expectedAnswer,0,strlen((char*)expectedAnswer));
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)commandString, "AT+CIPMUX=1\r\n");
        sprintf((char*)expectedAnswer, "OK");
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->MULTI CONNECTIONS> ",27,10);
        HAL_UART_Transmit(&UART_DEBUG,commandString,strlen((char*)commandString),10);
        HAL_UART_Transmit(&UART_WIFI,commandString, 13,10);
        HAL_UART_Receive(&UART_WIFI,answer,19,100);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
          {
          HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->MULTI CONNECTIONS-> SUCCESSFUL!!!\r\n",43,10);
          vhLedWiFi_on();
          return true;
          }
        else
          {
          HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"CONFIG->MULTI CONNECTIONS-> ERROR!!!\r\n",38,10);
          vhLedWiFi_off();
          return false;
          }    
			}
		case OpenUDPbroadcast:
			{
      memset(commandString,0,strlen((char*)commandString));
      memset(expectedAnswer,0,strlen((char*)expectedAnswer));
      memset(answer,0,strlen((char*)answer));
      HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP BC-> ",24,10);
      //Open connection on ID=0, type=UDP, IP=broadcast, port=45454 
      sprintf((char*)commandString, "AT+CIPSTART=0,\"UDP\",\"%d.%d.%d.255\",%d,%d,0\r\n",
      SystemData.myIP.firstByte, SystemData.myIP.secondByte, SystemData.myIP.therdByte,(int)WiFi_BC_PORT,(int)WiFi_BC_PORT);
      sprintf((char*)expectedAnswer, "CONNECT");
      HAL_UART_Transmit(&UART_DEBUG,commandString, strlen((char*)commandString),10);

      HAL_UART_Transmit(&UART_WIFI,commandString, strlen((char*)commandString),100);
      HAL_UART_Receive(&UART_WIFI,answer,56, 300);
      //HAL_UART_Transmit(&UART_DEBUG,answer,strlen((char*)answer),10);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP BC-> SUCCESSFUL!!!\r\n",39,10);
        vhLedWiFi_on();
        return true;
        }
        else
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP BC-> ERROR!!!\r\n",34,10);
        vhLedWiFi_off();
        return false;
        }
			}
      case OpenUDPhost0:
			{
      memset(commandString,0,strlen((char*)commandString));
      memset(expectedAnswer,0,strlen((char*)expectedAnswer));
      memset(answer,0,strlen((char*)answer));
      HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP HOST-> ",26,10);
      //Open connection on ID=0, type=UDP, IP=broadcast, port=45454 
      sprintf((char*)commandString, "AT+CIPSTART=1,\"UDP\",\"%d.%d.%d.%d\",%d,%d,0\r\n",
      SystemData.host0IP.firstByte, SystemData.host0IP.secondByte, SystemData.host0IP.therdByte,SystemData.host0IP.forthByte,
      (int)WiFi_HOST_PORT,(int)WiFi_HOST_PORT);
      sprintf((char*)expectedAnswer, "CONNECT");
      HAL_UART_Transmit(&UART_DEBUG,commandString, strlen((char*)commandString),10);
      HAL_UART_Transmit(&UART_WIFI,commandString, strlen((char*)commandString),100);
      HAL_UART_Receive(&UART_WIFI,answer,65, 100);

        //HAL_UART_Receive_IT(&UART_WIFI, bufferESP8266commands, 58);

      HAL_UART_Transmit(&UART_DEBUG,answer,strlen((char*)answer),10);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP HOST-> SUCCESSFUL!!!\r\n",41,10);
        vhLedWiFi_on();
        return true;
        }
        else
        {
        HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->OPEN UDP HOST-> ERROR!!!\r\n",36,10);
        vhLedWiFi_off();
        return false;
        }
			}
    case SendSystemData1:
      {
      unsigned char commandStringLength = 0;
      memset(commandString,0,strlen((char*)commandString));
      memset(expectedAnswer,0,strlen((char*)expectedAnswer));
      memset(answer,0,strlen((char*)answer));
      int d1 = SystemData.fVcell;			 // Get the integer part (1)
      float f2 = SystemData.fVcell - d1;     // Get fractional part (1.15 - 1 = 0.15)
      int d2 = (int)(f2 * 100);				 // Turn into integer.

      sprintf((char*)bufferSysData, "<ID>%c%d%d<IP>%d.%d.%d.%d<Vbat>%d.%02dV\r\n",SystemData.cBlockNr, tellMeTensID(),
		tellMeEntityID(), SystemData.myIP.firstByte, SystemData.myIP.secondByte, SystemData.myIP.therdByte, SystemData.myIP.forthByte,
		d1,d2);
      commandStringLength = strlen((char*)bufferSysData);
      sprintf((char*)commandString, "AT+CIPSEND=1,%d\r\n", commandStringLength-2);
      sprintf((char*)expectedAnswer, ">");
      HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->SEND SYS DATA-> ",26,10);
      HAL_UART_Transmit(&UART_DEBUG,commandString, strlen((char*)commandString),10);
      HAL_UART_Transmit(&UART_WIFI,commandString, strlen((char*)commandString),10);
      HAL_UART_Receive(&UART_WIFI,answer,30,50);

        memset(expectedAnswer,0,strlen((char*)expectedAnswer)); 
        memset(answer,0,strlen((char*)answer));
        sprintf((char*)expectedAnswer, "SEND OK");
        HAL_UART_Transmit(&UART_WIFI,bufferSysData, commandStringLength,10);
        HAL_UART_Receive(&UART_WIFI,answer,36,500);
        if (strstr((char*)answer, (char*)expectedAnswer)!=0)
          {
          HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->SEND SYS DATA-> SUCCESSFUL!!!\r\n",43,10);
          vhLedWiFi_on();
          return true;
          }
          else
          {
          HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)"TRANSMIT->SEND SYS DATA-> ERROR, NO SEND OK\r\n",47,10);
          vhLedWiFi_off();
          return false;
          }
      }
		default:
			{
			break;
			}
		}//end switch case
  return false;
}//end function

unsigned int ip_to_int (const char * ip)
{
    /* The return value. */
    unsigned v = 0;
    /* The count of the number of bytes processed. */
    int i;
    /* A pointer to the next digit to process. */
    const char * start;

    start = ip;
    for (i = 0; i < 4; i++) 
        {
        /* The digit being processed. */
        char c;
        /* The value of this byte. */
        int n = 0;
        while (1) 
          {
            c = * start;
            start++;
            if (c >= '0' && c <= '9') 
              {
              n *= 10;
              n += c - '0';
              }
            /* We insist on stopping at "." if we are still parsing
               the first, second, or third numbers. If we have reached
               the end of the numbers, we will allow any character. */
            else if ((i < 3 && c == '.') || i == 3) 
              {
                break;
              }
            else 
              {
                return false;
              }
          }
        if (n >= 256) 
          {
            return false;
          }
        v *= 256;
        v += n;
          
        //Save digits to SystemData
        switch(i)
          {
          case 0:
            {
            SystemData.myIP.firstByte = n;
            break;
            }
          case 1:
            {
            SystemData.myIP.secondByte = n;
            break;
            }
          case 2:
            {
            SystemData.myIP.therdByte = n;
            break;
            }
          case 3:
            {
            SystemData.myIP.forthByte = n;
            break;
            }
          default:
            {
            break;
            }
          }//end of switch->case for SystemData
        }
    return v;
}

unsigned int ipHost1_translator(const char * ip)
{
    /* The return value. */
    unsigned v = 0;
    /* The count of the number of bytes processed. */
    int i;
    /* A pointer to the next digit to process. */
    const char * start;

    start = ip;
    for (i = 0; i < 4; i++) 
        {
        /* The digit being processed. */
        char c;
        /* The value of this byte. */
        int n = 0;
        while (1) 
          {
            c = * start;
            start++;
            if (c >= '0' && c <= '9') 
              {
              n *= 10;
              n += c - '0';
              }
            /* We insist on stopping at "." if we are still parsing
               the first, second, or third numbers. If we have reached
               the end of the numbers, we will allow any character. */
            else if ((i < 3 && c == '.') || i == 3) 
              {
                break;
              }
            else 
              {
                return false;
              }
          }
        if (n >= 256) 
          {
            return false;
          }
        v *= 256;
        v += n;
          
        //Save digits to SystemData
        switch(i)
          {
          case 0:
            {
            SystemData.host0IP.firstByte = n;
            break;
            }
          case 1:
            {
            SystemData.host0IP.secondByte = n;
            break;
            }
          case 2:
            {
            SystemData.host0IP.therdByte = n;
            break;
            }
          case 3:
            {
            SystemData.host0IP.forthByte = n;
            break;
            }
          default:
            {
            break;
            }
          }//end of switch->case for SystemData
        }
    return v;
}

bool isIPempty(IPaddress_TypeDef* IPtoCheck)
{
  if(IPtoCheck->firstByte == 0x00 || IPtoCheck->secondByte == 0x00 || IPtoCheck->therdByte == 0x00 || IPtoCheck->forthByte == 0x00)
    {
    return true;
    }
  else 
    {
    return false;
    }
}

bool vESP8266receiveITon(void)
{
    __HAL_UART_FLUSH_DRREGISTER(&UART_WIFI);
  return HAL_UART_Receive_DMA(&UART_WIFI, bufferESP8266commands, 58);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{ 
  HAL_UART_Transmit(&UART_DEBUG,(uint8_t*)bufferESP8266commands,58,100);
  __HAL_UART_FLUSH_DRREGISTER(&UART_WIFI);
  HAL_UART_Receive_DMA(&UART_WIFI, bufferESP8266commands, 58);

   xSemaphoreGiveFromISR(xSemaphoreComReceived, NULL);
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    __HAL_UART_FLUSH_DRREGISTER(&UART_WIFI);
    HAL_UART_Receive_DMA(&UART_WIFI, bufferESP8266commands, 58);
}

