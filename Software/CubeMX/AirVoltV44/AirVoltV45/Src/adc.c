/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "projectSettings.h"

//My libraries
#include "SystemData.h"
#include <stdbool.h>
#include "adc.h"
#include "stm32f1xx_hal.h"
#include "uartdebug.h"

//ADC handler
extern ADC_HandleTypeDef hadc1;
//System Data 
extern SystemData_InitTypeDef SystemData;

xUARTdebug xUARTdataToSend;

//freeRTOS variable declaration
xTaskHandle xHandleTaskADC;
extern xQueueHandle xQueueUARTdebug;

void vStartADCTask(uint8_t Priority)
{
xTaskCreate(vADCTask,"ADC",512,NULL,
			Priority,&xHandleTaskADC);
 xTaskCreate(vStartWhoIam,"WhoIam",configMINIMAL_STACK_SIZE,NULL,
			Priority,NULL); 
  
}


void vADCTask(void * pvParameters)
{
const uint16_t V25 = 1750;// when V25=1.41V at ref 3.3V
const uint16_t Avg_Slope = 5; //when avg_slope=4.3mV/C at ref 3.3V

uint16_t TemperatureC;
uint16_t temp;
uint8_t channelnumber = 0;
uint8_t cyclenumber = 0;
uint16_t ADCconversions[7][ADC_CYCLES];

HAL_ADCEx_Calibration_Start(&hadc1);
/* Infinite loop */
for(;;)
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	for(cyclenumber = 0; cyclenumber < ADC_CYCLES; cyclenumber++)
		{

		for(channelnumber = 0; channelnumber<7; channelnumber ++)
			{
				HAL_ADC_Start(&hadc1);
					if (HAL_ADC_PollForConversion(&hadc1, 1000000) == HAL_OK)
							{
							temp = HAL_ADC_GetValue(&hadc1);
							}
			switch(channelnumber)
							{
							case 0:
									{
									ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVcell = temp;
									//SystemData.fVcell = ((temp*SystemData.fVrefPinVal)/4096);
									break;
									}
							case 1:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVsupply = temp;
									//SystemData.fVsupply = ((temp*SystemData.fVrefPinVal)/4096);
									break;
									}
							case 2:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVblockNr = temp;
									//SystemData.fVblockNr = ((temp*SystemData.fVrefPinVal)/4096);
									break;
									}
							case 3:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVtensNr = temp;
									//SystemData.fVtensNr = ((temp*SystemData.fVrefPinVal)/4096);
									break;
									}
							case 4:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVentityNr = temp;
									//SystemData.fVentityNr = ((temp*SystemData.fVrefPinVal)/4096);
									break;
									}
							case 5:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//TemperatureC = (uint16_t)(((V25-temp)/Avg_Slope)+25);
									//SystemData.iVtemp = temp;
									//SystemData.fVtemp = TemperatureC;
									break;
									}
							case 6:
									{
										ADCconversions[channelnumber][cyclenumber] = temp;
									//SystemData.iVref = temp;
									//SystemData.fVref = ((temp*SystemData.fVrefPinVal)/4096);
					#ifdef SEND_MEASURES
									xUARTdataToSend.command = SendMeasurements;
									xQueueSendToBack(xQueueUARTdebug, &xUARTdataToSend, 10 );
					#endif
									HAL_ADC_Stop(&hadc1);
									break;
									}
							default:
									{
									break;
									}
							}//end switch->case
				}//end channel loop
		}//end cycle loop

	for(channelnumber = 0; channelnumber<7; channelnumber ++)
				{
				switch(channelnumber)
						{
									case 0:
									{
										SystemData.iVcell = vADC_Mediana(&ADCconversions[channelnumber][0],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVcell = ((SystemData.iVcell*SystemData.fVrefPinVal)/4096);
										break;
									}
									case 1:
									{
										SystemData.iVsupply = vADC_Mediana(&ADCconversions[channelnumber][1],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVsupply = ((SystemData.iVsupply*SystemData.fVrefPinVal)/4096);
										break;
									}
									case 2:
									{
										SystemData.iVblockNr = vADC_Mediana(&ADCconversions[channelnumber][2],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVblockNr = ((SystemData.iVblockNr*SystemData.fVrefPinVal)/4096);
										break;
									}
									case 3:
									{
										SystemData.iVtensNr = vADC_Mediana(&ADCconversions[channelnumber][3],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVtensNr = ((SystemData.iVtensNr*SystemData.fVrefPinVal)/4096);
										break;
									}
									case 4:
									{
										SystemData.iVentityNr = vADC_Mediana(&ADCconversions[channelnumber][4],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVentityNr = ((SystemData.iVentityNr*SystemData.fVrefPinVal)/4096);
										break;
									}
									case 5:
									{
										temp = vADC_Mediana(&ADCconversions[channelnumber][5],MEDIANA_REJECT,ADC_CYCLES);
										TemperatureC = (uint16_t)(((V25-temp)/Avg_Slope)+25);
										SystemData.iVtemp = temp;
										SystemData.fVtemp = TemperatureC;
										break;
									}
									case 6:
									{
										SystemData.iVref = vADC_Mediana(&ADCconversions[channelnumber][6],MEDIANA_REJECT,ADC_CYCLES);
										SystemData.fVref = ((SystemData.iVref*SystemData.fVrefPinVal)/4096);
										break;
									}
									default:
									{
									break;
									}

						}
				}

vTaskDelayUntil( &xLastWakeTime, (1000/ portTICK_RATE_MS ) );
}//end task loop
}//end of function


/**
* @brief Sort the N ADC samples
* @param ADC samples to be sorted
* @param Numbre of ADC samples to be sorted
* @retval None
*/
void vSort_tab(uint16_t *tab, uint8_t lenght)
{
uint8_t l=0x00, exchange =0x01;
uint16_t tmp=0x00;

/* Sort tab */
while(exchange==1)
{
exchange=0;
for(l=0; l<lenght-1; l++)
{
if( tab[l] > tab[l+1] )
{
tmp = tab[l];
tab[l] = tab[l+1];
tab[l+1] = tmp;
exchange=1;
}
}
}
}

/**
 * @brief Get median
 * @param ADC samples to be sorted
 * @param Numbre of ADC samples to be rejected
 * @retval The average value
*/
uint16_t vADC_Mediana(uint16_t *tab , uint8_t X, uint8_t lenght)
{
	 uint32_t avg_sample =0x00;
	 uint8_t index=0x00;
	vSort_tab(tab,lenght);
	 // Add the N ADC samples
	 for (index=X/2; index<lenght-X/2; index++)
	 {
	 avg_sample += tab[index];
	 }
	 avg_sample /= lenght-X;
	 return avg_sample;

}


void vStartWhoIam(void * pvParameters)
{
	portTickType xLastFlashTime;
	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
#ifdef POTREVERSE
/////////////////////////////Block identification//////////////////////////////////
		if(SystemData.iVblockNr <= 1024)
		{
		SystemData.cBlockNr = 'D';
		}
		else if((SystemData.iVblockNr > 1024)&& (SystemData.iVblockNr <= 2048))
		{
		SystemData.cBlockNr = 'C';
		}
		else if((SystemData.iVblockNr > 2048)&& (SystemData.iVblockNr <= 3072))
		{
		SystemData.cBlockNr = 'B';
		}
		else if(SystemData.iVblockNr > 3072)
		{
		SystemData.cBlockNr = 'A';
		}
		else
		{
    SystemData.cBlockNr = 'Z';
		}
#else
		/////////////////////////////Block identification//////////////////////////////////
		if(SystemData.iVblockNr <= 1024)
		{
		SystemData.cBlockNr = 'A';
		}
		else if((SystemData.iVblockNr > 1024)&& (SystemData.iVblockNr <= 2048))
		{
		SystemData.cBlockNr = 'B';
		}
		else if((SystemData.iVblockNr > 2048)&& (SystemData.iVblockNr <= 3072))
		{
		SystemData.cBlockNr = 'C';
		}
		else if(SystemData.iVblockNr > 3072)
		{
		SystemData.cBlockNr = 'D';
		}
		else
		{
	SystemData.cBlockNr = 'Z';
		}
#endif
    
		vTaskDelayUntil(&xLastFlashTime,1000);
	}
}





