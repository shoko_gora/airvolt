#include "indicators.h"
#include "stm32f1xx_hal.h"
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

//My libraries
#include "SystemData.h"
#include <stdint.h>

extern SystemData_InitTypeDef SystemData;


xTaskHandle xHandleTaskLedGREEN, xHandleTaskLedORANGE, xHandleTaskLedRED, xHandleTaskLedWiFi;
xTaskHandle xHandleTaskBatInd;

/////////////////////////////////////GREEN//////////////////////////////
void vhLedGREEN_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, GREEN, GPIO_PIN_SET);
}

void vhLedGREEN_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, GREEN, GPIO_PIN_RESET);
}
void vhLedGREEN_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, GREEN);
}

/////////////////////////////////////ORANGE//////////////////////////////
void vhLedORANGE_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, ORANGE, GPIO_PIN_SET);
}

void vhLedORANGE_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, ORANGE, GPIO_PIN_RESET);
}
void vhLedORANGE_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, ORANGE);
}

/////////////////////////////////////RED//////////////////////////////
void vhLedRED_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, RED, GPIO_PIN_SET);
}

void vhLedRED_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, RED, GPIO_PIN_RESET);
}
void vhLedRED_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, RED);
}
/////////////////////////////////////BLUE//////////////////////////////
void vhLedWiFi_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, WiFi, GPIO_PIN_SET);
}

void vhLedWiFi_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, WiFi, GPIO_PIN_RESET);
}
void vhLedWiFi_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, WiFi);
}

////////////////////////////////////////////////////RTOS//////////////////////////////////////
void vTaskLedGREEN(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,3000);
		vhLedGREEN_toggle();
	}
}

void vTaskLedORANGE(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,1000);
		vhLedORANGE_toggle();
	}
}
void vTaskLedRED(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,500);
		vhLedRED_toggle();
	}
}
void vTaskLedWiFi(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,1000);
		vhLedWiFi_toggle();
	}
}


void vStartLEDTasksTest(uint8_t Priority)
{

	//Creating a task for flashing LedRED with f = 1Hz
	xTaskCreate(vTaskLedGREEN,"LedGREEN",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedGREEN);
	//Creating a task for flashing LedRED with f = 2Hz
	xTaskCreate(vTaskLedORANGE,"LedORANGE",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedORANGE);
	//Creating a task for flashing LedRED with f = 5Hz
	xTaskCreate(vTaskLedRED,"LedRED",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedRED);
	//Creating a task for flashing LedRED with f = 10Hz
	xTaskCreate(vTaskLedWiFi,"LedWiFi",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedWiFi);
}

void vKillLEDTasksTest(void)
{
vTaskDelete(xHandleTaskLedGREEN);
vTaskDelete(xHandleTaskLedORANGE);
vTaskDelete(xHandleTaskLedRED);
vTaskDelete(xHandleTaskLedWiFi);
}

void vBatInd(uint8_t task, uint8_t Priority)
{
	switch(task)
	{
		case BatIndStart:
		{
			xTaskCreate(vTaskBatIndSimple,"BatInd",configMINIMAL_STACK_SIZE,NULL,Priority,&xHandleTaskBatInd);
			break;
		}
		case BatIndStop:
		{
			vTaskDelete(xHandleTaskBatInd);
			break;
		}
		default:
		{
		}
		
	}
}
void vTask3xBlink(void * pvParameters)
{
	//3 x times blink to indicate the message has benn received
}



void vTaskBatInd(void * pvParameters)
{
	portTickType xLastFlashTime;
	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		if(SystemData.fVcell > SystemData.fVthreshold1)
		{
		vhLedGREEN_on();
		vhLedORANGE_on();
		vhLedRED_on();
		}
		else if((SystemData.fVcell < SystemData.fVthreshold1)&& (SystemData.fVcell > SystemData.fVthreshold2))
		{
		vhLedGREEN_toggle();
		vhLedORANGE_on();
		vhLedRED_on();
		}
		else if((SystemData.fVcell <= SystemData.fVthreshold2)&& (SystemData.fVcell > SystemData.fVthreshold3))
		{
		vhLedGREEN_off();
		vhLedORANGE_on();
		vhLedRED_on();
		}
		else if((SystemData.fVcell <= SystemData.fVthreshold3)&& (SystemData.fVcell > SystemData.fVthreshold4))
		{
		vhLedGREEN_off();
		vhLedORANGE_toggle();
		vhLedRED_on();
		}
		else if((SystemData.fVcell <= SystemData.fVthreshold4)&& (SystemData.fVcell > SystemData.fVthreshold5))
		{
		vhLedGREEN_off();
		vhLedORANGE_off();
		vhLedRED_on();
		}
		else if(SystemData.fVcell <= SystemData.fVthreshold5)
		{
		vhLedGREEN_off();
		vhLedORANGE_off();
		vhLedRED_toggle();
		}
		vTaskDelayUntil(&xLastFlashTime,500);
		
	}
}

void vTaskBatIndSimple(void * pvParameters)
{
	portTickType xLastFlashTime;
	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		if(SystemData.fVcell > SystemData.fVthreshold1)
		{
		vhLedGREEN_on();
		vhLedORANGE_on();
		vhLedRED_on();
		}
		else if((SystemData.fVcell < SystemData.fVthreshold1)&& (SystemData.fVcell > SystemData.fVthreshold2))
		{
		vhLedGREEN_off();
		vhLedORANGE_on();
		vhLedRED_on();
		}
		else if((SystemData.fVcell <= SystemData.fVthreshold2)&& (SystemData.fVcell > SystemData.fVthreshold3))
		{
		vhLedGREEN_off();
		vhLedORANGE_off();
		vhLedRED_on();
		}
		else if(SystemData.fVcell <= SystemData.fVthreshold3)
		{
		vhLedGREEN_off();
		vhLedORANGE_off();
		vhLedRED_toggle();
		}
		vTaskDelayUntil(&xLastFlashTime,500);

	}
}

