#include "uartdebug.h"
#include "indicators.h"
#include "stm32f1xx_hal.h"
#include <stdio.h>
#include "SystemData.h"
#include "adc.h"
#include "projectSettings.h"
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

extern UART_HandleTypeDef UART_DEBUG;
extern SystemData_InitTypeDef SystemData;
static uint8_t stringBuffer[42] = "Hello, I'm maiking a measurement";

xQueueHandle xQueueUARTdebug;
xTaskHandle xHandleTaskUARTdebug;
xUARTdebug xUARTdata;
uint8_t quickChar2;

void vhSend_Data(uint8_t *data, uint16_t size)
{
	HAL_UART_Transmit(&UART_DEBUG,data,size,100);
}

void vhDebugSendChar(uint8_t *character)
{
  
}

void vStartUARTdebugTask(uint8_t Priority)
{

//HAL_UART_Receive_IT(&UART_DEBUG, &quickChar2, 1);

xTaskCreate(vTaskUARTdebug,"UARTdebug",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskUARTdebug);
}

void vTaskUARTdebug(void *pvParameters)
{
xQueueUARTdebug = xQueueCreate( 10, sizeof(xUARTdebug));
for(;;)
	{
	xQueueReceive( xQueueUARTdebug, &xUARTdata, portMAX_DELAY );
	switch (xUARTdata.command)
		{
		case SendLogo1:
			{
			break;
			}
		case SendLogo2:
			{
			break;
			}
		case SendMeasurements:
			{				
			sprintf((char*)stringBuffer, "Voltage of the cell:         %04.2f V\r\n", SystemData.fVcell);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Voltage of the power supply: %04.2f V\r\n", SystemData.fVsupply);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Voltage of the Block Ident:  %04.2f V\r\n", SystemData.fVblockNr);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Voltage of the Tens Ident]:  %04.2f V\r\n", SystemData.fVtensNr);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Voltage of the Entity Ident: %04.2f V\r\n", SystemData.fVentityNr);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Temperature:                 %04.2f C\r\n", SystemData.fVtemp);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			sprintf((char*)stringBuffer, "Voltage of the reference:    %04.2f V\r\n", SystemData.fVref);
			vhSend_Data(stringBuffer, sizeof(stringBuffer));
			break;
			}
		case SendSystemData:
			{
			break;
			}
		default:
			{
			break;
			}
		}//end switch case 
	}//end for loop
}//end function

