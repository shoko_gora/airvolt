#include "indicators.h"
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

//My libraries
#include <stdint.h>



xTaskHandle xHandleTaskLedGREEN, xHandleTaskLedORANGE, xHandleTaskLedRED, xHandleTaskLedWiFi;
xTaskHandle xHandleTaskBatInd;

/////////////////////////////////////GREEN//////////////////////////////
void vhLedGREEN_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, GREEN, GPIO_PIN_SET);
}

void vhLedGREEN_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, GREEN, GPIO_PIN_RESET);
}
void vhLedGREEN_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, GREEN);
}

/////////////////////////////////////ORANGE//////////////////////////////
void vhLedORANGE_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, ORANGE, GPIO_PIN_SET);
}

void vhLedORANGE_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, ORANGE, GPIO_PIN_RESET);
}
void vhLedORANGE_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, ORANGE);
}

/////////////////////////////////////RED//////////////////////////////
void vhLedRED_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, RED, GPIO_PIN_SET);
}

void vhLedRED_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, RED, GPIO_PIN_RESET);
}
void vhLedRED_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, RED);
}
/////////////////////////////////////BLUE//////////////////////////////
void vhLedWiFi_on(void)
{
	HAL_GPIO_WritePin(IND_PORT, WiFi, GPIO_PIN_SET);
}

void vhLedWiFi_off(void)
{
	HAL_GPIO_WritePin(IND_PORT, WiFi, GPIO_PIN_RESET);
}
void vhLedWiFi_toggle(void)
{
		HAL_GPIO_TogglePin(IND_PORT, WiFi);
}

////////////////////////////////////////////////////RTOS//////////////////////////////////////
void vTaskLedGREEN(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,3000);
		vhLedGREEN_toggle();
	}
}

void vTaskLedORANGE(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,1000);
		vhLedORANGE_toggle();
	}
}
void vTaskLedRED(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,500);
		vhLedRED_toggle();
	}
}
void vTaskLedWiFi(void * pvParameters)
{
	portTickType xLastFlashTime;

	xLastFlashTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastFlashTime,1000);
		vhLedWiFi_toggle();
	}
}


void vStartLEDTasksTest(uint8_t Priority)
{

	//Creating a task for flashing LedRED with f = 1Hz
	xTaskCreate(vTaskLedGREEN,"LedGREEN",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedGREEN);
	//Creating a task for flashing LedRED with f = 2Hz
	xTaskCreate(vTaskLedORANGE,"LedORANGE",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedORANGE);
	//Creating a task for flashing LedRED with f = 5Hz
	xTaskCreate(vTaskLedRED,"LedRED",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedRED);
	//Creating a task for flashing LedRED with f = 10Hz
	xTaskCreate(vTaskLedWiFi,"LedWiFi",configMINIMAL_STACK_SIZE,NULL,
			Priority,&xHandleTaskLedWiFi);
}

void vKillLEDTasksTest(void)
{
vTaskDelete(xHandleTaskLedGREEN);
vTaskDelete(xHandleTaskLedORANGE);
vTaskDelete(xHandleTaskLedRED);
vTaskDelete(xHandleTaskLedWiFi);
}

