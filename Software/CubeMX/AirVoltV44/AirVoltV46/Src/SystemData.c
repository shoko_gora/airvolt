#include "SystemData.h"
#include "projectSettings.h"




SystemData_InitTypeDef SystemData;

void SystemDataInitialize(void)
{
	
	SystemData.fVcell = 0.0;
	SystemData.fVsupply = 0.0;
	SystemData.fVblockNr = 0.0 ;					
	SystemData.fVtensNr = 0.0;					
	SystemData.fVentityNr = 0.0;					
	SystemData.fVtemp = 0.0;					
	SystemData.fVref = 0.0;				
	SystemData.fVrefPinVal = VREF;
	SystemData.fQ = 0.0;
	SystemData.fspare0 = 0.0;
	SystemData.fspare1 = 0.0;
	SystemData.fVthreshold1 = 2.2;
	SystemData.fVthreshold2 = 2.0;
	SystemData.fVthreshold3 = 1.8;
	SystemData.fVthreshold4 = 0.0;
	SystemData.fVthreshold5 = 0.0;

	SystemData.iVcell = 0;
	SystemData.iVsupply = 0;					
	SystemData.iVblockNr = 0;
  SystemData.cBlockNr = 'X';
	SystemData.iVtensNr = 0;					
	SystemData.iVentityNr = 0;					
	SystemData.iVtemp = 0;					
	SystemData.iVref = 0;
	SystemData.iVrefPinVal = 4095;
	SystemData.iQ = 0;
	SystemData.ispare0 = 0;
	SystemData.ispare1 = 0;
	
	SystemData.ProgramStatus = NotStarted;
	SystemData.USBstate = On;
	SystemData.Buz = On;
	SystemData.cDay = 30;
	SystemData.cMonth = 31;
	SystemData.cYear = 32;
	SystemData.cDayOfTheWeek = 33;
	SystemData.cSeconds = 34;
	SystemData.cMinuts = 35;
	SystemData.cHours = 36;
	
	SystemData.myIP.firstByte = 0x00;
	SystemData.myIP.secondByte = 0x00;
	SystemData.myIP.therdByte = 0x00;
	SystemData.myIP.forthByte = 0x00;
	SystemData.host0IP = SystemData.myIP;
	SystemData.host1IP = SystemData.myIP;
	SystemData.host2IP = SystemData.myIP;
}

unsigned char tellMeTensID(void)
{
#ifdef POTREVERSE
    if(SystemData.iVtensNr <= 405)
		{
		return 9;
		}
		else if((SystemData.iVtensNr > 405)&& (SystemData.iVtensNr <= 810))
		{
		return 8 ;
		}
		else if((SystemData.iVtensNr > 810)&& (SystemData.iVtensNr <= 1214))
		{
		return 7;
		}
		else if((SystemData.iVtensNr > 1214)&& (SystemData.iVtensNr <= 1619))
		{
		return 6;
		}
		else if((SystemData.iVtensNr > 2024)&& (SystemData.iVtensNr <= 2429))
		{
		return 5;
		}
    else if((SystemData.iVtensNr > 2429)&& (SystemData.iVtensNr <= 2834))
		{
		return 4;
		}
    else if((SystemData.iVtensNr > 2834)&& (SystemData.iVtensNr <= 3238))
		{
		return 3;
		}
    else if((SystemData.iVtensNr > 3238)&& (SystemData.iVtensNr <= 3643))
		{
		return 2;
		}
    else if((SystemData.iVtensNr > 3643)&& (SystemData.iVtensNr <= 4048))
		{
		return 1;
		}
return 0;
#else
if(SystemData.iVtensNr <= 405)
	{
	return 1;
	}
	else if((SystemData.iVtensNr > 405)&& (SystemData.iVtensNr <= 810))
	{
	return 2 ;
	}
	else if((SystemData.iVtensNr > 810)&& (SystemData.iVtensNr <= 1214))
	{
	return 3;
	}
	else if((SystemData.iVtensNr > 1214)&& (SystemData.iVtensNr <= 1619))
	{
	return 4;
	}
	else if((SystemData.iVtensNr > 2024)&& (SystemData.iVtensNr <= 2429))
	{
	return 5;
	}
else if((SystemData.iVtensNr > 2429)&& (SystemData.iVtensNr <= 2834))
	{
	return 6;
	}
else if((SystemData.iVtensNr > 2834)&& (SystemData.iVtensNr <= 3238))
	{
	return 7;
	}
else if((SystemData.iVtensNr > 3238)&& (SystemData.iVtensNr <= 3643))
	{
	return 8;
	}
else if((SystemData.iVtensNr > 3643)&& (SystemData.iVtensNr <= 4048))
	{
	return 9;
	}
return 0;
#endif
}

unsigned char tellMeEntityID(void)
{
#ifdef POTREVERSE
    if(SystemData.iVentityNr <= 405)
		{
		return 9;
		}
		else if((SystemData.iVentityNr > 405)&& (SystemData.iVentityNr <= 810))
		{
		return 8 ;
		}
		else if((SystemData.iVentityNr > 810)&& (SystemData.iVentityNr <= 1214))
		{
		return 7;
		}
		else if((SystemData.iVentityNr > 1214)&& (SystemData.iVentityNr <= 1619))
		{
		return 6;
		}
		else if((SystemData.iVentityNr > 2024)&& (SystemData.iVentityNr <= 2429))
		{
		return 5;
		}
    else if((SystemData.iVentityNr > 2429)&& (SystemData.iVentityNr <= 2834))
		{
		return 4;
		}
    else if((SystemData.iVentityNr > 2834)&& (SystemData.iVentityNr <= 3238))
		{
		return 3;
		}
    else if((SystemData.iVentityNr > 3238)&& (SystemData.iVentityNr <= 3643))
		{
		return 2;
		}
    else if((SystemData.iVentityNr > 3643)&& (SystemData.iVentityNr <= 4048))
		{
		return 1;
		}
return 0;
#else
if(SystemData.iVentityNr <= 405)
		{
		return 1;
		}
		else if((SystemData.iVentityNr > 405)&& (SystemData.iVentityNr <= 810))
		{
		return 2 ;
		}
		else if((SystemData.iVentityNr > 810)&& (SystemData.iVentityNr <= 1214))
		{
		return 3;
		}
		else if((SystemData.iVentityNr > 1214)&& (SystemData.iVentityNr <= 1619))
		{
		return 4;
		}
		else if((SystemData.iVentityNr > 2024)&& (SystemData.iVentityNr <= 2429))
		{
		return 5;
		}
    else if((SystemData.iVentityNr > 2429)&& (SystemData.iVentityNr <= 2834))
		{
		return 6;
		}
    else if((SystemData.iVentityNr > 2834)&& (SystemData.iVentityNr <= 3238))
		{
		return 7;
		}
    else if((SystemData.iVentityNr > 3238)&& (SystemData.iVentityNr <= 3643))
		{
		return 8;
		}
    else if((SystemData.iVentityNr > 3643)&& (SystemData.iVentityNr <= 4048))
		{
		return 9;
		}
return 0;
#endif

}
