#ifndef PROJECTSETTINGS_H_
#define PROJECTSETTINGS_H_

#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal.h"


//#define SEND_MEASURES					      //this mode switch off tasks,hardware configuration and all what's connected with ADC
#define WATCHDOG_ON
#define POTREVERSE//no idea why I couldn't place it in projectSettinghs.h
#define WiFi_SSID  "AirVolt"
#define WiFi_PASSWORD "AirVoltPoland"
#define WiFi_BC_PORT 45454
#define WiFi_HOST_PORT 45455

#define WIFI_SW_RESET_PIN GPIO_PIN_9
#define WIFI_HW_RESET_PIN GPIO_PIN_7

#define WIFI_PORT	GPIOB
#define WIFI_HW_RESET_PORT GPIOA

#define UART_WIFI huart1
#define UART_DEBUG huart2

#define VREF 3.302
#define ADC_CYCLES 100
#define MEDIANA_REJECT 20


#endif //PROJECTSETTINGS_H_
