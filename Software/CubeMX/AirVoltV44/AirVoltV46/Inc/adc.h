//Indicators library like LEDs,buzzer,etc.
//08.09.2015
//              STM32F103VG
//            -----------------
//           |                 |
//           |                 |
//           |             IN0 |-->Cell Voltage
//           |             IN1 |-->VCC Voltage
//           |             IN4 |-->Block Identifier
//           |             IN5 |-->Tens Identifier
//           |             IN6 |-->Entity Identifier
//           |             IN16|-->Temperature Identifier
//           |             IN17|-->Vreference Identifier



#ifndef ADC_H_
#define ADC_H_

#include <stdint.h>

void vStartADCTask(uint8_t Priority);
void vADCTask(void * pvParameters);
void vStartWhoIam(void * pvParameters);
void vSort_tab(uint16_t *tab, uint8_t lenght);
uint16_t vADC_Mediana(uint16_t *tab , uint8_t X, uint8_t lenght);


#endif //ADC_H_
