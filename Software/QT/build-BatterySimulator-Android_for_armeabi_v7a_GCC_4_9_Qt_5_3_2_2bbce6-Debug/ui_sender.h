/********************************************************************************
** Form generated from reading UI file 'sender.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SENDER_H
#define UI_SENDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_sender
{
public:
    QWidget *centralWidget;
    QLabel *labelBatVal;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelIPhost;
    QLabel *labelIPhostBattery;
    QLabel *labelIPhostTens;
    QLabel *labelIPhostEntity;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *ipToSend;
    QComboBox *Battery;
    QComboBox *Tens;
    QComboBox *Entity;
    QLCDNumber *BatVoltage;
    QToolButton *buttonUp;
    QToolButton *buttonDown;
    QPushButton *startButton;
    QPushButton *quitButton;
    QLabel *statusLabel;
    QLineEdit *port;
    QComboBox *timeDuration;
    QPushButton *dischargeButton;

    void setupUi(QMainWindow *sender)
    {
        if (sender->objectName().isEmpty())
            sender->setObjectName(QStringLiteral("sender"));
        sender->resize(1080, 1920);
        sender->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(sender);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        labelBatVal = new QLabel(centralWidget);
        labelBatVal->setObjectName(QStringLiteral("labelBatVal"));
        labelBatVal->setGeometry(QRect(200, 430, 751, 101));
        QFont font;
        font.setPointSize(35);
        labelBatVal->setFont(font);
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(40, 150, 1001, 53));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        labelIPhost = new QLabel(horizontalLayoutWidget);
        labelIPhost->setObjectName(QStringLiteral("labelIPhost"));
        QFont font1;
        font1.setPointSize(20);
        labelIPhost->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhost);

        labelIPhostBattery = new QLabel(horizontalLayoutWidget);
        labelIPhostBattery->setObjectName(QStringLiteral("labelIPhostBattery"));
        labelIPhostBattery->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostBattery);

        labelIPhostTens = new QLabel(horizontalLayoutWidget);
        labelIPhostTens->setObjectName(QStringLiteral("labelIPhostTens"));
        labelIPhostTens->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostTens);

        labelIPhostEntity = new QLabel(horizontalLayoutWidget);
        labelIPhostEntity->setObjectName(QStringLiteral("labelIPhostEntity"));
        labelIPhostEntity->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostEntity);

        horizontalLayout_2->setStretch(0, 6);
        horizontalLayout_2->setStretch(1, 1);
        horizontalLayout_2->setStretch(2, 1);
        horizontalLayout_2->setStretch(3, 1);
        horizontalLayoutWidget_2 = new QWidget(centralWidget);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(40, 200, 1034, 111));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_3->setSpacing(20);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout_3->setContentsMargins(1, 1, 1, 1);
        ipToSend = new QLineEdit(horizontalLayoutWidget_2);
        ipToSend->setObjectName(QStringLiteral("ipToSend"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ipToSend->sizePolicy().hasHeightForWidth());
        ipToSend->setSizePolicy(sizePolicy);
        ipToSend->setMinimumSize(QSize(300, 100));
        ipToSend->setMaximumSize(QSize(600, 200));
        QFont font2;
        font2.setFamily(QStringLiteral("Arial"));
        font2.setPointSize(25);
        font2.setBold(true);
        font2.setWeight(75);
        ipToSend->setFont(font2);

        horizontalLayout_3->addWidget(ipToSend);

        Battery = new QComboBox(horizontalLayoutWidget_2);
        Battery->setObjectName(QStringLiteral("Battery"));
        sizePolicy.setHeightForWidth(Battery->sizePolicy().hasHeightForWidth());
        Battery->setSizePolicy(sizePolicy);
        Battery->setMinimumSize(QSize(100, 100));
        Battery->setMaximumSize(QSize(100, 100));
        QFont font3;
        font3.setPointSize(25);
        font3.setBold(true);
        font3.setWeight(75);
        Battery->setFont(font3);
        Battery->setLayoutDirection(Qt::LeftToRight);
        Battery->setFrame(true);

        horizontalLayout_3->addWidget(Battery);

        Tens = new QComboBox(horizontalLayoutWidget_2);
        Tens->setObjectName(QStringLiteral("Tens"));
        sizePolicy.setHeightForWidth(Tens->sizePolicy().hasHeightForWidth());
        Tens->setSizePolicy(sizePolicy);
        Tens->setMinimumSize(QSize(100, 100));
        Tens->setMaximumSize(QSize(100, 100));
        Tens->setFont(font3);
        Tens->setLayoutDirection(Qt::LeftToRight);
        Tens->setFrame(true);

        horizontalLayout_3->addWidget(Tens);

        Entity = new QComboBox(horizontalLayoutWidget_2);
        Entity->setObjectName(QStringLiteral("Entity"));
        sizePolicy.setHeightForWidth(Entity->sizePolicy().hasHeightForWidth());
        Entity->setSizePolicy(sizePolicy);
        Entity->setMinimumSize(QSize(100, 100));
        Entity->setMaximumSize(QSize(100, 100));
        Entity->setFont(font3);
        Entity->setLayoutDirection(Qt::LeftToRight);
        Entity->setFrame(true);

        horizontalLayout_3->addWidget(Entity);

        BatVoltage = new QLCDNumber(centralWidget);
        BatVoltage->setObjectName(QStringLiteral("BatVoltage"));
        BatVoltage->setGeometry(QRect(330, 550, 391, 401));
        BatVoltage->setFrameShadow(QFrame::Plain);
        BatVoltage->setLineWidth(2);
        BatVoltage->setMidLineWidth(1);
        BatVoltage->setDigitCount(4);
        BatVoltage->setSegmentStyle(QLCDNumber::Filled);
        BatVoltage->setProperty("value", QVariant(3.3));
        buttonUp = new QToolButton(centralWidget);
        buttonUp->setObjectName(QStringLiteral("buttonUp"));
        buttonUp->setGeometry(QRect(120, 600, 200, 200));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(buttonUp->sizePolicy().hasHeightForWidth());
        buttonUp->setSizePolicy(sizePolicy1);
        buttonUp->setMinimumSize(QSize(100, 200));
        buttonUp->setMaximumSize(QSize(200, 200));
        buttonUp->setCursor(QCursor(Qt::PointingHandCursor));
        buttonUp->setAutoFillBackground(false);
        buttonUp->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/icons/arrowUp.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonUp->setIcon(icon);
        buttonUp->setIconSize(QSize(180, 180));
        buttonUp->setPopupMode(QToolButton::DelayedPopup);
        buttonUp->setToolButtonStyle(Qt::ToolButtonIconOnly);
        buttonUp->setAutoRaise(false);
        buttonUp->setArrowType(Qt::NoArrow);
        buttonDown = new QToolButton(centralWidget);
        buttonDown->setObjectName(QStringLiteral("buttonDown"));
        buttonDown->setGeometry(QRect(760, 610, 200, 200));
        sizePolicy1.setHeightForWidth(buttonDown->sizePolicy().hasHeightForWidth());
        buttonDown->setSizePolicy(sizePolicy1);
        buttonDown->setMinimumSize(QSize(100, 200));
        buttonDown->setMaximumSize(QSize(200, 200));
        QFont font4;
        font4.setPointSize(10);
        buttonDown->setFont(font4);
        buttonDown->setCursor(QCursor(Qt::PointingHandCursor));
        buttonDown->setAutoFillBackground(false);
        buttonDown->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/new/icons/arrowDown.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonDown->setIcon(icon1);
        buttonDown->setIconSize(QSize(180, 180));
        buttonDown->setPopupMode(QToolButton::InstantPopup);
        buttonDown->setToolButtonStyle(Qt::ToolButtonIconOnly);
        buttonDown->setAutoRaise(false);
        buttonDown->setArrowType(Qt::NoArrow);
        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setGeometry(QRect(380, 1530, 311, 111));
        QFont font5;
        font5.setPointSize(30);
        font5.setBold(true);
        font5.setWeight(75);
        startButton->setFont(font5);
        quitButton = new QPushButton(centralWidget);
        quitButton->setObjectName(QStringLiteral("quitButton"));
        quitButton->setGeometry(QRect(730, 1530, 311, 111));
        QFont font6;
        font6.setPointSize(30);
        font6.setBold(false);
        font6.setItalic(true);
        font6.setWeight(50);
        quitButton->setFont(font6);
        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        statusLabel->setGeometry(QRect(15, 980, 871, 51));
        statusLabel->setFont(font1);
        port = new QLineEdit(centralWidget);
        port->setObjectName(QStringLiteral("port"));
        port->setGeometry(QRect(350, 320, 300, 100));
        sizePolicy.setHeightForWidth(port->sizePolicy().hasHeightForWidth());
        port->setSizePolicy(sizePolicy);
        port->setMinimumSize(QSize(300, 100));
        port->setMaximumSize(QSize(600, 200));
        port->setFont(font2);
        timeDuration = new QComboBox(centralWidget);
        timeDuration->setObjectName(QStringLiteral("timeDuration"));
        timeDuration->setGeometry(QRect(390, 1050, 200, 100));
        sizePolicy.setHeightForWidth(timeDuration->sizePolicy().hasHeightForWidth());
        timeDuration->setSizePolicy(sizePolicy);
        timeDuration->setMinimumSize(QSize(200, 100));
        timeDuration->setMaximumSize(QSize(200, 100));
        QFont font7;
        font7.setPointSize(20);
        font7.setBold(true);
        font7.setWeight(75);
        timeDuration->setFont(font7);
        timeDuration->setLayoutDirection(Qt::LeftToRight);
        timeDuration->setFrame(true);
        dischargeButton = new QPushButton(centralWidget);
        dischargeButton->setObjectName(QStringLiteral("dischargeButton"));
        dischargeButton->setGeometry(QRect(20, 1050, 341, 181));
        dischargeButton->setMinimumSize(QSize(100, 100));
        dischargeButton->setMaximumSize(QSize(400, 300));
        QFont font8;
        font8.setPointSize(15);
        font8.setBold(true);
        font8.setWeight(75);
        font8.setStyleStrategy(QFont::PreferAntialias);
        dischargeButton->setFont(font8);
        dischargeButton->setCheckable(true);
        dischargeButton->setChecked(false);
        dischargeButton->setAutoDefault(false);
        dischargeButton->setDefault(false);
        dischargeButton->setFlat(false);
        sender->setCentralWidget(centralWidget);

        retranslateUi(sender);

        Battery->setCurrentIndex(0);
        Tens->setCurrentIndex(0);
        Entity->setCurrentIndex(0);
        timeDuration->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(sender);
    } // setupUi

    void retranslateUi(QMainWindow *sender)
    {
        sender->setWindowTitle(QApplication::translate("sender", "sender", 0));
        labelBatVal->setText(QApplication::translate("sender", "batery value", 0));
        labelIPhost->setText(QApplication::translate("sender", "Host IP to send", 0));
        labelIPhostBattery->setText(QApplication::translate("sender", "B", 0));
        labelIPhostTens->setText(QApplication::translate("sender", "T", 0));
        labelIPhostEntity->setText(QApplication::translate("sender", "E", 0));
        ipToSend->setText(QApplication::translate("sender", "192.168.43.252", 0));
        Battery->clear();
        Battery->insertItems(0, QStringList()
         << QApplication::translate("sender", "A", 0)
         << QApplication::translate("sender", "B", 0)
         << QApplication::translate("sender", "C", 0)
         << QApplication::translate("sender", "D", 0)
        );
        Tens->clear();
        Tens->insertItems(0, QStringList()
         << QApplication::translate("sender", "0", 0)
         << QApplication::translate("sender", "1", 0)
         << QApplication::translate("sender", "2", 0)
         << QApplication::translate("sender", "3", 0)
         << QApplication::translate("sender", "4", 0)
         << QApplication::translate("sender", "5", 0)
         << QApplication::translate("sender", "6", 0)
         << QApplication::translate("sender", "7", 0)
         << QApplication::translate("sender", "8", 0)
         << QApplication::translate("sender", "9", 0)
        );
        Entity->clear();
        Entity->insertItems(0, QStringList()
         << QApplication::translate("sender", "0", 0)
         << QApplication::translate("sender", "1", 0)
         << QApplication::translate("sender", "2", 0)
         << QApplication::translate("sender", "3", 0)
         << QApplication::translate("sender", "4", 0)
         << QApplication::translate("sender", "5", 0)
         << QApplication::translate("sender", "6", 0)
         << QApplication::translate("sender", "7", 0)
         << QApplication::translate("sender", "8", 0)
         << QApplication::translate("sender", "9", 0)
        );
        startButton->setText(QApplication::translate("sender", "Start", 0));
        quitButton->setText(QApplication::translate("sender", "Quit", 0));
        statusLabel->setText(QApplication::translate("sender", "Ready to broadcast datagrams on port 45454", 0));
        port->setText(QApplication::translate("sender", "45455", 0));
        timeDuration->clear();
        timeDuration->insertItems(0, QStringList()
         << QApplication::translate("sender", "10 sec", 0)
         << QApplication::translate("sender", "30 sec", 0)
         << QApplication::translate("sender", "1 min", 0)
         << QApplication::translate("sender", "15 min", 0)
         << QApplication::translate("sender", "30 min", 0)
         << QApplication::translate("sender", "60 min", 0)
        );
        dischargeButton->setText(QApplication::translate("sender", "Press to run \n"
"discharge mode", 0));
    } // retranslateUi

};

namespace Ui {
    class sender: public Ui_sender {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SENDER_H
