#ifndef SENDER_H
#define SENDER_H

#include <QMainWindow>
#include <QTimer>
#include <QUdpSocket>

namespace Ui {
class sender;
}

class sender : public QMainWindow
{
    Q_OBJECT

public:
    explicit sender(QWidget *parent = 0);
    ~sender();

private slots:
    void startBroadcasting();
    void broadcastDatagram();

    void on_buttonUp_clicked();

    void on_buttonDown_clicked();

    void on_dischargeButton_pressed();
    void reduceVoltage10mV();
    void reduceVoltage100mV();


    void on_timeDuration_activated(int index);

private:
    Ui::sender *ui;
    QUdpSocket *udpSocket;
    QTimer *timer;
    QTimer *timer2;
    int messageNo;
    float fBatVoltage;
    QString getMyIPasString();
    void voltageLimiter();
    void dischargerSimulator(bool dischargerSimulatorYesNo);

};

#endif // SENDER_H
