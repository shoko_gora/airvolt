#include "sender.h"
#include "ui_sender.h"
#include <QNetworkInterface>
#include <QString>

sender::sender(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::sender)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    timer2 = new QTimer(this);

    udpSocket = new QUdpSocket(this);

    messageNo = 1;

    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(startBroadcasting()));
    connect(ui->quitButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(timer, SIGNAL(timeout()), this, SLOT(broadcastDatagram()));
    connect(timer2, SIGNAL(timeout()), this, SLOT(reduceVoltage100mV()));


    fBatVoltage = 3.3;

    ui->BatVoltage->display(fBatVoltage);

}

sender::~sender()
{
    delete ui;
}

void sender::startBroadcasting()
{
    ui->startButton->setEnabled(false);
    timer->start(1000);
}

void sender::broadcastDatagram()
{
    ui->statusLabel->setText(tr("Now broadcasting datagram %1").arg(messageNo));
//! [1]
    QByteArray datagram = "<ID>";
    datagram.append(ui->Battery->currentText());
    datagram.append(ui->Tens->currentText());
    datagram.append(ui->Entity->currentText());
    datagram.append("<IP>");
    datagram.append(getMyIPasString());
    datagram.append("<Vbat>");
    datagram.append(QString::number(fBatVoltage,'f',2));
    datagram.append("V");
    udpSocket->writeDatagram(datagram.data(), datagram.size(),
                             QHostAddress(ui->ipToSend->text()), ui->port->text().toInt());
//! [1]
    ++messageNo;
}

QString sender::getMyIPasString()
{
foreach (const QHostAddress &address, QNetworkInterface::allAddresses())
    {
    if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
         return address.toString();
    }
return 0;
}

void sender::voltageLimiter()
{
    if (fBatVoltage < 0)
        {
        fBatVoltage = 0;
        }
    else if (fBatVoltage > 3.3)
        {
        fBatVoltage = 3.3;
        }
    ui->BatVoltage->display(fBatVoltage);
}

void sender::dischargerSimulator(bool dischargerSimulatorYesNo)
{
   if(dischargerSimulatorYesNo == true)
     {
       switch(ui->timeDuration->currentIndex())
        {
       case 0://10 sec
             {
             timer2->start(10000);
             break;
             }
       case 1://30 sec
            {
            timer2->start(30000);
            break;
            }

       case 2://1 min
            {
            timer2->start(60000);
            break;
            }
       case 3://15 min
           {
           timer2->start(900000);
           break;
           }
       case 4://30 min
            {
            timer2->start(1800000);
            break;
            }
       case 5://60 min
           {
           timer2->start(3600000);
           break;
           }
        default:
             {
             break;
             }
        }


     }
   else
     {
     timer2->stop();
     }

}

void sender::reduceVoltage100mV()
{
    fBatVoltage-= 0.1;
    voltageLimiter();
}

void sender::reduceVoltage10mV()
{
    fBatVoltage-= 0.01;
    voltageLimiter();
}

void sender::on_buttonUp_clicked()
{
    fBatVoltage+= 0.1;

    voltageLimiter();
}

void sender::on_buttonDown_clicked()
{
    fBatVoltage-= 0.1;

    voltageLimiter();

}

void sender::on_dischargeButton_pressed()

{
    if(ui->dischargeButton->isChecked() == true)
    {
        dischargerSimulator(false);
        ui->dischargeButton->setText("Press to run \ndischarge mode");

    }
    else
    {
        dischargerSimulator(true);
        ui->dischargeButton->setText("Discharging!!!");
    }
}


void sender::on_timeDuration_activated(int index)
{
    if(ui->dischargeButton->isChecked() == true)
    {
        dischargerSimulator(true);
    }
    else
    {
        dischargerSimulator(false);
    }
}
