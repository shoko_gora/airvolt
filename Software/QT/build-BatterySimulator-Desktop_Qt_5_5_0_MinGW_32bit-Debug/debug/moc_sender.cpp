/****************************************************************************
** Meta object code from reading C++ file 'sender.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../BatterySimulator2/sender.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sender.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_sender_t {
    QByteArrayData data[11];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_sender_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_sender_t qt_meta_stringdata_sender = {
    {
QT_MOC_LITERAL(0, 0, 6), // "sender"
QT_MOC_LITERAL(1, 7, 17), // "startBroadcasting"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 17), // "broadcastDatagram"
QT_MOC_LITERAL(4, 44, 19), // "on_buttonUp_clicked"
QT_MOC_LITERAL(5, 64, 21), // "on_buttonDown_clicked"
QT_MOC_LITERAL(6, 86, 26), // "on_dischargeButton_pressed"
QT_MOC_LITERAL(7, 113, 17), // "reduceVoltage10mV"
QT_MOC_LITERAL(8, 131, 18), // "reduceVoltage100mV"
QT_MOC_LITERAL(9, 150, 25), // "on_timeDuration_activated"
QT_MOC_LITERAL(10, 176, 5) // "index"

    },
    "sender\0startBroadcasting\0\0broadcastDatagram\0"
    "on_buttonUp_clicked\0on_buttonDown_clicked\0"
    "on_dischargeButton_pressed\0reduceVoltage10mV\0"
    "reduceVoltage100mV\0on_timeDuration_activated\0"
    "index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_sender[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    1,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,

       0        // eod
};

void sender::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        sender *_t = static_cast<sender *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->startBroadcasting(); break;
        case 1: _t->broadcastDatagram(); break;
        case 2: _t->on_buttonUp_clicked(); break;
        case 3: _t->on_buttonDown_clicked(); break;
        case 4: _t->on_dischargeButton_pressed(); break;
        case 5: _t->reduceVoltage10mV(); break;
        case 6: _t->reduceVoltage100mV(); break;
        case 7: _t->on_timeDuration_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject sender::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_sender.data,
      qt_meta_data_sender,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *sender::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *sender::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_sender.stringdata0))
        return static_cast<void*>(const_cast< sender*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int sender::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
