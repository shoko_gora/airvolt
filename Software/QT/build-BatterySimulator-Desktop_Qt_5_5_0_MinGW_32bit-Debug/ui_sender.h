/********************************************************************************
** Form generated from reading UI file 'sender.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SENDER_H
#define UI_SENDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_sender
{
public:
    QWidget *centralWidget;
    QLabel *labelBatVal;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelIPhost;
    QLabel *labelIPhostBattery;
    QLabel *labelIPhostTens;
    QLabel *labelIPhostEntity;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *hboxLayout;
    QLineEdit *ipToSend;
    QComboBox *Battery;
    QComboBox *Tens;
    QComboBox *Entity;
    QLCDNumber *BatVoltage;
    QToolButton *buttonUp;
    QToolButton *buttonDown;
    QPushButton *startButton;
    QPushButton *quitButton;
    QLabel *statusLabel;
    QLineEdit *port;
    QComboBox *timeDuration;
    QPushButton *dischargeButton;

    void setupUi(QMainWindow *sender)
    {
        if (sender->objectName().isEmpty())
            sender->setObjectName(QStringLiteral("sender"));
        sender->resize(480, 800);
        sender->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(sender);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        labelBatVal = new QLabel(centralWidget);
        labelBatVal->setObjectName(QStringLiteral("labelBatVal"));
        labelBatVal->setGeometry(QRect(150, 270, 181, 61));
        QFont font;
        font.setPointSize(20);
        labelBatVal->setFont(font);
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 10, 461, 54));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        labelIPhost = new QLabel(horizontalLayoutWidget);
        labelIPhost->setObjectName(QStringLiteral("labelIPhost"));
        QFont font1;
        font1.setPointSize(10);
        labelIPhost->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhost);

        labelIPhostBattery = new QLabel(horizontalLayoutWidget);
        labelIPhostBattery->setObjectName(QStringLiteral("labelIPhostBattery"));
        labelIPhostBattery->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostBattery);

        labelIPhostTens = new QLabel(horizontalLayoutWidget);
        labelIPhostTens->setObjectName(QStringLiteral("labelIPhostTens"));
        labelIPhostTens->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostTens);

        labelIPhostEntity = new QLabel(horizontalLayoutWidget);
        labelIPhostEntity->setObjectName(QStringLiteral("labelIPhostEntity"));
        labelIPhostEntity->setFont(font1);

        horizontalLayout_2->addWidget(labelIPhostEntity);

        horizontalLayout_2->setStretch(0, 6);
        horizontalLayout_2->setStretch(1, 2);
        horizontalLayout_2->setStretch(2, 2);
        horizontalLayout_2->setStretch(3, 2);
        horizontalLayoutWidget_2 = new QWidget(centralWidget);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 60, 461, 111));
        hboxLayout = new QHBoxLayout(horizontalLayoutWidget_2);
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(11, 11, 11, 11);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setSizeConstraint(QLayout::SetMinimumSize);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        ipToSend = new QLineEdit(horizontalLayoutWidget_2);
        ipToSend->setObjectName(QStringLiteral("ipToSend"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ipToSend->sizePolicy().hasHeightForWidth());
        ipToSend->setSizePolicy(sizePolicy);
        ipToSend->setMinimumSize(QSize(150, 50));
        ipToSend->setMaximumSize(QSize(100, 50));
        QFont font2;
        font2.setFamily(QStringLiteral("Arial"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        ipToSend->setFont(font2);

        hboxLayout->addWidget(ipToSend);

        Battery = new QComboBox(horizontalLayoutWidget_2);
        Battery->setObjectName(QStringLiteral("Battery"));
        sizePolicy.setHeightForWidth(Battery->sizePolicy().hasHeightForWidth());
        Battery->setSizePolicy(sizePolicy);
        Battery->setMinimumSize(QSize(50, 50));
        Battery->setMaximumSize(QSize(50, 50));
        QFont font3;
        font3.setPointSize(15);
        font3.setBold(true);
        font3.setWeight(75);
        Battery->setFont(font3);
        Battery->setLayoutDirection(Qt::LeftToRight);
        Battery->setFrame(true);

        hboxLayout->addWidget(Battery);

        Tens = new QComboBox(horizontalLayoutWidget_2);
        Tens->setObjectName(QStringLiteral("Tens"));
        sizePolicy.setHeightForWidth(Tens->sizePolicy().hasHeightForWidth());
        Tens->setSizePolicy(sizePolicy);
        Tens->setMinimumSize(QSize(50, 50));
        Tens->setMaximumSize(QSize(50, 50));
        Tens->setFont(font3);
        Tens->setLayoutDirection(Qt::LeftToRight);
        Tens->setFrame(true);

        hboxLayout->addWidget(Tens);

        Entity = new QComboBox(horizontalLayoutWidget_2);
        Entity->setObjectName(QStringLiteral("Entity"));
        sizePolicy.setHeightForWidth(Entity->sizePolicy().hasHeightForWidth());
        Entity->setSizePolicy(sizePolicy);
        Entity->setMinimumSize(QSize(50, 50));
        Entity->setMaximumSize(QSize(50, 50));
        Entity->setFont(font3);
        Entity->setLayoutDirection(Qt::LeftToRight);
        Entity->setFrame(true);

        hboxLayout->addWidget(Entity);

        BatVoltage = new QLCDNumber(centralWidget);
        BatVoltage->setObjectName(QStringLiteral("BatVoltage"));
        BatVoltage->setGeometry(QRect(120, 340, 231, 181));
        BatVoltage->setFrameShadow(QFrame::Plain);
        BatVoltage->setLineWidth(2);
        BatVoltage->setMidLineWidth(1);
        BatVoltage->setDigitCount(4);
        BatVoltage->setSegmentStyle(QLCDNumber::Filled);
        BatVoltage->setProperty("value", QVariant(3.3));
        buttonUp = new QToolButton(centralWidget);
        buttonUp->setObjectName(QStringLiteral("buttonUp"));
        buttonUp->setGeometry(QRect(10, 370, 100, 100));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(buttonUp->sizePolicy().hasHeightForWidth());
        buttonUp->setSizePolicy(sizePolicy1);
        buttonUp->setMinimumSize(QSize(100, 100));
        buttonUp->setMaximumSize(QSize(50, 50));
        buttonUp->setCursor(QCursor(Qt::PointingHandCursor));
        buttonUp->setAutoFillBackground(false);
        buttonUp->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/icons/arrowUp.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonUp->setIcon(icon);
        buttonUp->setIconSize(QSize(180, 180));
        buttonUp->setPopupMode(QToolButton::DelayedPopup);
        buttonUp->setToolButtonStyle(Qt::ToolButtonIconOnly);
        buttonUp->setAutoRaise(false);
        buttonUp->setArrowType(Qt::NoArrow);
        buttonDown = new QToolButton(centralWidget);
        buttonDown->setObjectName(QStringLiteral("buttonDown"));
        buttonDown->setGeometry(QRect(360, 370, 100, 100));
        sizePolicy1.setHeightForWidth(buttonDown->sizePolicy().hasHeightForWidth());
        buttonDown->setSizePolicy(sizePolicy1);
        buttonDown->setMinimumSize(QSize(100, 100));
        buttonDown->setMaximumSize(QSize(50, 50));
        buttonDown->setFont(font1);
        buttonDown->setCursor(QCursor(Qt::PointingHandCursor));
        buttonDown->setAutoFillBackground(false);
        buttonDown->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/new/icons/arrowDown.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonDown->setIcon(icon1);
        buttonDown->setIconSize(QSize(180, 180));
        buttonDown->setPopupMode(QToolButton::InstantPopup);
        buttonDown->setToolButtonStyle(Qt::ToolButtonIconOnly);
        buttonDown->setAutoRaise(false);
        buttonDown->setArrowType(Qt::NoArrow);
        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setGeometry(QRect(20, 670, 181, 61));
        QFont font4;
        font4.setPointSize(30);
        font4.setBold(true);
        font4.setWeight(75);
        startButton->setFont(font4);
        quitButton = new QPushButton(centralWidget);
        quitButton->setObjectName(QStringLiteral("quitButton"));
        quitButton->setGeometry(QRect(230, 670, 181, 61));
        QFont font5;
        font5.setPointSize(30);
        font5.setBold(false);
        font5.setItalic(true);
        font5.setWeight(50);
        quitButton->setFont(font5);
        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        statusLabel->setGeometry(QRect(0, 519, 871, 51));
        QFont font6;
        font6.setPointSize(12);
        statusLabel->setFont(font6);
        port = new QLineEdit(centralWidget);
        port->setObjectName(QStringLiteral("port"));
        port->setGeometry(QRect(60, 180, 131, 61));
        sizePolicy.setHeightForWidth(port->sizePolicy().hasHeightForWidth());
        port->setSizePolicy(sizePolicy);
        port->setFont(font2);
        timeDuration = new QComboBox(centralWidget);
        timeDuration->setObjectName(QStringLiteral("timeDuration"));
        timeDuration->setGeometry(QRect(180, 569, 50, 50));
        sizePolicy.setHeightForWidth(timeDuration->sizePolicy().hasHeightForWidth());
        timeDuration->setSizePolicy(sizePolicy);
        timeDuration->setMinimumSize(QSize(50, 50));
        timeDuration->setMaximumSize(QSize(50, 50));
        QFont font7;
        font7.setPointSize(10);
        font7.setBold(true);
        font7.setWeight(75);
        timeDuration->setFont(font7);
        timeDuration->setLayoutDirection(Qt::LeftToRight);
        timeDuration->setFrame(true);
        dischargeButton = new QPushButton(centralWidget);
        dischargeButton->setObjectName(QStringLiteral("dischargeButton"));
        dischargeButton->setGeometry(QRect(10, 570, 151, 51));
        QFont font8;
        font8.setPointSize(10);
        font8.setBold(true);
        font8.setWeight(75);
        font8.setStyleStrategy(QFont::PreferAntialias);
        dischargeButton->setFont(font8);
        dischargeButton->setCheckable(true);
        dischargeButton->setChecked(false);
        dischargeButton->setAutoDefault(false);
        dischargeButton->setFlat(false);
        sender->setCentralWidget(centralWidget);

        retranslateUi(sender);

        Battery->setCurrentIndex(0);
        Tens->setCurrentIndex(0);
        Entity->setCurrentIndex(0);
        timeDuration->setCurrentIndex(0);
        dischargeButton->setDefault(false);


        QMetaObject::connectSlotsByName(sender);
    } // setupUi

    void retranslateUi(QMainWindow *sender)
    {
        sender->setWindowTitle(QApplication::translate("sender", "sender", 0));
        labelBatVal->setText(QApplication::translate("sender", "batery value", 0));
        labelIPhost->setText(QApplication::translate("sender", "Host IP to send", 0));
        labelIPhostBattery->setText(QApplication::translate("sender", "B", 0));
        labelIPhostTens->setText(QApplication::translate("sender", "T", 0));
        labelIPhostEntity->setText(QApplication::translate("sender", "E", 0));
        ipToSend->setText(QApplication::translate("sender", "192.168.43.252", 0));
        Battery->clear();
        Battery->insertItems(0, QStringList()
         << QApplication::translate("sender", "A", 0)
         << QApplication::translate("sender", "B", 0)
         << QApplication::translate("sender", "C", 0)
         << QApplication::translate("sender", "D", 0)
        );
        Tens->clear();
        Tens->insertItems(0, QStringList()
         << QApplication::translate("sender", "0", 0)
         << QApplication::translate("sender", "1", 0)
         << QApplication::translate("sender", "2", 0)
         << QApplication::translate("sender", "3", 0)
         << QApplication::translate("sender", "4", 0)
         << QApplication::translate("sender", "5", 0)
         << QApplication::translate("sender", "6", 0)
         << QApplication::translate("sender", "7", 0)
         << QApplication::translate("sender", "8", 0)
         << QApplication::translate("sender", "9", 0)
        );
        Entity->clear();
        Entity->insertItems(0, QStringList()
         << QApplication::translate("sender", "0", 0)
         << QApplication::translate("sender", "1", 0)
         << QApplication::translate("sender", "2", 0)
         << QApplication::translate("sender", "3", 0)
         << QApplication::translate("sender", "4", 0)
         << QApplication::translate("sender", "5", 0)
         << QApplication::translate("sender", "6", 0)
         << QApplication::translate("sender", "7", 0)
         << QApplication::translate("sender", "8", 0)
         << QApplication::translate("sender", "9", 0)
        );
        startButton->setText(QApplication::translate("sender", "Start", 0));
        quitButton->setText(QApplication::translate("sender", "Quit", 0));
        statusLabel->setText(QApplication::translate("sender", "Ready to broadcast datagrams on port 45455", 0));
        port->setText(QApplication::translate("sender", "45455", 0));
        timeDuration->clear();
        timeDuration->insertItems(0, QStringList()
         << QApplication::translate("sender", "10 sec", 0)
         << QApplication::translate("sender", "30 sec", 0)
         << QApplication::translate("sender", "1 min", 0)
         << QApplication::translate("sender", "15 min", 0)
         << QApplication::translate("sender", "30 min", 0)
         << QApplication::translate("sender", "60 min", 0)
        );
        dischargeButton->setText(QApplication::translate("sender", "Press to run \n"
"discharge mode", 0));
    } // retranslateUi

};

namespace Ui {
    class sender: public Ui_sender {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SENDER_H
