#-------------------------------------------------
#
# Project created by QtCreator 2016-04-16T15:29:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BatterySimulator
TEMPLATE = app


SOURCES += main.cpp\
        sender.cpp
QT += network

HEADERS  += sender.h

FORMS    += sender.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    icons.qrc

