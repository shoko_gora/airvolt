<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="framebase" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="case" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Stiffner" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="mVias" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="mtStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="PART_" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Marcin_LIB">
<packages>
<package name="LQFP48">
<wire x1="-3.4" y1="3.1" x2="-3.1" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="3.4" x2="3.1" y2="3.4" width="0.2032" layer="51"/>
<wire x1="3.1" y1="3.4" x2="3.4" y2="3.1" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.1" x2="3.4" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.1" x2="3.1" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-3.4" x2="-3.1" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="-3.1" y1="-3.4" x2="-3.4" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-3.1" x2="-3.4" y2="3.1" width="0.2032" layer="51"/>
<circle x="-2.5" y="2.5" radius="0.5" width="0" layer="21"/>
<smd name="1" x="-3.95" y="2.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="2" x="-3.95" y="2.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="3" x="-3.95" y="1.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="4" x="-3.95" y="1.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="5" x="-3.95" y="0.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="6" x="-3.95" y="0.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="7" x="-3.95" y="-0.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="8" x="-3.95" y="-0.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="9" x="-3.95" y="-1.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="10" x="-3.95" y="-1.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="11" x="-3.95" y="-2.25" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="12" x="-3.95" y="-2.75" dx="1.3" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="13" x="-2.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="14" x="-2.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="15" x="-1.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="16" x="-1.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="17" x="-0.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="18" x="-0.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="19" x="0.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="20" x="0.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="21" x="1.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="22" x="1.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="23" x="2.25" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="24" x="2.75" y="-3.95" dx="1.3" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="25" x="3.95" y="-2.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="26" x="3.95" y="-2.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="27" x="3.95" y="-1.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="28" x="3.95" y="-1.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="29" x="3.95" y="-0.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="30" x="3.95" y="-0.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="31" x="3.95" y="0.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="32" x="3.95" y="0.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="33" x="3.95" y="1.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="34" x="3.95" y="1.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="35" x="3.95" y="2.25" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="36" x="3.95" y="2.75" dx="1.3" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="37" x="2.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="38" x="2.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="39" x="1.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="40" x="1.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="41" x="0.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="42" x="0.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="43" x="-0.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="44" x="-0.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="45" x="-1.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="46" x="-1.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="47" x="-2.25" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="48" x="-2.75" y="3.95" dx="1.3" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<text x="-4" y="4.675" size="1.27" layer="25">&gt;NAME</text>
<text x="-4" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.45" y1="2.55" x2="-3.45" y2="2.95" layer="29"/>
<rectangle x1="-4.375" y1="2.625" x2="-3.525" y2="2.875" layer="31"/>
<rectangle x1="-4.225" y1="2.65" x2="-3.475" y2="2.85" layer="51"/>
<rectangle x1="-4.45" y1="2.05" x2="-3.45" y2="2.45" layer="29"/>
<rectangle x1="-4.375" y1="2.125" x2="-3.525" y2="2.375" layer="31"/>
<rectangle x1="-4.225" y1="2.15" x2="-3.475" y2="2.35" layer="51"/>
<rectangle x1="-4.45" y1="1.55" x2="-3.45" y2="1.95" layer="29"/>
<rectangle x1="-4.375" y1="1.625" x2="-3.525" y2="1.875" layer="31"/>
<rectangle x1="-4.225" y1="1.65" x2="-3.475" y2="1.85" layer="51"/>
<rectangle x1="-4.45" y1="1.05" x2="-3.45" y2="1.45" layer="29"/>
<rectangle x1="-4.375" y1="1.125" x2="-3.525" y2="1.375" layer="31"/>
<rectangle x1="-4.225" y1="1.15" x2="-3.475" y2="1.35" layer="51"/>
<rectangle x1="-4.45" y1="0.55" x2="-3.45" y2="0.95" layer="29"/>
<rectangle x1="-4.375" y1="0.625" x2="-3.525" y2="0.875" layer="31"/>
<rectangle x1="-4.225" y1="0.65" x2="-3.475" y2="0.85" layer="51"/>
<rectangle x1="-4.45" y1="0.05" x2="-3.45" y2="0.45" layer="29"/>
<rectangle x1="-4.375" y1="0.125" x2="-3.525" y2="0.375" layer="31"/>
<rectangle x1="-4.225" y1="0.15" x2="-3.475" y2="0.35" layer="51"/>
<rectangle x1="-4.45" y1="-0.45" x2="-3.45" y2="-0.05" layer="29"/>
<rectangle x1="-4.375" y1="-0.375" x2="-3.525" y2="-0.125" layer="31"/>
<rectangle x1="-4.225" y1="-0.35" x2="-3.475" y2="-0.15" layer="51"/>
<rectangle x1="-4.45" y1="-0.95" x2="-3.45" y2="-0.55" layer="29"/>
<rectangle x1="-4.375" y1="-0.875" x2="-3.525" y2="-0.625" layer="31"/>
<rectangle x1="-4.225" y1="-0.85" x2="-3.475" y2="-0.65" layer="51"/>
<rectangle x1="-4.45" y1="-1.45" x2="-3.45" y2="-1.05" layer="29"/>
<rectangle x1="-4.375" y1="-1.375" x2="-3.525" y2="-1.125" layer="31"/>
<rectangle x1="-4.225" y1="-1.35" x2="-3.475" y2="-1.15" layer="51"/>
<rectangle x1="-4.45" y1="-1.95" x2="-3.45" y2="-1.55" layer="29"/>
<rectangle x1="-4.375" y1="-1.875" x2="-3.525" y2="-1.625" layer="31"/>
<rectangle x1="-4.225" y1="-1.85" x2="-3.475" y2="-1.65" layer="51"/>
<rectangle x1="-4.45" y1="-2.45" x2="-3.45" y2="-2.05" layer="29"/>
<rectangle x1="-4.375" y1="-2.375" x2="-3.525" y2="-2.125" layer="31"/>
<rectangle x1="-4.225" y1="-2.35" x2="-3.475" y2="-2.15" layer="51"/>
<rectangle x1="-4.45" y1="-2.95" x2="-3.45" y2="-2.55" layer="29"/>
<rectangle x1="-4.375" y1="-2.875" x2="-3.525" y2="-2.625" layer="31"/>
<rectangle x1="-4.225" y1="-2.85" x2="-3.475" y2="-2.65" layer="51"/>
<rectangle x1="-3.25" y1="-4.15" x2="-2.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-3.175" y1="-4.075" x2="-2.325" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-3.125" y1="-3.95" x2="-2.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-2.75" y1="-4.15" x2="-1.75" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-2.675" y1="-4.075" x2="-1.825" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-2.625" y1="-3.95" x2="-1.875" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-2.25" y1="-4.15" x2="-1.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-2.175" y1="-4.075" x2="-1.325" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-2.125" y1="-3.95" x2="-1.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-1.75" y1="-4.15" x2="-0.75" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-1.675" y1="-4.075" x2="-0.825" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-1.625" y1="-3.95" x2="-0.875" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-1.25" y1="-4.15" x2="-0.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-1.175" y1="-4.075" x2="-0.325" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-1.125" y1="-3.95" x2="-0.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-0.75" y1="-4.15" x2="0.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-0.675" y1="-4.075" x2="0.175" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-0.625" y1="-3.95" x2="0.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-0.25" y1="-4.15" x2="0.75" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="-0.175" y1="-4.075" x2="0.675" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="-0.125" y1="-3.95" x2="0.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="0.25" y1="-4.15" x2="1.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="0.325" y1="-4.075" x2="1.175" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="0.375" y1="-3.95" x2="1.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="0.75" y1="-4.15" x2="1.75" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="0.825" y1="-4.075" x2="1.675" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="0.875" y1="-3.95" x2="1.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="1.25" y1="-4.15" x2="2.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="1.325" y1="-4.075" x2="2.175" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="1.375" y1="-3.95" x2="2.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="1.75" y1="-4.15" x2="2.75" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="1.825" y1="-4.075" x2="2.675" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="1.875" y1="-3.95" x2="2.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="2.25" y1="-4.15" x2="3.25" y2="-3.75" layer="29" rot="R90"/>
<rectangle x1="2.325" y1="-4.075" x2="3.175" y2="-3.825" layer="31" rot="R90"/>
<rectangle x1="2.375" y1="-3.95" x2="3.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="3.45" y1="-2.95" x2="4.45" y2="-2.55" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-2.875" x2="4.375" y2="-2.625" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-2.85" x2="4.225" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="-2.45" x2="4.45" y2="-2.05" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-2.375" x2="4.375" y2="-2.125" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-2.35" x2="4.225" y2="-2.15" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="-1.95" x2="4.45" y2="-1.55" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-1.875" x2="4.375" y2="-1.625" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-1.85" x2="4.225" y2="-1.65" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="-1.45" x2="4.45" y2="-1.05" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-1.375" x2="4.375" y2="-1.125" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-1.35" x2="4.225" y2="-1.15" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="-0.95" x2="4.45" y2="-0.55" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-0.875" x2="4.375" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-0.85" x2="4.225" y2="-0.65" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="-0.45" x2="4.45" y2="-0.05" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-0.375" x2="4.375" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="-0.35" x2="4.225" y2="-0.15" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="0.05" x2="4.45" y2="0.45" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="0.125" x2="4.375" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="0.15" x2="4.225" y2="0.35" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="0.55" x2="4.45" y2="0.95" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="0.625" x2="4.375" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="0.65" x2="4.225" y2="0.85" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="1.05" x2="4.45" y2="1.45" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="1.125" x2="4.375" y2="1.375" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="1.15" x2="4.225" y2="1.35" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="1.55" x2="4.45" y2="1.95" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="1.625" x2="4.375" y2="1.875" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="1.65" x2="4.225" y2="1.85" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="2.05" x2="4.45" y2="2.45" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="2.125" x2="4.375" y2="2.375" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="2.15" x2="4.225" y2="2.35" layer="51" rot="R180"/>
<rectangle x1="3.45" y1="2.55" x2="4.45" y2="2.95" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="2.625" x2="4.375" y2="2.875" layer="31" rot="R180"/>
<rectangle x1="3.475" y1="2.65" x2="4.225" y2="2.85" layer="51" rot="R180"/>
<rectangle x1="2.25" y1="3.75" x2="3.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="2.325" y1="3.825" x2="3.175" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="2.375" y1="3.75" x2="3.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="1.75" y1="3.75" x2="2.75" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="1.825" y1="3.825" x2="2.675" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="1.875" y1="3.75" x2="2.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="1.25" y1="3.75" x2="2.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="1.325" y1="3.825" x2="2.175" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="1.375" y1="3.75" x2="2.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="0.75" y1="3.75" x2="1.75" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="0.825" y1="3.825" x2="1.675" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="0.875" y1="3.75" x2="1.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="0.25" y1="3.75" x2="1.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="0.325" y1="3.825" x2="1.175" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="0.375" y1="3.75" x2="1.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-0.25" y1="3.75" x2="0.75" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-0.175" y1="3.825" x2="0.675" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-0.125" y1="3.75" x2="0.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-0.75" y1="3.75" x2="0.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-0.675" y1="3.825" x2="0.175" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-0.625" y1="3.75" x2="0.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-1.25" y1="3.75" x2="-0.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-1.175" y1="3.825" x2="-0.325" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-1.125" y1="3.75" x2="-0.375" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-1.75" y1="3.75" x2="-0.75" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-1.675" y1="3.825" x2="-0.825" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-1.625" y1="3.75" x2="-0.875" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-2.25" y1="3.75" x2="-1.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-2.175" y1="3.825" x2="-1.325" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-2.125" y1="3.75" x2="-1.375" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-2.75" y1="3.75" x2="-1.75" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-2.675" y1="3.825" x2="-1.825" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-2.625" y1="3.75" x2="-1.875" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-3.25" y1="3.75" x2="-2.25" y2="4.15" layer="29" rot="R270"/>
<rectangle x1="-3.175" y1="3.825" x2="-2.325" y2="4.075" layer="31" rot="R270"/>
<rectangle x1="-3.125" y1="3.75" x2="-2.375" y2="3.95" layer="51" rot="R270"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="CSTCE8M00G52A-R0">
<description>Source: http://search.murata.co.jp/Ceramy/CatalogshowpageAction.do?sDirnm=A07X&amp;sFilnm=81G07006&amp;sType=2&amp;sLang=en&amp;sNHinnm=CSTCR6M00G53Z-R0&amp;sCapt=Standard_Land_Pattern_Dimensions</description>
<wire x1="-2.2" y1="-0.95" x2="2.2" y2="-0.95" width="0.1016" layer="51"/>
<wire x1="2.2" y1="-0.95" x2="2.2" y2="0.95" width="0.1016" layer="21"/>
<wire x1="2.2" y1="0.95" x2="-2.2" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-2.2" y1="0.95" x2="-2.2" y2="-0.95" width="0.1016" layer="21"/>
<smd name="1" x="-1.2" y="0" dx="0.5" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="0" dx="0.5" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="3" x="1.2" y="0" dx="0.5" dy="1.5" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.8" x2="0.4" y2="1.3" layer="29"/>
<rectangle x1="-0.4" y1="-1.3" x2="0.4" y2="-0.8" layer="29" rot="R180"/>
<rectangle x1="-0.2" y1="-0.8" x2="0.2" y2="0.8" layer="29"/>
<rectangle x1="-1.646" y1="0.8" x2="-0.846" y2="1.3" layer="29"/>
<rectangle x1="-1.646" y1="-1.3" x2="-0.846" y2="-0.8" layer="29" rot="R180"/>
<rectangle x1="-1.446" y1="-0.8" x2="-1.046" y2="0.8" layer="29"/>
<rectangle x1="0.846" y1="0.8" x2="1.646" y2="1.3" layer="29"/>
<rectangle x1="0.846" y1="-1.3" x2="1.646" y2="-0.8" layer="29" rot="R180"/>
<rectangle x1="1.046" y1="-0.8" x2="1.446" y2="0.8" layer="29"/>
<text x="-2.794" y="-0.508" size="0.8128" layer="21">*</text>
</package>
<package name="LFXTAL003000">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="3" x="3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<smd name="4" x="-3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<text x="-4.0192" y="2.815" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.0192" y="-4.593" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.5" y1="2.5" x2="4.73" y2="2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="2.5" x2="4.73" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4.74" y1="-2.5" x2="-4.74" y2="2.46" width="0.127" layer="21"/>
<wire x1="-5.5" y1="2.5" x2="-5.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<circle x="-5.05" y="-2.87" radius="0.2" width="0" layer="21"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="SJ_2S">
<description>Small solder jumper with big paste layer so it will short during reflow.</description>
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.1524" layer="21"/>
<wire x1="0.8" y1="1" x2="1.1" y2="0.75" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.1" y1="0.75" x2="-0.8" y2="1" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.75" x2="-0.8" y2="-1" width="0.1524" layer="21" curve="90.114706"/>
<wire x1="0.8" y1="-1" x2="1.1" y2="-0.75" width="0.1524" layer="21" curve="90"/>
<wire x1="1.1" y1="-0.75" x2="1.1" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-1.1" y1="-0.75" x2="-1.1" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.1524" layer="21"/>
<rectangle x1="-1.2192" y1="-1.143" x2="1.2192" y2="1.143" layer="31"/>
<smd name="1" x="-0.4119" y="0" dx="0.635" dy="1.27" layer="1"/>
<smd name="2" x="0.4119" y="0" dx="0.635" dy="1.27" layer="1"/>
<text x="-0.9498" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9498" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-NOTRACE">
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90.076445"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90.03821"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90.03821"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90.03821"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" layer="31"/>
<smd name="1" x="-0.4009" y="0" dx="0.635" dy="1.27" layer="1" rot="R180" cream="no"/>
<smd name="2" x="0.4127" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.9525" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-NO">
<description>Small solder jumper with no paste layer so it will open after reflow.</description>
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.908" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.908" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-PTH">
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778"/>
<text x="-2.54" y="-1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-TRACE">
<description>Solder jumper, small, shorted with trace. No paste layer. Trace is cuttable.</description>
<wire x1="0.8255" y1="-1.016" x2="-0.8255" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="0.8255" y1="1.016" x2="1.0795" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.0795" y1="0.762" x2="-0.8255" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.0795" y1="-0.762" x2="-0.8255" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8255" y1="-1.016" x2="1.0795" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8255" y1="1.016" x2="0.8255" y2="1.016" width="0.2032" layer="21"/>
<wire x1="-0.381" y1="0" x2="0.381" y2="0" width="0.2032" layer="1"/>
<smd name="1" x="-0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.9525" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-TRACE-PTH">
<rectangle x1="-0.3048" y1="-1.27" x2="0.3048" y2="1.27" layer="30"/>
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778"/>
<smd name="P$1" x="0" y="0" dx="1.27" dy="0.381" layer="16" rot="R180"/>
<text x="-2.54" y="-1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="J0805">
<description>&lt;b&gt;JUMPER SMD&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="ESP01">
<description>ESP8266 Module 01</description>
<pad name="GND" x="-3.825" y="-5.6375" drill="0.8" shape="square" rot="R270"/>
<pad name="RX" x="3.795" y="-5.6375" drill="0.8" shape="square" rot="R270"/>
<pad name="GPIO2" x="-1.285" y="-5.6375" drill="0.8" rot="R270"/>
<pad name="GPIO0" x="1.255" y="-5.6375" drill="0.8" rot="R270"/>
<pad name="RST" x="1.255" y="-8.1775" drill="0.8" rot="R270"/>
<pad name="CH_PD" x="-1.285" y="-8.1775" drill="0.8" rot="R270"/>
<pad name="TX" x="-3.825" y="-8.1775" drill="0.8" rot="R270"/>
<pad name="VCC" x="3.795" y="-8.1775" drill="0.8" rot="R270"/>
<wire x1="-7" y1="15" x2="-7" y2="7.38" width="0.127" layer="21"/>
<wire x1="-7" y1="7.38" x2="-7" y2="-9.765" width="0.127" layer="21"/>
<wire x1="-7" y1="-9.765" x2="7.2875" y2="-9.765" width="0.127" layer="21"/>
<wire x1="7.2875" y1="-9.765" x2="7.2875" y2="7.38" width="0.127" layer="21"/>
<wire x1="7.2875" y1="7.38" x2="7.2875" y2="15" width="0.127" layer="21"/>
<wire x1="7.2875" y1="15" x2="-7" y2="15" width="0.127" layer="21"/>
<wire x1="-7" y1="7.38" x2="7.2875" y2="7.38" width="0.127" layer="21"/>
<text x="-5.73" y="9.92" size="2.54" layer="21">ESP-01</text>
<text x="-7" y="16" size="1.27" layer="25">&gt;Name</text>
<text x="-7" y="-11" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="ESP-07/12">
<wire x1="0" y1="0" x2="0" y2="22" width="0.127" layer="21"/>
<wire x1="0" y1="22" x2="16" y2="22" width="0.127" layer="21"/>
<wire x1="16" y1="22" x2="16" y2="0" width="0.127" layer="21"/>
<wire x1="16" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="22" x2="16" y2="22" width="0.127" layer="39"/>
<wire x1="16" y1="22" x2="16" y2="18" width="0.127" layer="39"/>
<wire x1="0" y1="18" x2="0" y2="22" width="0.127" layer="39"/>
<smd name="TXD" x="16.365" y="16" dx="2.54" dy="1.27" layer="1"/>
<smd name="RXD" x="16.365" y="14" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO4" x="16.365" y="12" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO5" x="16.365" y="10" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO0" x="16.365" y="8" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO2" x="16.365" y="6" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO15" x="16.365" y="4" dx="2.54" dy="1.27" layer="1"/>
<smd name="GND" x="16.365" y="2" dx="2.54" dy="1.27" layer="1"/>
<smd name="VCC" x="-0.365" y="2" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO13" x="-0.365" y="4" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO12" x="-0.365" y="6" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO14" x="-0.365" y="8" dx="2.54" dy="1.27" layer="1"/>
<smd name="GPIO16" x="-0.365" y="10" dx="2.54" dy="1.27" layer="1"/>
<smd name="CH_PD" x="-0.365" y="12" dx="2.54" dy="1.27" layer="1"/>
<smd name="ADC" x="-0.365" y="14" dx="2.54" dy="1.27" layer="1"/>
<smd name="REST" x="-0.365" y="16" dx="2.54" dy="1.27" layer="1"/>
<text x="2.54" y="15.24" size="1.27" layer="21">ESP-07/12</text>
<wire x1="7.62" y1="20.32" x2="13.97" y2="20.32" width="2.54" layer="51"/>
<wire x1="2.54" y1="20.32" x2="3.81" y2="20.32" width="2.54" layer="51"/>
</package>
<package name="RTRIM3364W">
<description>&lt;b&gt;Trimm resistor&lt;/b&gt; BOURNS&lt;p&gt;
SMD Cermet trimmer</description>
<wire x1="-2.3" y1="-1.85" x2="-2.3" y2="1.85" width="0.254" layer="51"/>
<wire x1="-2.3" y1="1.85" x2="2.3" y2="1.85" width="0.254" layer="51"/>
<wire x1="2.3" y1="1.85" x2="2.3" y2="-1.85" width="0.254" layer="51"/>
<wire x1="2.3" y1="-1.85" x2="-2.3" y2="-1.85" width="0.254" layer="51"/>
<wire x1="-2.3" y1="-1.85" x2="-2.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="-2.3" y1="1.85" x2="-1.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="2.3" y1="-1.85" x2="2.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="2.3" y1="1.85" x2="1.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-1.85" x2="0.4" y2="-1.85" width="0.254" layer="21"/>
<circle x="1.2" y="0.65" radius="0.7" width="0.1016" layer="51"/>
<smd name="A" x="-1.275" y="-1.45" dx="1.3" dy="1.6" layer="1"/>
<smd name="E" x="1.275" y="-1.45" dx="1.3" dy="1.6" layer="1"/>
<smd name="S" x="0" y="2.085" dx="2" dy="1.6" layer="1"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.55" y1="0.55" x2="1.85" y2="0.75" layer="51"/>
<rectangle x1="-1.8" y1="-2.1" x2="-0.75" y2="-1.95" layer="51"/>
<rectangle x1="0.75" y1="-2.1" x2="1.8" y2="-1.95" layer="51"/>
<rectangle x1="-0.75" y1="1.95" x2="0.75" y2="2.1" layer="51"/>
</package>
<package name="LED_1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SWITCH_DTSM6">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="T1" x="-3.4" y="2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T2" x="3.4" y="2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T4" x="3.4" y="-2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T3" x="-3.4" y="-2.3" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI-USB">
<description>&lt;b&gt;USB On-The-Go (OTG)&lt;/b&gt;&lt;p&gt;
USB On-The-Go (OTG)
These Molex range of USB OTG (On the Go) connectors are compatible with the On-The-Go supplement (Rev. 1.0) to the USB 2.0 specification. The needs of various portable and mobile equipment are met by these Molex OTG USB connectors by operating at speeds up to 480 Mbps coupled with a compact light-weight design.
Features and Benefits:
• They are about one-eighth the size of a standard USB-B connector
• Excellent RFI/EMI performance
• 5-pin design (one ID pin)
• Through-hole and SMT versions available
• Cable assemblies available
• Voltage Rating of 30V
• Current Rating of 1A
• Expected Life Cycles are 5000</description>
<wire x1="-5.9182" y1="3.8416" x2="-3.6879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="3.8416" x2="-3.6879" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="4.8799" x2="-3.3245" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.8799" x2="-3.3245" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.4646" x2="-2.7015" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.4646" x2="-2.7015" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.8799" x2="-2.3093" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="4.8799" x2="-2.3093" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-1.5825" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="3.8416" x2="0.519" y2="4.0492" width="0.1016" layer="21" curve="-90"/>
<wire x1="0.519" y1="4.0492" x2="0.519" y2="4.205" width="0.1016" layer="21"/>
<wire x1="0.519" y1="4.205" x2="2.907" y2="4.205" width="0.1016" layer="51"/>
<wire x1="2.907" y1="4.205" x2="3.4781" y2="3.6339" width="0.1016" layer="51" curve="-90"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="3.0627" y2="3.5821" width="0.1016" layer="51" curve="-90"/>
<wire x1="3.0627" y1="3.5821" x2="3.0627" y2="3.011" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="3.011" x2="3.4261" y2="3.011" width="0.1016" layer="21"/>
<wire x1="1.713" y1="4.2569" x2="1.713" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="1.713" y1="4.8799" x2="2.1283" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.8799" x2="2.1283" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.4646" x2="2.6474" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.4646" x2="2.6474" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.8799" x2="3.0627" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="4.8799" x2="3.0627" y2="4.2569" width="0.1016" layer="51"/>
<wire x1="0.5709" y1="1.7651" x2="0.5709" y2="-1.765" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="-1.8169" x2="1.0381" y2="1.817" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="1.817" x2="0.8305" y2="2.0246" width="0.1016" layer="21" curve="90.055225"/>
<wire x1="0.8305" y1="2.0246" x2="0.8304" y2="2.0246" width="0.1016" layer="21"/>
<wire x1="0.8304" y1="2.0246" x2="0.5709" y2="1.7651" width="0.1016" layer="21" curve="89.955858"/>
<wire x1="1.5573" y1="-2.0246" x2="3.4261" y2="-2.0246" width="0.1016" layer="21"/>
<wire x1="3.0627" y1="-1.9726" x2="3.0627" y2="1.9727" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.6879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-3.8414" x2="-3.6879" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-4.8797" x2="-3.3245" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.8797" x2="-3.3245" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.4644" x2="-2.7015" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.4644" x2="-2.7015" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.8797" x2="-2.3093" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-4.8797" x2="-2.3093" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-3.8414" x2="2.8032" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="-3.8414" x2="0.519" y2="-4.049" width="0.1016" layer="21" curve="90"/>
<wire x1="0.519" y1="-4.049" x2="0.519" y2="-4.2048" width="0.1016" layer="21"/>
<wire x1="0.519" y1="-4.2048" x2="2.907" y2="-4.2048" width="0.1016" layer="51"/>
<wire x1="2.907" y1="-4.2048" x2="3.4781" y2="-3.6337" width="0.1016" layer="51" curve="90.020069"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="-3.8414" x2="3.0627" y2="-3.5819" width="0.1016" layer="51" curve="90.044176"/>
<wire x1="3.0627" y1="-3.5819" x2="3.0627" y2="-3.0108" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-3.0108" x2="3.4261" y2="-3.0108" width="0.1016" layer="21"/>
<wire x1="1.713" y1="-4.2567" x2="1.713" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-4.8797" x2="2.1283" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.8797" x2="2.1283" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.4644" x2="2.6474" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.4644" x2="2.6474" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.8797" x2="3.0627" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-4.8797" x2="3.0627" y2="-4.2567" width="0.1016" layer="51"/>
<wire x1="1.0381" y1="-1.8168" x2="0.8305" y2="-2.0244" width="0.1016" layer="21" curve="-90.055225"/>
<wire x1="0.8304" y1="-2.0244" x2="0.5709" y2="-1.7649" width="0.1016" layer="21" curve="-89.867677"/>
<wire x1="1.5573" y1="-1.9725" x2="1.5573" y2="2.0248" width="0.1016" layer="51"/>
<wire x1="1.5573" y1="2.0248" x2="3.4261" y2="2.0248" width="0.1016" layer="21"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-1.5826" y1="-3.8414" x2="0.7267" y2="-3.8415" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-4.4146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-4.4147" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="-2.3093" y1="3.8416" x2="0.7265" y2="3.8415" width="0.1016" layer="51"/>
<wire x1="3.4781" y1="-2.0245" x2="3.4781" y2="-3.0109" width="0.1016" layer="21"/>
<wire x1="3.4781" y1="3.634" x2="3.478" y2="-3.0109" width="0.1016" layer="51"/>
<wire x1="3.4782" y1="3.011" x2="3.4782" y2="2.0246" width="0.1016" layer="21"/>
<smd name="M1" x="-3" y="-4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M2" x="-3" y="4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M4" x="2.9" y="-4.45" dx="3.3" dy="2" layer="1"/>
<smd name="M3" x="2.9" y="4.45" dx="3.3" dy="2" layer="1"/>
<smd name="1" x="3" y="1.6" dx="3.1" dy="0.5" layer="1"/>
<smd name="2" x="3" y="0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="3" x="3" y="0" dx="3.1" dy="0.5" layer="1"/>
<smd name="4" x="3" y="-0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="5" x="3" y="-1.6" dx="3.1" dy="0.5" layer="1"/>
<text x="-4.445" y="5.715" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;Smal Outline Transistor&lt;/b&gt;</description>
<wire x1="-3.124" y1="1.731" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="-3.124" y1="1.731" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<smd name="1" x="-2.2606" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="2" x="0.0254" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="3" x="2.3114" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="4" x="0" y="3.1496" dx="3.81" dy="2.0066" layer="1"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.794" y="-5.842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="1.778" x2="1.524" y2="3.302" layer="51"/>
<rectangle x1="-2.667" y1="-3.302" x2="-1.905" y2="-1.778" layer="51"/>
<rectangle x1="1.905" y1="-3.302" x2="2.667" y2="-1.778" layer="51"/>
<rectangle x1="-0.381" y1="-3.302" x2="0.381" y2="-1.778" layer="51"/>
</package>
<package name="MSOP8">
<description>&lt;b&gt;MSOP8&lt;/b&gt;</description>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.2032" layer="21"/>
<circle x="-1.15" y="-1.15" radius="0.2" width="0" layer="21"/>
<smd name="1" x="-0.975" y="-2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="2" x="-0.325" y="-2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="3" x="0.325" y="-2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="4" x="0.975" y="-2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="5" x="0.975" y="2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="7" x="-0.325" y="2.05" dx="0.4" dy="1.2" layer="1"/>
<smd name="8" x="-0.975" y="2.05" dx="0.4" dy="1.2" layer="1"/>
<text x="-1.724" y="-1.504" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="2.828" y="-1.612" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.125" y1="-2.45" x2="-0.825" y2="-1.55" layer="51"/>
<rectangle x1="-0.475" y1="-2.45" x2="-0.175" y2="-1.55" layer="51"/>
<rectangle x1="0.175" y1="-2.45" x2="0.475" y2="-1.55" layer="51"/>
<rectangle x1="0.825" y1="-2.45" x2="1.125" y2="-1.55" layer="51"/>
<rectangle x1="0.825" y1="1.55" x2="1.125" y2="2.45" layer="51"/>
<rectangle x1="0.175" y1="1.55" x2="0.475" y2="2.45" layer="51"/>
<rectangle x1="-0.475" y1="1.55" x2="-0.175" y2="2.45" layer="51"/>
<rectangle x1="-1.125" y1="1.55" x2="-0.825" y2="2.45" layer="51"/>
</package>
<package name="DLG-0504-4R7">
<smd name="P$1" x="0" y="0" dx="2.15" dy="5.5" layer="1"/>
<smd name="P$2" x="3.75" y="0" dx="2.15" dy="5.5" layer="1"/>
<text x="0" y="1.3" size="0.6096" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3" size="0.6096" layer="27" font="vector" ratio="12" rot="R180" align="bottom-center">&gt;VALUE</text>
<wire x1="-2" y1="3.3" x2="5.8" y2="3.3" width="0.127" layer="21"/>
<wire x1="5.8" y1="3.3" x2="5.8" y2="-3.3" width="0.127" layer="21"/>
<wire x1="5.8" y1="-3.3" x2="-2" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.3" x2="-2" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.2" y1="1.4" x2="-1.2" y2="0" width="0.127" layer="51"/>
<wire x1="-1.2" y1="1.4" x2="0.4" y2="3" width="0.127" layer="51" curve="-90"/>
<wire x1="0.4" y1="3" x2="1.1" y2="3" width="0.127" layer="51"/>
<wire x1="1.1" y1="3" x2="1.3" y2="2.8" width="0.127" layer="51" curve="-90"/>
<wire x1="1.3" y1="2.8" x2="1.3" y2="2.3" width="0.127" layer="51"/>
<wire x1="1.3" y1="2.3" x2="1.6" y2="2" width="0.127" layer="51" curve="90"/>
<wire x1="1.6" y1="2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="5.2" y2="0" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.6" y2="3" width="0.127" layer="51" curve="90"/>
<wire x1="3.6" y1="3" x2="2.9" y2="3" width="0.127" layer="51"/>
<wire x1="2.9" y1="3" x2="2.7" y2="2.8" width="0.127" layer="51" curve="90"/>
<wire x1="2.7" y1="2.8" x2="2.7" y2="2.3" width="0.127" layer="51"/>
<wire x1="2.7" y1="2.3" x2="2.4" y2="2" width="0.127" layer="51" curve="-90"/>
<wire x1="2.4" y1="2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="5.2" y1="-1.4" x2="5.2" y2="0" width="0.127" layer="51"/>
<wire x1="5.2" y1="-1.4" x2="3.6" y2="-3" width="0.127" layer="51" curve="-90"/>
<wire x1="3.6" y1="-3" x2="2.9" y2="-3" width="0.127" layer="51"/>
<wire x1="2.9" y1="-3" x2="2.7" y2="-2.8" width="0.127" layer="51" curve="-90"/>
<wire x1="2.7" y1="-2.8" x2="2.7" y2="-2.3" width="0.127" layer="51"/>
<wire x1="2.7" y1="-2.3" x2="2.4" y2="-2" width="0.127" layer="51" curve="90"/>
<wire x1="2.4" y1="-2" x2="2" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-1.4" x2="-1.2" y2="0" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-1.4" x2="0.4" y2="-3" width="0.127" layer="51" curve="90"/>
<wire x1="0.4" y1="-3" x2="1.1" y2="-3" width="0.127" layer="51"/>
<wire x1="1.1" y1="-3" x2="1.3" y2="-2.8" width="0.127" layer="51" curve="90"/>
<wire x1="1.3" y1="-2.8" x2="1.3" y2="-2.3" width="0.127" layer="51"/>
<wire x1="1.3" y1="-2.3" x2="1.6" y2="-2" width="0.127" layer="51" curve="-90"/>
<wire x1="1.6" y1="-2" x2="2" y2="-2" width="0.127" layer="51"/>
</package>
<package name="SOT23-BEC">
<description>TO-236 ITT Intermetall</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.127" layer="51"/>
<smd name="C" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SO-08">
<description>&lt;B&gt;Small Outline Narrow Plastic Gull Wing&lt;/B&gt;&lt;p&gt;
150-mil body, package type SN</description>
<wire x1="-2.9" y1="3.9" x2="2.9" y2="3.9" width="0.1998" layer="39"/>
<wire x1="2.9" y1="3.9" x2="2.9" y2="-3.9" width="0.1998" layer="39"/>
<wire x1="2.9" y1="-3.9" x2="-2.9" y2="-3.9" width="0.1998" layer="39"/>
<wire x1="-2.9" y1="-3.9" x2="-2.9" y2="3.9" width="0.1998" layer="39"/>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-0.635" size="0.4064" layer="48">IPC SO8</text>
<text x="-1.905" y="0.365" size="0.3048" layer="48">JEDEC MS-012 AA</text>
<rectangle x1="-2.1501" y1="-3.1001" x2="-1.6599" y2="-2" layer="51"/>
<rectangle x1="-0.8801" y1="-3.1001" x2="-0.3899" y2="-2" layer="51"/>
<rectangle x1="0.3899" y1="-3.1001" x2="0.8801" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="-3.1001" x2="2.1501" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="2" x2="2.1501" y2="3.1001" layer="51"/>
<rectangle x1="0.3899" y1="2" x2="0.8801" y2="3.1001" layer="51"/>
<rectangle x1="-0.8801" y1="2" x2="-0.3899" y2="3.1001" layer="51"/>
<rectangle x1="-2.1501" y1="2" x2="-1.6599" y2="3.1001" layer="51"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35"/>
</package>
<package name="T821M114A1S100CEU-B">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-11.43" y1="3.175" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-3.175" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="3.175" x2="-11.43" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.445" x2="-11.43" y2="4.445" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-4.445" x2="8.001" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-4.445" x2="12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.445" x2="-12.7" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-3.175" x2="7.112" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-11.43" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="4.445" x2="11.43" y2="4.699" width="0.1524" layer="21"/>
<wire x1="11.43" y1="4.699" x2="10.16" y2="4.699" width="0.1524" layer="21"/>
<wire x1="10.16" y1="4.445" x2="10.16" y2="4.699" width="0.1524" layer="21"/>
<wire x1="11.43" y1="4.445" x2="12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="10.16" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.699" x2="-11.43" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="4.699" x2="-11.43" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.699" x2="-10.16" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-3.175" x2="5.588" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-3.175" x2="7.112" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-3.175" x2="5.588" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.445" x2="5.08" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-3.937" x2="8.001" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-3.937" x2="7.112" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-3.429" x2="11.684" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="11.684" y1="-3.429" x2="11.684" y2="3.429" width="0.0508" layer="21"/>
<wire x1="11.684" y1="3.429" x2="-11.684" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-11.684" y1="3.429" x2="-11.684" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-11.684" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-3.429" x2="5.588" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="5.588" y1="-3.937" x2="5.08" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-3.429" x2="7.112" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-3.937" x2="5.588" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-4.445" x2="-6.858" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="-4.318" x2="-6.858" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="-4.318" x2="-8.382" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-4.445" x2="-8.382" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-4.445" x2="-12.7" y2="-4.445" width="0.1524" layer="21"/>
<text x="-12.7" y="6.38" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.9" y="-8.42" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">14</text>
<text x="-10.16" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-10.16" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<smd name="1" x="-7.62" y="-3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="2" x="-7.62" y="3.44" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="-3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="4" x="-5.1" y="3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="5" x="-2.54" y="-3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="6" x="-2.5" y="3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="7" x="0" y="-3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="8" x="0" y="3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="9" x="2.54" y="-3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="10" x="2.5" y="3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="11" x="5.1" y="-3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="12" x="5.12" y="3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="13" x="7.64" y="-3.43" dx="4.3" dy="1.02" layer="1" rot="R90"/>
<smd name="14" x="7.62" y="3.41" dx="4.3" dy="1.02" layer="1" rot="R90"/>
</package>
<package name="CR2032-SMD">
<wire x1="-15.24" y1="-2.54" x2="-10.16" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-4.572" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-4.572" x2="-4.826" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-10.16" x2="5.08" y2="-10.16" width="0.127" layer="21"/>
<wire x1="5.08" y1="-10.16" x2="10.16" y2="-4.826" width="0.127" layer="21"/>
<wire x1="10.16" y1="-4.826" x2="10.16" y2="-2.54" width="0.127" layer="21"/>
<wire x1="10.16" y1="-2.54" x2="15.24" y2="-2.54" width="0.127" layer="21"/>
<wire x1="15.24" y1="-2.54" x2="15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="15.24" y1="2.54" x2="10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="9.652" width="0.127" layer="21"/>
<wire x1="10.16" y1="9.652" x2="5.842" y2="9.652" width="0.127" layer="21"/>
<wire x1="5.842" y1="9.652" x2="3.302" y2="6.604" width="0.127" layer="21"/>
<wire x1="3.302" y1="6.604" x2="-3.048" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.048" y1="6.604" x2="-5.334" y2="9.652" width="0.127" layer="21"/>
<wire x1="-5.334" y1="9.652" x2="-10.16" y2="9.652" width="0.127" layer="21"/>
<wire x1="-10.16" y1="9.652" x2="-10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="-10.16" y1="2.54" x2="-15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="-2.54" width="0.127" layer="21"/>
<circle x="-12.954" y="0" radius="0.9158" width="0.127" layer="21"/>
<circle x="12.7" y="0.254" radius="0.9158" width="0.127" layer="21"/>
<smd name="-" x="0" y="0" dx="4.064" dy="4.064" layer="1"/>
<smd name="+$1" x="-12.7" y="0" dx="5.08" dy="5.08" layer="1"/>
<smd name="+$2" x="12.7" y="0" dx="5.08" dy="5.08" layer="1"/>
<text x="-3.556" y="10.302" size="1.6764" layer="25" font="vector">&gt;NAME</text>
<text x="-4.318" y="-12.35" size="1.6764" layer="27" font="vector">&gt;VALUE</text>
<polygon width="0.127" layer="29">
<vertex x="-9" y="0" curve="-90"/>
<vertex x="0" y="9" curve="-90"/>
<vertex x="9" y="0" curve="-90"/>
<vertex x="0" y="-9" curve="-90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="STM32F103C8T6">
<pin name="VBAT" x="-25.4" y="-33.02" length="short"/>
<pin name="PC13-TAMPER-RTC" x="-25.4" y="-17.78" length="short"/>
<pin name="PC14-OSC32_IN" x="-25.4" y="-20.32" length="short"/>
<pin name="PD0-OSC_IN" x="27.94" y="-22.86" length="short" rot="R180"/>
<pin name="PC15-OSC32-OUT" x="-25.4" y="-22.86" length="short"/>
<pin name="PD1-OSC_OUT" x="27.94" y="-25.4" length="short" rot="R180"/>
<pin name="NRST" x="27.94" y="30.48" length="short" rot="R180"/>
<pin name="VSSA" x="27.94" y="-40.64" length="short" rot="R180"/>
<pin name="PA0-WKUP" x="-25.4" y="20.32" length="short"/>
<pin name="VDDA" x="-25.4" y="-43.18" length="short"/>
<pin name="PA1" x="-25.4" y="17.78" length="short"/>
<pin name="PA2" x="-25.4" y="15.24" length="short"/>
<pin name="PA3" x="-25.4" y="12.7" length="short"/>
<pin name="PA4" x="-25.4" y="10.16" length="short"/>
<pin name="PA5" x="-25.4" y="7.62" length="short"/>
<pin name="PA6" x="-25.4" y="5.08" length="short"/>
<pin name="PA7" x="-25.4" y="2.54" length="short"/>
<pin name="PB0" x="27.94" y="20.32" length="short" rot="R180"/>
<pin name="PB1" x="27.94" y="17.78" length="short" rot="R180"/>
<pin name="PB2(BOOT1)" x="27.94" y="15.24" length="short" rot="R180"/>
<pin name="PB10" x="27.94" y="-5.08" length="short" rot="R180"/>
<pin name="PB11" x="27.94" y="-7.62" length="short" rot="R180"/>
<pin name="VSS_1" x="27.94" y="-33.02" length="short" rot="R180"/>
<pin name="VDD_1" x="-25.4" y="-35.56" length="short"/>
<pin name="PB12" x="27.94" y="-10.16" length="short" rot="R180"/>
<pin name="PB13" x="27.94" y="-12.7" length="short" rot="R180"/>
<pin name="PB14" x="27.94" y="-15.24" length="short" rot="R180"/>
<pin name="PB15" x="27.94" y="-17.78" length="short" rot="R180"/>
<pin name="PA8" x="-25.4" y="0" length="short"/>
<pin name="PA9" x="-25.4" y="-2.54" length="short"/>
<pin name="PA10" x="-25.4" y="-5.08" length="short"/>
<pin name="PA11" x="-25.4" y="-7.62" length="short"/>
<pin name="PA12" x="-25.4" y="-10.16" length="short"/>
<pin name="SWIO" x="27.94" y="25.4" length="short" rot="R180"/>
<pin name="VSS_2" x="27.94" y="-35.56" length="short" rot="R180"/>
<pin name="VDD_2" x="-25.4" y="-40.64" length="short"/>
<pin name="SWCLK" x="27.94" y="27.94" length="short" rot="R180"/>
<pin name="PA15" x="-25.4" y="-12.7" length="short"/>
<pin name="PB3" x="27.94" y="12.7" length="short" rot="R180"/>
<pin name="PB4" x="27.94" y="10.16" length="short" rot="R180"/>
<pin name="PB5" x="27.94" y="7.62" length="short" rot="R180"/>
<pin name="PB6" x="27.94" y="5.08" length="short" rot="R180"/>
<pin name="PB7" x="27.94" y="2.54" length="short" rot="R180"/>
<pin name="BOOT0" x="27.94" y="-45.72" length="short" rot="R180"/>
<pin name="PB8" x="27.94" y="0" length="short" rot="R180"/>
<pin name="PB9" x="27.94" y="-2.54" length="short" rot="R180"/>
<pin name="VSS_3" x="27.94" y="-38.1" length="short" rot="R180"/>
<pin name="VDD_3" x="-25.4" y="-38.1" length="short"/>
<wire x1="-22.86" y1="22.86" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="-22.86" y1="22.86" x2="-22.86" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-30.48" x2="25.4" y2="-30.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="-30.48" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<text x="-7.62" y="-2.54" size="5.08" layer="94">Ports</text>
<wire x1="-22.86" y1="22.86" x2="-22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="-22.86" y1="33.02" x2="25.4" y2="33.02" width="0.254" layer="94"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="22.86" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-30.48" x2="-22.86" y2="-48.26" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-48.26" x2="25.4" y2="-48.26" width="0.254" layer="94"/>
<wire x1="25.4" y1="-48.26" x2="25.4" y2="-30.48" width="0.254" layer="94"/>
<text x="-7.62" y="25.4" size="5.08" layer="94">SWD</text>
<text x="-7.62" y="-40.64" size="5.08" layer="94">Power</text>
<text x="-22.86" y="35.56" size="1.778" layer="95">&gt;NAME</text>
<text x="-22.86" y="-53.34" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="C0805">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CSTCE8M00G52A-R0">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="-0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="-1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-2.286" x2="1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.286" x2="2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.286" x2="-1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.286" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.302" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<circle x="-3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="0" y="-3.302" radius="0.254" width="0" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LFXTAL003000">
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.889" y1="1.524" x2="0.889" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.524" x2="1.651" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="-1.524" x2="1.651" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.524" x2="0.889" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="0" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="2.286" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.254" y1="1.778" x2="0.254" y2="0" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="2.54" x2="4.318" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="2.54" x2="4.318" y2="1.905" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="-1.905" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.1524" layer="94" style="shortdash"/>
<text x="-2.54" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="R0805">
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<text x="3.81" y="1.4986" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="3.81" y="-3.302" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="J">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ESP01">
<description>ESP8266 Wifi module 01</description>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<pin name="GND" x="-17.78" y="-7.62" visible="pin" length="middle"/>
<pin name="GPIO2" x="17.78" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO0" x="17.78" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="RXD" x="17.78" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="TXD" x="17.78" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="CH_PD" x="-17.78" y="2.54" visible="pin" length="middle"/>
<pin name="RST" x="-17.78" y="7.62" visible="pin" length="middle"/>
<pin name="VCC" x="0" y="15.24" visible="pin" length="middle" rot="R270"/>
<text x="-12.7" y="12.7" size="1.778" layer="95">&gt;Name</text>
<text x="-12.7" y="-12.7" size="1.778" layer="95">&gt;Value</text>
</symbol>
<symbol name="ESP-07/12">
<pin name="GND" x="-17.78" y="-12.7" visible="pin" length="middle"/>
<pin name="GPIO15" x="17.78" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO2" x="17.78" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO0" x="17.78" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO4" x="17.78" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO5" x="17.78" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="RXD" x="17.78" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="TXD" x="17.78" y="22.86" visible="pin" length="middle" rot="R180"/>
<pin name="RST" x="-17.78" y="22.86" visible="pin" length="middle"/>
<pin name="ADC" x="-17.78" y="12.7" visible="pin" length="middle"/>
<pin name="CH_PD" x="-17.78" y="17.78" visible="pin" length="middle"/>
<pin name="GPIO16" x="17.78" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO14" x="17.78" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO12" x="17.78" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO13" x="17.78" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="VCC" x="0" y="30.48" length="middle" rot="R270"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<text x="-17.78" y="27.94" size="1.27" layer="94">&gt;NAME</text>
</symbol>
<symbol name="RTRIM">
<wire x1="0.762" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-1.8796" y2="1.7526" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="-1.651" y2="2.413" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="E" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
<symbol name="LED_1210">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="SWITCH_DTSM6">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="3.175" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="T2" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="T4" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="T3" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="T1" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2"/>
</symbol>
<symbol name="MINI-USB">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="pas"/>
</symbol>
<symbol name="NCP1117ST33T3G">
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="2.54" size="1.778" layer="96" ratio="10">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="0" length="short" direction="pwr"/>
<pin name="VOUT" x="10.16" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="0" y="-10.16" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="MCP1642B">
<pin name="EN" x="-15.24" y="0" length="middle"/>
<pin name="VFB" x="15.24" y="0" length="middle" direction="in" rot="R180"/>
<pin name="PG" x="15.24" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="VOUT" x="15.24" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="SW" x="0" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="PGND" x="-15.24" y="-10.16" length="middle" direction="pwr"/>
<pin name="SGND" x="15.24" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-15.24" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DLG-0504-4R7">
<text x="-0.02" y="4.46" size="2.1844" layer="97">&gt;Name</text>
<text x="-0.22" y="-7.94" size="2.1844" layer="97">&gt;Value</text>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="5.08" visible="off" length="point"/>
<pin name="2" x="-2.54" y="-7.62" visible="off" length="point"/>
</symbol>
<symbol name="PNP">
<wire x1="-0.4539" y1="1.6779" x2="-0.9619" y2="2.5941" width="0.1524" layer="94"/>
<wire x1="-0.9619" y1="2.5941" x2="-2.0241" y2="1.478" width="0.1524" layer="94"/>
<wire x1="-2.0241" y1="1.478" x2="-0.4539" y2="1.6779" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.732" y2="2.1239" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.778" x2="-1.016" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.413" x2="-1.778" y2="1.651" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.651" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="-1.016" y2="2.159" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.159" x2="-1.397" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.397" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="94"/>
<text x="-12.7" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="2.54" layer="94"/>
<pin name="B" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="TL431">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-0.635" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-1.27" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<text x="-3.81" y="-3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-5.715" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="R" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="T821M114A1S100CEU-B">
<wire x1="3.81" y1="-10.16" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="2.54" y1="5.08" x2="3.175" y2="5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<text x="-3.81" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CR2032">
<pin name="+" x="-2.54" y="2.54" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pwr" rot="R90"/>
<pin name="+1" x="2.54" y="2.54" visible="off" length="short" direction="pwr" rot="R270"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.1524" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.1524" layer="94"/>
<text x="3.81" y="1.27" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="3.81" y="-6.35" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<text x="15.24" y="5.08" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="15.24" y="-5.08" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F103C8T6" prefix="U">
<description>&lt;b&gt;STMicroelectronics STM32F103C8T6, 32bit ARM Cortex M3 Microcontroller, 72MHz, 64 kB Flash, 48-pin LQFP&lt;/b&gt;&lt;p&gt;
STMicroelectronics devices STM32F103 Cortex-M3 core with 72 MHz CPU speed and up to 1 MB of Flash. Comprising of motor control peripherals plus CAN and USB full speed interfaces. The STM32 series ARM Cortex-M3 32 bit Flash microcontroller operates low power, low voltage and combines great performance with real time capabilities. In a range of package types available for your embedded application. The MCU architecture has an easy to use STM32 platform and will work in applications including motor drives; PC and gaming; HVAC and Industrial applications.
32-bit RISC
Pin to pin software compatible
SRAM up to 96 Kb
Flash up to 1MB
Power Supply: 2 V to 3.6 V
Temperature ranges: -40 to +85 °C or -40 to +105 °C</description>
<gates>
<gate name="G$1" symbol="STM32F103C8T6" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="LQFP48">
<connects>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="NRST" pad="7"/>
<connect gate="G$1" pin="PA0-WKUP" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11" pad="32"/>
<connect gate="G$1" pin="PA12" pad="33"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA7" pad="17"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB10" pad="21"/>
<connect gate="G$1" pin="PB11" pad="22"/>
<connect gate="G$1" pin="PB12" pad="25"/>
<connect gate="G$1" pin="PB13" pad="26"/>
<connect gate="G$1" pin="PB14" pad="27"/>
<connect gate="G$1" pin="PB15" pad="28"/>
<connect gate="G$1" pin="PB2(BOOT1)" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13-TAMPER-RTC" pad="2"/>
<connect gate="G$1" pin="PC14-OSC32_IN" pad="3"/>
<connect gate="G$1" pin="PC15-OSC32-OUT" pad="4"/>
<connect gate="G$1" pin="PD0-OSC_IN" pad="5"/>
<connect gate="G$1" pin="PD1-OSC_OUT" pad="6"/>
<connect gate="G$1" pin="SWCLK" pad="37"/>
<connect gate="G$1" pin="SWIO" pad="34"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VDDA" pad="9"/>
<connect gate="G$1" pin="VDD_1" pad="24"/>
<connect gate="G$1" pin="VDD_2" pad="36"/>
<connect gate="G$1" pin="VDD_3" pad="48"/>
<connect gate="G$1" pin="VSSA" pad="8"/>
<connect gate="G$1" pin="VSS_1" pad="23"/>
<connect gate="G$1" pin="VSS_2" pad="35"/>
<connect gate="G$1" pin="VSS_3" pad="47"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="STM32F103C8T6" constant="no"/>
<attribute name="OC_MOUSER" value="511-STM32F103C8T6" constant="no"/>
<attribute name="OC_RS" value="402-279" constant="no"/>
<attribute name="OC_TME" value="STM32F103C8T6" constant="no"/>
<attribute name="PACKAGE" value="LQFP48" constant="no"/>
<attribute name="SUPPLIER" value="STMicroelectronics" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C0805" prefix="C">
<description>&lt;b&gt;Samsung 0805 X7R/Y5V MLCC series reels&lt;p&gt;
0805 Range&lt;p&gt;&lt;/b&gt;
Nickel barrier terminations covered with a layer of plated Tin (NiSn),Applications include mobile phones, video and tuner designs,COG/NPO is the most popular formulation of the "temperature compensating", EIA Class I ceramic materials,X7R, X5R formulations are called "temperature stable" ceramics and fall into the EIA Class II materials,Y5V, Z5U formulations are for general purpose use in a limited temperature range, EIA Class II materials; these characteristics are ideal for decoupling applications.</description>
<gates>
<gate name="G$1" symbol="C0805" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_100NF">
<attribute name="MPN" value="CL21B104KBCNNNC" constant="no"/>
<attribute name="OC_RS" value="766-6177" constant="no"/>
<attribute name="OC_TME" value="CL21B104KBCNNNC" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="100nF" constant="no"/>
</technology>
<technology name="_10UF">
<attribute name="MPN" value="CL21A106KQFNNNG" constant="no"/>
<attribute name="OC_RS" value="GRM21BR60J106KE19L" constant="no"/>
<attribute name="OC_TME" value="CL21A106KQFNNNG" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
<technology name="_20PF">
<attribute name="MPN" value="CL10C200JB8NNNC" constant="no"/>
<attribute name="OC_RS" value="766-5815" constant="no"/>
<attribute name="OC_TME" value="CL10C200JB8NNNC" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="20pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CSTCE8M00G52A-R0" prefix="X">
<description>&lt;b&gt;CSTCE CERALOCK® Ceramic Resonators&lt;/b&gt;&lt;p&gt;
These Murata brand CERALOCK® ceramic resonators are ideal for many applications, such as audio-visual and automotive electronics,and are available in a wide range of frequencies.

With built-in load capacitance, this SMD ceramic resonator eliminates the need for an external load capacitor which, combined with its small package size and low profile, results in an economic use of space and the opportunity for high-density mounting.</description>
<gates>
<gate name="G$1" symbol="CSTCE8M00G52A-R0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CSTCE8M00G52A-R0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CSTCE8M00G52A-R0" constant="no"/>
<attribute name="OC_MOUSER" value="721-4827" constant="no"/>
<attribute name="OC_TME" value="CSTCE8M00G52A-R0 " constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="Murata" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LFXTAL003000" prefix="X">
<description>&lt;b&gt;CRYSTALS WITH CAN GROUNDED&lt;/b&gt;&lt;p&gt;
Industry standard SMD package with 5.5mm lead spacing
Suitable for real time clock applications
Plastic encapsulated</description>
<gates>
<gate name="G$1" symbol="LFXTAL003000" x="0" y="0"/>
</gates>
<devices>
<device name="-4-PAD" package="LFXTAL003000">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="LFXTAL003000" constant="no"/>
<attribute name="OC_RS" value="478-9274" constant="no"/>
<attribute name="OC_TME" value="32.768K-12-85SMXR " constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="IQD" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R0805" prefix="R">
<gates>
<gate name="G$1" symbol="R0805" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_MOUSER" value="" constant="no"/>
<attribute name="OC_TME" value="" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER_2WAY" prefix="RJ">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt;
Standard SMD jumper. Used to automate production. Two varients : Normally Open and Normally Closed are the same, but have different paste layers. NC will have a large amount of paste and should jumper during reflow. J0805 is basically 0R resistor.</description>
<gates>
<gate name="1" symbol="J" x="0" y="0"/>
</gates>
<devices>
<device name="NC" package="SJ_2S">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NC2" package="SJ_2S-NOTRACE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO" package="SJ_2S-NO">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="SJ_2S-PTH">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRACE" package="SJ_2S-TRACE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRACE-PTH" package="SJ_2S-TRACE-PTH">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0R" package="J0805">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP01" prefix="U">
<description>ESP8266 Wifi module 01</description>
<gates>
<gate name="G$1" symbol="ESP01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP01">
<connects>
<connect gate="G$1" pin="CH_PD" pad="CH_PD"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GPIO0" pad="GPIO0"/>
<connect gate="G$1" pin="GPIO2" pad="GPIO2"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RXD" pad="RX"/>
<connect gate="G$1" pin="TXD" pad="TX"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP-07/12" prefix="U">
<description>Version 1.0 of the library for the ESP-07/12 full GPIO dev board module


 byME Jon
jonnyradu@gmail.com</description>
<gates>
<gate name="G$1" symbol="ESP-07/12" x="-12.7" y="-7.62" swaplevel="1"/>
</gates>
<devices>
<device name="" package="ESP-07/12">
<connects>
<connect gate="G$1" pin="ADC" pad="ADC"/>
<connect gate="G$1" pin="CH_PD" pad="CH_PD"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GPIO0" pad="GPIO0"/>
<connect gate="G$1" pin="GPIO12" pad="GPIO12"/>
<connect gate="G$1" pin="GPIO13" pad="GPIO13"/>
<connect gate="G$1" pin="GPIO14" pad="GPIO14"/>
<connect gate="G$1" pin="GPIO15" pad="GPIO15"/>
<connect gate="G$1" pin="GPIO16" pad="GPIO16"/>
<connect gate="G$1" pin="GPIO2" pad="GPIO2"/>
<connect gate="G$1" pin="GPIO4" pad="GPIO4"/>
<connect gate="G$1" pin="GPIO5" pad="GPIO5"/>
<connect gate="G$1" pin="RST" pad="REST"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RTRIM" prefix="POT">
<description>&lt;b&gt;200mW 4mm Open Frame 3364W Series&lt;/b&gt;&lt;p&gt;
Bourns® 3364W Series 4mm Square Single turn Cermet SMD Trimpot® Trimming Potentiometer
Miniature (nominal 4mm square)
Excellent mounting efficiency
Suitable for reflow soldering/hand solder
Ideal for automatic assembly
Cross slot top adjuster</description>
<gates>
<gate name="G$1" symbol="RTRIM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RTRIM3364W">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="E" pad="E"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name="3364W-1-203E">
<attribute name="MPN" value="3364W-1-203E" constant="no"/>
<attribute name="OC_MOUSER" value="177-239P" constant="no"/>
<attribute name="OC_TME" value="3364X-1-203E " constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="BOURNS" constant="no"/>
<attribute name="VALUE" value="20k" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="D">
<description>&lt;b&gt;Lite-On C930 Series Chip LED&lt;/b&gt;&lt;p&gt;
The C930 series, from Lite-On, are in the standard 1210 Chip LED package. They are available in a variety of colours with a dome lens. 
Features of the C930 series:
Viewing angle 25°
1210 package
Surface mount (SMT)
Single Colour
Variety of colour options
Dome lens</description>
<gates>
<gate name="G$1" symbol="LED_1210" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="LTST-C930KFKT">
<attribute name="MPN" value="LTST-C930KFKT" constant="no"/>
<attribute name="OC_RS" value="692-1133" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KFKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="ORANGE" constant="no"/>
</technology>
<technology name="LTST-C930KGKT">
<attribute name="MPN" value="LTST-C930KGKT" constant="no"/>
<attribute name="OC_RS" value="692-1136" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KGKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
<technology name="LTST-C930KRKT">
<attribute name="MPN" value="LTST-C930KRKT " constant="no"/>
<attribute name="OC_RS" value="692-1130" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KRKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="RED" constant="no"/>
</technology>
<technology name="LTST-C930KSKT">
<attribute name="MPN" value="LTST-C930KSKT" constant="no"/>
<attribute name="OC_RS" value="692-1149" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KSKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="YELLOW" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH" prefix="SW">
<description>&lt;b&gt;Apem DTSM-6 Series Tact Switches&lt;/b&gt;&lt;p&gt;

Apem DTSM-6 series tactile or tact SPST switches are designed to be used with small currents and feature a sharp click feel and positive tactile feedback. A small movement distance on these tact switches mean that the user experiences a distinct sensation when the switch ’clicks’ into place. This series of Apem tact switches have SMT terminals, low contact resistance and high reliability. The ultra-miniature and lightweight structure of this series of tact switches ensures suitability of high density mounting.
Vertical operating direction
Low contact resistance: 100mΩ
Insulation resistance &gt;100MΩ at 500Vdc
Positive tactile feedback
Long life</description>
<gates>
<gate name="G$1" symbol="SWITCH_DTSM6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH_DTSM6">
<connects>
<connect gate="G$1" pin="T1" pad="T1"/>
<connect gate="G$1" pin="T2" pad="T2"/>
<connect gate="G$1" pin="T3" pad="T3"/>
<connect gate="G$1" pin="T4" pad="T4"/>
</connects>
<technologies>
<technology name="DTSM6">
<attribute name="MPN" value="DTSM63NV" constant="no"/>
<attribute name="OC_RS" value="765-1071" constant="no"/>
<attribute name="OC_TME" value="DTSM-61N-V-B" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="DIPTRONICS" constant="no"/>
<attribute name="VALUE" value="SPST_NO" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI-USB" prefix="X">
<description>&lt;b&gt;USB On-The-Go (OTG)&lt;/b&gt;&lt;p&gt;

These Molex range of USB OTG (On the Go) connectors are compatible with the On-The-Go supplement (Rev. 1.0) to the USB 2.0 specification. The needs of various portable and mobile equipment are met by these Molex OTG USB connectors by operating at speeds up to 480 Mbps coupled with a compact light-weight design.
Features and Benefits:
• They are about one-eighth the size of a standard USB-B connector
• Excellent RFI/EMI performance
• 5-pin design (one ID pin)
• Through-hole and SMT versions available
• Cable assemblies available
• Voltage Rating of 30V
• Current Rating of 1A
• Expected Life Cycles are 5000</description>
<gates>
<gate name="G$1" symbol="MINI-USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MINI-USB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="67503-1020" constant="no"/>
<attribute name="OC_RS" value="666-1099" constant="no"/>
<attribute name="OC_TME" value="MX-67503-1020" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="MOLEX" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NCP1117ST33T3G" prefix="IC">
<description>&lt;b&gt;LDO (Low Dropout) Linear Voltage Regulator - ON Semiconductor&lt;/b&gt;&lt;p&gt;
The Low dropout or LDO is a linear voltage regulator. We have a wide range of linear regulators. The LDO regulator can operate with a small input–output differential voltage. LDO voltage regulators may offer fast transient response, wide input voltage range, low quiescent current and low noise with high PSRR.</description>
<gates>
<gate name="G$1" symbol="NCP1117ST33T3G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="NCP1117ST33T3G" constant="no"/>
<attribute name="OC_RS" value="785-7207" constant="no"/>
<attribute name="OC_TME" value="NCP1117ST33T3G" constant="no"/>
<attribute name="PACKAGE" value="SMD_SOT223" constant="no"/>
<attribute name="SUPPLIER" value="ON Semiconductor" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP1642B" prefix="U">
<description>&lt;b&gt;MCP1642 Synchronous Boost Converter&lt;/b&gt;&lt;p&gt;

The MCP1642 family of devices from Microchip are compact, high-efficiency, fixed-frequency, synchronous step-up DC-DC converters. These regulators are designed for applications powered by alkaline, NiCd, NiMH, Li-Ion or Li-Polymer batteries.
Features&lt;p&gt;
Up to 96% Typical Efficiency&lt;p&gt;
1.8A Typical Peak Input Current Limit&lt;p&gt;
IOUT &gt; 175 mA @ 1.2V VIN, 3.3V VOUT&lt;p&gt;
IOUT &gt; 600 mA @ 2.4V VIN, 3.3V VOUT&lt;p&gt;
IOUT &gt; 800 mA @ 3.3V VIN, 5.0V VOUT&lt;p&gt;
IOUT &gt; 1A @ VIN &gt; 3.6V, 5.0V VOUT&lt;p&gt;
Low Start-up Voltage: 0.65V&lt;p&gt;
Minimum Operating Input Voltage: 0.35V&lt;p&gt;
Maximum Input Voltage &lt; VOUT &lt; 5.5V&lt;p&gt;
Adjustable Output Voltage Range: 1.8V to 5.5V&lt;p&gt;
PWM Operation at 1 MHz&lt;p&gt;
Shutdown Current (All States): &lt; 1 μA&lt;p&gt;
Internal Synchronous Rectifier&lt;p&gt;
Internal Compensation&lt;p&gt;
Inrush Current Limiting and Internal Soft-Start&lt;p&gt;
True Load Disconnect Shutdown (MCP1642B)&lt;p&gt;
Input-to-Output Bypass Option (MCP1642D)&lt;p&gt;
Low Noise, Anti-Ringing Control&lt;p&gt;
Over-temperature Protection&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="MCP1642B" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="MSOP8">
<connects>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="PG" pad="3"/>
<connect gate="G$1" pin="PGND" pad="6"/>
<connect gate="G$1" pin="SGND" pad="7"/>
<connect gate="G$1" pin="SW" pad="5"/>
<connect gate="G$1" pin="VFB" pad="2"/>
<connect gate="G$1" pin="VIN" pad="8"/>
<connect gate="G$1" pin="VOUT" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="MCP1642B-ADJI/MS" constant="no"/>
<attribute name="OC_RS" value="869-6104" constant="no"/>
<attribute name="OC_TME" value="MCP1642B-ADJI/MS" constant="no"/>
<attribute name="PACKAGE" value="SMD_TSOP8" constant="no"/>
<attribute name="SUPPLIER" value="MIRCOCHIP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DLG-0504-4R7" prefix="L">
<gates>
<gate name="G$1" symbol="DLG-0504-4R7" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="DLG-0504-4R7">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="DLG-0504-4R7" constant="no"/>
<attribute name="OC_RS" value="770-0873" constant="no"/>
<attribute name="OC_TME" value="DLG-0504-4R7" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="FERROCORE" constant="no"/>
<attribute name="VALUE" value="4.7uH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PNP" prefix="T">
<gates>
<gate name="G$1" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-BEC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="SOT23">
<attribute name="MPN" value="BC807-25.215 " constant="no"/>
<attribute name="OC_RS" value="436-7896" constant="no"/>
<attribute name="OC_TME" value="BC807-25.215 " constant="no"/>
<attribute name="SUPPLIER" value="NXP" constant="no"/>
</technology>
<technology name="TO92">
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_RS" value="" constant="no"/>
<attribute name="OC_TME" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TL431" prefix="U" uservalue="yes">
<description>&lt;b&gt;VOLTAGE REGULATOR&lt;/b&gt;&lt;p&gt;
Voltage Reference - Texas Instruments
Precison Fixed and Adjustable Voltage Reference ICs utilsing series, shunt or series/shunt topologies and available in both through-hole and surface mount packages. Voltage References are available with initial accuracies of ±0.02 to ±2%.</description>
<gates>
<gate name="G$1" symbol="TL431" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-08">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="R" pad="8"/>
</connects>
<technologies>
<technology name="ACD">
<attribute name="MPN" value="TL431ACD" constant="no"/>
<attribute name="OC_RS" value="517-1927" constant="no"/>
<attribute name="OC_TME" value="TL431ACD" constant="no"/>
<attribute name="PACKAGE" value="SMD_SO8" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="T821M114A1S100CEU-B" prefix="CON" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="T821M114A1S100CEU-B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T821M114A1S100CEU-B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="T821M114A1S100CEU-B" constant="no"/>
<attribute name="OC_RS" value="832-3594" constant="no"/>
<attribute name="OC_TME" value="T821M114A1S100CEU-" constant="no"/>
<attribute name="PACKAGE" value="SMD_14" constant="no"/>
<attribute name="SUPPLIER" value="AMPHENOL" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CR2032" prefix="BAT" uservalue="yes">
<description>&lt;b&gt;Low Profile Coin Cell Holder&lt;/b&gt;&lt;p&gt;
Through-hole SMD coin cell holders which utilise PCB tracking for -ve contact&lt;p&gt;
&lt;b&gt;Holders, Clips Internal Surface Mounting&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="CR2032" x="0" y="0"/>
</gates>
<devices>
<device name="SMT" package="CR2032-SMD">
<connects>
<connect gate="G$1" pin="+" pad="+$1"/>
<connect gate="G$1" pin="+1" pad="+$2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="3003" constant="no"/>
<attribute name="OC_RS" value="219-7960" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="Keystone" constant="no"/>
<attribute name="VALUE" value="CR2032" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VDD">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="V+">
<wire x1="0.889" y1="-1.27" x2="0" y2="0.127" width="0.254" layer="94"/>
<wire x1="0" y1="0.127" x2="-0.889" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-1.27" x2="0.889" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="V+" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VDD" prefix="VDD">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V+" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="V+" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<modules>
<module name="ESP12" prefix="U" dx="30.48" dy="20.32">
<ports>
</ports>
<variantdefs>
</variantdefs>
<parts>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</module>
<module name="ESP12B" prefix="" dx="30.48" dy="20.32">
<ports>
</ports>
<variantdefs>
</variantdefs>
<parts>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</module>
</modules>
<parts>
<part name="U3" library="Marcin_LIB" deviceset="STM32F103C8T6" device=""/>
<part name="VDD8" library="supply1" deviceset="VDD" device=""/>
<part name="C9" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C10" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C11" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="VDD9" library="supply1" deviceset="VDD" device=""/>
<part name="VDD10" library="supply1" deviceset="VDD" device=""/>
<part name="X3" library="Marcin_LIB" deviceset="CSTCE8M00G52A-R0" device="" value="8MHz"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="X2" library="Marcin_LIB" deviceset="LFXTAL003000" device="-4-PAD" value="32kHz"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="C7" library="Marcin_LIB" deviceset="C0805" device="" technology="_20PF" value="20pF"/>
<part name="C8" library="Marcin_LIB" deviceset="C0805" device="" technology="_20PF" value="20pF"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="R9" library="Marcin_LIB" deviceset="R0805" device="" value="1k"/>
<part name="R10" library="Marcin_LIB" deviceset="R0805" device="" value="2k"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="VDD11" library="supply1" deviceset="VDD" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="VDD15" library="supply1" deviceset="VDD" device=""/>
<part name="R18" library="Marcin_LIB" deviceset="R0805" device="" value="1k"/>
<part name="R11" library="Marcin_LIB" deviceset="R0805" device="" value="100k"/>
<part name="RJ2" library="Marcin_LIB" deviceset="JUMPER_2WAY" device="0R" value="Normal/Boot"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="VDD13" library="supply1" deviceset="VDD" device=""/>
<part name="U5" library="Marcin_LIB" deviceset="ESP01" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="U4" library="Marcin_LIB" deviceset="ESP-07/12" device=""/>
<part name="POT3" library="Marcin_LIB" deviceset="RTRIM" device="" technology="3364W-1-203E" value="20k"/>
<part name="POT1" library="Marcin_LIB" deviceset="RTRIM" device="" technology="3364W-1-203E" value="20k"/>
<part name="POT2" library="Marcin_LIB" deviceset="RTRIM" device="" technology="3364W-1-203E" value="20k"/>
<part name="VDD5" library="supply1" deviceset="VDD" device=""/>
<part name="VDD6" library="supply1" deviceset="VDD" device=""/>
<part name="VDD7" library="supply1" deviceset="VDD" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R15" library="Marcin_LIB" deviceset="R0805" device="" value="150R"/>
<part name="R16" library="Marcin_LIB" deviceset="R0805" device="" value="150R"/>
<part name="R20" library="Marcin_LIB" deviceset="R0805" device="" value="150R"/>
<part name="R21" library="Marcin_LIB" deviceset="R0805" device="" value="150R"/>
<part name="D5" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KFKT" value="ORANGE"/>
<part name="D4" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="GREEN"/>
<part name="D3" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KRKT" value="RED"/>
<part name="D2" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KSKT" value="USER LED"/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="SW2" library="Marcin_LIB" deviceset="SWITCH" device="" technology="DTSM6" value="USER1"/>
<part name="SW1" library="Marcin_LIB" deviceset="SWITCH" device="" technology="DTSM6" value="RESET"/>
<part name="R17" library="Marcin_LIB" deviceset="R0805" device="" value="10k"/>
<part name="VDD14" library="supply1" deviceset="VDD" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="C12" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="R14" library="Marcin_LIB" deviceset="R0805" device="" value="10k"/>
<part name="C13" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="X1" library="Marcin_LIB" deviceset="MINI-USB" device=""/>
<part name="R1" library="Marcin_LIB" deviceset="R0805" device="" value="20R"/>
<part name="R2" library="Marcin_LIB" deviceset="R0805" device="" value="20R"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="RJ1" library="Marcin_LIB" deviceset="JUMPER_2WAY" device="0R" value="ID_PIN"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="Marcin_LIB" deviceset="R0805" device="" value="4k7"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="IC1" library="Marcin_LIB" deviceset="NCP1117ST33T3G" device=""/>
<part name="U1" library="Marcin_LIB" deviceset="MCP1642B" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="Marcin_LIB" deviceset="R0805" device="" value="536k"/>
<part name="R5" library="Marcin_LIB" deviceset="R0805" device="" value="309k"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="VDD1" library="supply1" deviceset="VDD" device=""/>
<part name="VDD2" library="supply1" deviceset="VDD" device=""/>
<part name="P+2" library="supply1" deviceset="V+" device=""/>
<part name="P+1" library="supply1" deviceset="V+" device=""/>
<part name="L1" library="Marcin_LIB" deviceset="DLG-0504-4R7" device="" value="4.7uH"/>
<part name="C1" library="Marcin_LIB" deviceset="C0805" device="" technology="_10UF" value="10uF"/>
<part name="C3" library="Marcin_LIB" deviceset="C0805" device="" technology="_10UF" value="10uF"/>
<part name="C4" library="Marcin_LIB" deviceset="C0805" device="" technology="_10UF" value="10uF"/>
<part name="C2" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C5" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="R13" library="Marcin_LIB" deviceset="R0805" device="" value="100R"/>
<part name="R12" library="Marcin_LIB" deviceset="R0805" device="" value="1k"/>
<part name="T1" library="Marcin_LIB" deviceset="PNP" device="" technology="SOT23"/>
<part name="VDD12" library="supply1" deviceset="VDD" device=""/>
<part name="U2" library="Marcin_LIB" deviceset="TL431" device="" technology="ACD" value="TL431"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R6" library="Marcin_LIB" deviceset="R0805" device="" value="62R"/>
<part name="VDD3" library="supply1" deviceset="VDD" device=""/>
<part name="R7" library="Marcin_LIB" deviceset="R0805" device="" value="620R"/>
<part name="R8" library="Marcin_LIB" deviceset="R0805" device="" value="3k3"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="Marcin_LIB" deviceset="C0805" device="" technology="_10UF" value="10uF"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="C14" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="CON1" library="Marcin_LIB" deviceset="T821M114A1S100CEU-B" device="" value="IDC"/>
<part name="VDD4" library="supply1" deviceset="VDD" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="BAT1" library="Marcin_LIB" deviceset="CR2032" device="SMT" value="CR2032"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R22" library="Marcin_LIB" deviceset="R0805" device="" value="10k"/>
<part name="R23" library="Marcin_LIB" deviceset="R0805" device="" value="10k"/>
<part name="VDD16" library="supply1" deviceset="VDD" device=""/>
<part name="VDD17" library="supply1" deviceset="VDD" device=""/>
<part name="D1" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KSKT" value="YELLOW"/>
<part name="R19" library="Marcin_LIB" deviceset="R0805" device="" value="150R"/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="C15" library="Marcin_LIB" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-149.86" y="55.88" size="1.778" layer="97">Inductor
DLG-0504-4R7 from TME
770-0873 from RS</text>
<text x="109.22" y="27.94" size="1.778" layer="97">           GPIO0      GPIO2       GPIO15
NORMAL       1          1           0
PROG         0          1           0</text>
<text x="137.16" y="-5.08" size="1.778" layer="97">Required correction:
1. GPIO0 of ESP8266 to VDD not GND
2. POTs should be connected to VDDA not to VDD
3. Wrong pinout of NCP1117
4. Adjust values of R5 and R4
5. Switch should be connected to PB2(BOOT1) not to PB1 -&gt; Bootloader
6. VDD to GPIO2 of ESP8266 is not connected!
7. R3 must be pull-up to 3.3V and value of 1k5
8. RJ1 must be replaced by 100k resistor
9. BOOT0 must be shorted to the GND
10. Consider replacement by STM32F030C8T6 
11. Place all of the components on the top layer. It will allow quick reflow process. </text>
<text x="63.5" y="-81.28" size="1.778" layer="97">           BOOT1      BOOT0     
NORMAL       x          0          
BOOT         0          1           </text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="12.7" y="-17.78"/>
<instance part="VDD8" gate="G$1" x="-48.26" y="-48.26"/>
<instance part="C9" gate="G$1" x="-48.26" y="-58.42"/>
<instance part="C10" gate="G$1" x="-38.1" y="-66.04"/>
<instance part="C11" gate="G$1" x="-27.94" y="-63.5"/>
<instance part="GND22" gate="1" x="-27.94" y="-73.66"/>
<instance part="GND21" gate="1" x="-38.1" y="-73.66"/>
<instance part="GND20" gate="1" x="-48.26" y="-73.66"/>
<instance part="GND25" gate="1" x="50.8" y="-63.5"/>
<instance part="VDD9" gate="G$1" x="-38.1" y="-53.34"/>
<instance part="VDD10" gate="G$1" x="-27.94" y="-55.88"/>
<instance part="X3" gate="G$1" x="71.12" y="-43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="67.31" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.74" y="-40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND30" gate="1" x="83.82" y="-53.34"/>
<instance part="X2" gate="G$1" x="-40.64" y="-36.83" smashed="yes" rot="R270">
<attribute name="NAME" x="-37.084" y="-34.29" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-36.83" y="-34.29" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND18" gate="1" x="-60.96" y="-40.64"/>
<instance part="C7" gate="G$1" x="-50.8" y="-33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="-56.166" y="-32.379" size="1.778" layer="95"/>
<attribute name="VALUE" x="-50.936" y="-32.339" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="-50.8" y="-43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="-55.816" y="-42.939" size="1.778" layer="95"/>
<attribute name="VALUE" x="-50.536" y="-42.689" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="22.86" y="35.56"/>
<instance part="R9" gate="G$1" x="-5.08" y="73.66" rot="R270"/>
<instance part="R10" gate="G$1" x="-5.08" y="63.5" rot="R270"/>
<instance part="GND23" gate="1" x="-5.08" y="55.88"/>
<instance part="VDD11" gate="G$1" x="2.54" y="76.2"/>
<instance part="GND31" gate="1" x="86.36" y="53.34"/>
<instance part="VDD15" gate="G$1" x="86.36" y="66.04"/>
<instance part="R18" gate="G$1" x="81.28" y="58.42"/>
<instance part="R11" gate="G$1" x="10.16" y="73.66"/>
<instance part="RJ2" gate="1" x="93.98" y="63.5" rot="R90"/>
<instance part="GND33" gate="1" x="93.98" y="58.42"/>
<instance part="VDD13" gate="G$1" x="45.72" y="99.06"/>
<instance part="U5" gate="G$1" x="142.24" y="60.96"/>
<instance part="GND36" gate="1" x="121.92" y="45.72"/>
<instance part="U4" gate="G$1" x="45.72" y="55.88"/>
<instance part="POT3" gate="G$1" x="-43.18" y="-15.24" smashed="yes" rot="MR90">
<attribute name="NAME" x="-45.64" y="-18.489" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-40.09" y="-13.02" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="POT1" gate="G$1" x="-73.66" y="-10.16" smashed="yes" rot="MR90">
<attribute name="NAME" x="-76.5" y="-13.539" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-70.96" y="-7.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="POT2" gate="G$1" x="-58.42" y="-12.7" smashed="yes" rot="MR90">
<attribute name="NAME" x="-61.06" y="-15.849" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-55.45" y="-10.63" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="VDD5" gate="G$1" x="-78.74" y="-7.62" smashed="yes">
<attribute name="VALUE" x="-81.28" y="-10.16" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="VDD6" gate="G$1" x="-63.5" y="-10.16" smashed="yes">
<attribute name="VALUE" x="-66.04" y="-12.7" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="VDD7" gate="G$1" x="-48.26" y="-12.7" smashed="yes">
<attribute name="VALUE" x="-50.8" y="-15.24" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND19" gate="1" x="-53.34" y="-22.86" smashed="yes">
<attribute name="VALUE" x="-55.88" y="-25.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R15" gate="G$1" x="60.96" y="-22.86"/>
<instance part="R16" gate="G$1" x="71.12" y="-25.4"/>
<instance part="R20" gate="G$1" x="81.28" y="-27.94"/>
<instance part="R21" gate="G$1" x="91.44" y="-30.48"/>
<instance part="D5" gate="G$1" x="119.38" y="-35.56" smashed="yes">
<attribute name="NAME" x="121.476" y="-41.782" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="121.805" y="-35.132" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D4" gate="G$1" x="114.3" y="-35.56" smashed="yes">
<attribute name="NAME" x="116.806" y="-41.932" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="116.605" y="-35.012" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D3" gate="G$1" x="109.22" y="-35.56" smashed="yes">
<attribute name="NAME" x="111.246" y="-41.972" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="111.305" y="-34.972" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D2" gate="G$1" x="104.14" y="-35.56" smashed="yes">
<attribute name="NAME" x="106.196" y="-42.112" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="106.375" y="-34.892" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND35" gate="1" x="111.76" y="-48.26"/>
<instance part="SW2" gate="G$1" x="78.74" y="-7.62" smashed="yes" rot="R270">
<attribute name="NAME" x="85.09" y="-5.08" size="1.778" layer="95" rot="R270" display="off"/>
<attribute name="VALUE" x="84.49" y="-4.825" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SW1" gate="G$1" x="76.2" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="82.55" y="20.32" size="1.778" layer="95" rot="R270" display="off"/>
<attribute name="VALUE" x="81.93" y="19.925" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R17" gate="G$1" x="76.2" y="30.48" rot="R90"/>
<instance part="VDD14" gate="G$1" x="76.2" y="38.1" smashed="yes">
<attribute name="VALUE" x="73.66" y="35.56" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND28" gate="1" x="76.2" y="5.08" smashed="yes">
<attribute name="VALUE" x="73.66" y="2.54" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C12" gate="G$1" x="68.58" y="15.24" rot="MR0"/>
<instance part="GND29" gate="1" x="78.74" y="-17.78" smashed="yes">
<attribute name="VALUE" x="76.2" y="-20.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R14" gate="G$1" x="60.96" y="0" rot="R180"/>
<instance part="C13" gate="G$1" x="71.12" y="-10.16" rot="MR0"/>
<instance part="GND26" gate="1" x="68.58" y="5.08" smashed="yes">
<attribute name="VALUE" x="66.04" y="2.54" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND27" gate="1" x="71.12" y="-17.78" smashed="yes">
<attribute name="VALUE" x="68.58" y="-20.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X1" gate="G$1" x="-149.86" y="-27.94" rot="MR0"/>
<instance part="R1" gate="G$1" x="-139.7" y="-25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="-141.478" y="-24.1554" size="1.778" layer="95"/>
<attribute name="VALUE" x="-141.732" y="-28.194" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="-129.54" y="-27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="-131.572" y="-26.6954" size="1.778" layer="95"/>
<attribute name="VALUE" x="-131.572" y="-30.734" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="-142.24" y="-43.18"/>
<instance part="RJ1" gate="1" x="-134.62" y="-38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-136.652" y="-40.132" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-131.064" y="-41.656" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="-134.62" y="-43.18"/>
<instance part="R3" gate="G$1" x="-119.38" y="-35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="-120.6246" y="-37.592" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-116.586" y="-37.592" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND10" gate="1" x="-119.38" y="-43.18"/>
<instance part="IC1" gate="G$1" x="-121.92" y="-7.62"/>
<instance part="U1" gate="G$1" x="-134.62" y="25.4"/>
<instance part="GND8" gate="1" x="-121.92" y="-20.32" smashed="yes">
<attribute name="VALUE" x="-124.46" y="-22.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND9" gate="1" x="-119.38" y="10.16" smashed="yes">
<attribute name="VALUE" x="-121.92" y="7.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND3" gate="1" x="-149.86" y="10.16" smashed="yes">
<attribute name="VALUE" x="-152.4" y="7.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R4" gate="G$1" x="-104.14" y="27.94" rot="R270"/>
<instance part="R5" gate="G$1" x="-104.14" y="17.78" rot="R270"/>
<instance part="GND12" gate="1" x="-104.14" y="10.16" smashed="yes">
<attribute name="VALUE" x="-106.68" y="7.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="VDD1" gate="G$1" x="-99.06" y="38.1" smashed="yes">
<attribute name="VALUE" x="-101.6" y="35.56" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="VDD2" gate="G$1" x="-99.06" y="-2.54" smashed="yes">
<attribute name="VALUE" x="-101.6" y="-5.08" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="P+2" gate="1" x="-40.64" y="83.82"/>
<instance part="P+1" gate="1" x="-167.64" y="38.1"/>
<instance part="L1" gate="G$1" x="-142.24" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="-144.8" y="42.56" size="2.1844" layer="97"/>
<attribute name="VALUE" x="-147.54" y="47.94" size="2.1844" layer="97"/>
</instance>
<instance part="C1" gate="G$1" x="-167.64" y="25.4"/>
<instance part="C3" gate="G$1" x="-137.16" y="-12.7"/>
<instance part="C4" gate="G$1" x="-106.68" y="-15.24"/>
<instance part="C2" gate="G$1" x="-160.02" y="25.4"/>
<instance part="C5" gate="G$1" x="-99.06" y="-15.24"/>
<instance part="GND1" gate="1" x="-167.64" y="20.32" smashed="yes">
<attribute name="VALUE" x="-170.18" y="17.78" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND2" gate="1" x="-160.02" y="20.32" smashed="yes">
<attribute name="VALUE" x="-162.56" y="17.78" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND11" gate="1" x="-106.68" y="-20.32" smashed="yes">
<attribute name="VALUE" x="-109.22" y="-22.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND13" gate="1" x="-99.06" y="-20.32" smashed="yes">
<attribute name="VALUE" x="-101.6" y="-22.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND6" gate="1" x="-137.16" y="-20.32" smashed="yes">
<attribute name="VALUE" x="-139.7" y="-22.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R13" gate="G$1" x="35.56" y="91.44"/>
<instance part="R12" gate="G$1" x="27.94" y="96.52" rot="R90"/>
<instance part="T1" gate="G$1" x="45.72" y="91.44"/>
<instance part="VDD12" gate="G$1" x="27.94" y="104.14"/>
<instance part="U2" gate="G$1" x="-91.44" y="-66.04" rot="MR90"/>
<instance part="GND14" gate="1" x="-91.44" y="-78.74"/>
<instance part="R6" gate="G$1" x="-91.44" y="-45.72" rot="R270"/>
<instance part="VDD3" gate="G$1" x="-91.44" y="-38.1"/>
<instance part="R7" gate="G$1" x="-78.74" y="-58.42" rot="R270"/>
<instance part="R8" gate="G$1" x="-78.74" y="-68.58" rot="R270"/>
<instance part="GND16" gate="1" x="-78.74" y="-78.74"/>
<instance part="C6" gate="G$1" x="-73.66" y="-66.04"/>
<instance part="GND17" gate="1" x="-73.66" y="-78.74"/>
<instance part="C14" gate="G$1" x="104.14" y="81.28" rot="MR0"/>
<instance part="GND34" gate="1" x="104.14" y="76.2"/>
<instance part="CON1" gate="G$1" x="-71.12" y="58.42"/>
<instance part="VDD4" gate="G$1" x="-81.28" y="71.12" smashed="yes">
<attribute name="VALUE" x="-83.82" y="68.58" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND15" gate="1" x="-81.28" y="45.72" smashed="yes">
<attribute name="VALUE" x="-83.82" y="43.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="BAT1" gate="G$1" x="-139.7" y="-66.04"/>
<instance part="GND5" gate="1" x="-139.7" y="-78.74"/>
<instance part="R22" gate="G$1" x="109.22" y="-2.54" rot="R270"/>
<instance part="R23" gate="G$1" x="116.84" y="-5.08" rot="R270"/>
<instance part="VDD16" gate="G$1" x="109.22" y="5.08" smashed="yes">
<attribute name="VALUE" x="106.68" y="2.54" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="VDD17" gate="G$1" x="116.84" y="5.08" smashed="yes">
<attribute name="VALUE" x="114.3" y="2.54" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D1" gate="G$1" x="88.9" y="40.64" smashed="yes">
<attribute name="NAME" x="90.956" y="34.088" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="91.135" y="41.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="81.28" y="45.72"/>
<instance part="GND32" gate="1" x="88.9" y="33.02"/>
<instance part="C15" gate="G$1" x="111.76" y="81.28" rot="MR0"/>
<instance part="GND37" gate="1" x="111.76" y="76.2"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="-48.26" y1="-60.96" x2="-48.26" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="-38.1" y1="-68.58" x2="-38.1" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="-66.04" x2="-27.94" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VSS_1"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="40.64" y1="-50.8" x2="50.8" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-50.8" x2="50.8" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS_2"/>
<wire x1="50.8" y1="-53.34" x2="50.8" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-55.88" x2="50.8" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-58.42" x2="50.8" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-53.34" x2="50.8" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSSA"/>
<wire x1="40.64" y1="-58.42" x2="50.8" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS_3"/>
<wire x1="40.64" y1="-55.88" x2="50.8" y2="-55.88" width="0.1524" layer="91"/>
<junction x="50.8" y="-53.34"/>
<junction x="50.8" y="-55.88"/>
<junction x="50.8" y="-58.42"/>
</segment>
<segment>
<pinref part="X3" gate="G$1" pin="2"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="78.74" y1="-43.18" x2="83.82" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-43.18" x2="83.82" y2="-50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="-33.02" x2="-55.88" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="-55.88" y1="-38.1" x2="-55.88" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-38.1" x2="-55.88" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-55.88" y="-38.1"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="86.36" y1="58.42" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RJ2" gate="1" pin="1"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="22.86" y1="38.1" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="22.86" y1="43.18" x2="27.94" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="121.92" y1="48.26" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="53.34" x2="124.46" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="E"/>
<wire x1="-68.58" y1="-10.16" x2="-68.58" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="POT3" gate="G$1" pin="E"/>
<wire x1="-68.58" y1="-20.32" x2="-53.34" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-20.32" x2="-38.1" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-20.32" x2="-38.1" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="POT2" gate="G$1" pin="E"/>
<wire x1="-53.34" y1="-12.7" x2="-53.34" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="GND19" gate="1" pin="GND"/>
<junction x="-53.34" y="-20.32"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="119.38" y1="-40.64" x2="119.38" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="119.38" y1="-43.18" x2="114.3" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-43.18" x2="111.76" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-43.18" x2="109.22" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-43.18" x2="104.14" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-43.18" x2="104.14" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="109.22" y1="-40.64" x2="109.22" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="114.3" y1="-40.64" x2="114.3" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="111.76" y1="-45.72" x2="111.76" y2="-43.18" width="0.1524" layer="91"/>
<junction x="109.22" y="-43.18"/>
<junction x="111.76" y="-43.18"/>
<junction x="114.3" y="-43.18"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="T4"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="76.2" y1="7.62" x2="76.2" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="T3"/>
<wire x1="76.2" y1="10.16" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="73.66" y1="12.7" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<wire x1="73.66" y1="10.16" x2="76.2" y2="10.16" width="0.1524" layer="91"/>
<junction x="76.2" y="10.16"/>
</segment>
<segment>
<pinref part="SW2" gate="G$1" pin="T4"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="SW2" gate="G$1" pin="T3"/>
<wire x1="78.74" y1="-12.7" x2="78.74" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-12.7" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<junction x="78.74" y="-12.7"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="71.12" y1="-15.24" x2="71.12" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="68.58" y1="7.62" x2="68.58" y2="12.7" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="5"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-144.78" y1="-33.02" x2="-142.24" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-33.02" x2="-142.24" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RJ1" gate="1" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="PGND"/>
<wire x1="-149.86" y1="12.7" x2="-149.86" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="SGND"/>
<wire x1="-119.38" y1="12.7" x2="-119.38" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-137.16" y1="-17.78" x2="-137.16" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="A"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="-91.44" y1="-76.2" x2="-91.44" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="-76.2" x2="-78.74" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="-73.66" y1="-68.58" x2="-73.66" y2="-76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="-78.74" y1="50.8" x2="-81.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="50.8" x2="-81.28" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="BAT1" gate="G$1" pin="-"/>
<wire x1="-139.7" y1="-76.2" x2="-139.7" y2="-71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VDD_1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="VDD8" gate="G$1" pin="VDD"/>
<wire x1="-12.7" y1="-53.34" x2="-48.26" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-53.34" x2="-48.26" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-48.26" y="-53.34"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDD_3"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-55.88" x2="-38.1" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-55.88" x2="-38.1" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="VDD9" gate="G$1" pin="VDD"/>
<junction x="-38.1" y="-55.88"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDD_2"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-58.42" x2="-27.94" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="VDD10" gate="G$1" pin="VDD"/>
<junction x="-27.94" y="-58.42"/>
</segment>
<segment>
<pinref part="VDD11" gate="G$1" pin="VDD"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="2.54" y1="73.66" x2="5.08" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="A"/>
<pinref part="VDD5" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="POT2" gate="G$1" pin="A"/>
<pinref part="VDD6" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="POT3" gate="G$1" pin="A"/>
<pinref part="VDD7" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="VDD14" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-119.38" y1="33.02" x2="-104.14" y2="33.02" width="0.1524" layer="91"/>
<pinref part="VDD1" gate="G$1" pin="VDD"/>
<wire x1="-104.14" y1="33.02" x2="-99.06" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="33.02" x2="-99.06" y2="35.56" width="0.1524" layer="91"/>
<junction x="-104.14" y="33.02"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="VDD2" gate="G$1" pin="VDD"/>
<wire x1="-99.06" y1="-5.08" x2="-99.06" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="-99.06" y1="-7.62" x2="-99.06" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="-7.62" x2="-106.68" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-106.68" y1="-7.62" x2="-99.06" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-10.16" x2="-106.68" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-106.68" y="-7.62"/>
<junction x="-99.06" y="-7.62"/>
</segment>
<segment>
<pinref part="VDD13" gate="G$1" pin="VDD"/>
<pinref part="T1" gate="G$1" pin="E"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="VDD12" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="VDD3" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="14"/>
<pinref part="VDD4" gate="G$1" pin="VDD"/>
<wire x1="-78.74" y1="66.04" x2="-81.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="66.04" x2="-81.28" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="VDD16" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="VDD17" gate="G$1" pin="VDD"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="116.84" y1="2.54" x2="116.84" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SWCLK"/>
<wire x1="40.64" y1="10.16" x2="53.34" y2="10.16" width="0.1524" layer="91"/>
<label x="43.18" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="11"/>
<wire x1="-63.5" y1="63.5" x2="-53.34" y2="63.5" width="0.1524" layer="91"/>
<label x="-60.96" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWIO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SWIO"/>
<wire x1="40.64" y1="7.62" x2="53.34" y2="7.62" width="0.1524" layer="91"/>
<label x="43.18" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="9"/>
<wire x1="-63.5" y1="60.96" x2="-53.34" y2="60.96" width="0.1524" layer="91"/>
<label x="-60.96" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1_RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA10"/>
<wire x1="-12.7" y1="-22.86" x2="-33.02" y2="-22.86" width="0.1524" layer="91"/>
<label x="-33.02" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="81.28" y1="78.74" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<label x="63.5" y="78.74" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="TXD"/>
</segment>
<segment>
<wire x1="175.26" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="TXD"/>
<label x="160.02" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="10"/>
<wire x1="-78.74" y1="60.96" x2="-93.98" y2="60.96" width="0.1524" layer="91"/>
<label x="-93.98" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1_TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA9"/>
<wire x1="-12.7" y1="-20.32" x2="-33.02" y2="-20.32" width="0.1524" layer="91"/>
<label x="-33.02" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="81.28" y1="73.66" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<label x="63.5" y="73.66" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="RXD"/>
</segment>
<segment>
<wire x1="175.26" y1="63.5" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="RXD"/>
<label x="160.02" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="12"/>
<wire x1="-78.74" y1="63.5" x2="-93.98" y2="63.5" width="0.1524" layer="91"/>
<label x="-93.98" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_SENS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB15"/>
<wire x1="40.64" y1="-35.56" x2="55.88" y2="-35.56" width="0.1524" layer="91"/>
<label x="43.18" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_WIFI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB13"/>
<label x="43.18" y="-30.48" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-30.48" x2="86.36" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_RED" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB12"/>
<label x="43.18" y="-27.94" size="1.778" layer="95"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-27.94" x2="76.2" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_GRE" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB11"/>
<wire x1="40.64" y1="-25.4" x2="66.04" y2="-25.4" width="0.1524" layer="91"/>
<label x="43.18" y="-25.4" size="1.778" layer="95"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="LED_ORA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB10"/>
<wire x1="40.64" y1="-22.86" x2="55.88" y2="-22.86" width="0.1524" layer="91"/>
<label x="43.18" y="-22.86" size="1.778" layer="95"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BUZZ" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB0"/>
<wire x1="40.64" y1="2.54" x2="55.88" y2="2.54" width="0.1524" layer="91"/>
<label x="43.18" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="BUT1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB1"/>
<wire x1="40.64" y1="0" x2="55.88" y2="0" width="0.1524" layer="91"/>
<label x="43.18" y="0" size="1.778" layer="95"/>
<pinref part="R14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USART2_TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA2"/>
<wire x1="-12.7" y1="-2.54" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<label x="-30.48" y="-2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="8"/>
<wire x1="-78.74" y1="58.42" x2="-93.98" y2="58.42" width="0.1524" layer="91"/>
<label x="-93.98" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART2_RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA3"/>
<wire x1="-12.7" y1="-5.08" x2="-30.48" y2="-5.08" width="0.1524" layer="91"/>
<label x="-30.48" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="6"/>
<wire x1="-78.74" y1="55.88" x2="-93.98" y2="55.88" width="0.1524" layer="91"/>
<label x="-93.98" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC_V_VDD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA1"/>
<wire x1="-12.7" y1="0" x2="-30.48" y2="0" width="0.1524" layer="91"/>
<label x="-30.48" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC_V_BLOCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA4"/>
<label x="-30.48" y="-7.62" size="1.778" layer="95"/>
<pinref part="POT1" gate="G$1" pin="S"/>
<wire x1="-12.7" y1="-7.62" x2="-73.66" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC_V_TENS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA5"/>
<label x="-30.48" y="-10.16" size="1.778" layer="95"/>
<pinref part="POT2" gate="G$1" pin="S"/>
<wire x1="-12.7" y1="-10.16" x2="-58.42" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC_V_ENTITY" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA6"/>
<wire x1="-12.7" y1="-12.7" x2="-43.18" y2="-12.7" width="0.1524" layer="91"/>
<label x="-30.48" y="-12.7" size="1.778" layer="95"/>
<pinref part="POT3" gate="G$1" pin="S"/>
</segment>
</net>
<net name="SPI1_NSS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA15"/>
<wire x1="-12.7" y1="-30.48" x2="-30.48" y2="-30.48" width="0.1524" layer="91"/>
<label x="-30.48" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DP" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA12"/>
<label x="-30.48" y="-27.94" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="-27.94" x2="-119.38" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-119.38" y1="-30.48" x2="-119.38" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-119.38" y="-27.94"/>
<wire x1="-119.38" y1="-27.94" x2="-12.7" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_DM" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA11"/>
<label x="-30.48" y="-25.4" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="-25.4" x2="-134.62" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB3"/>
<wire x1="40.64" y1="-5.08" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<label x="43.18" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="50.8" x2="-53.34" y2="50.8" width="0.1524" layer="91"/>
<label x="-60.96" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MISO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB4"/>
<wire x1="40.64" y1="-7.62" x2="55.88" y2="-7.62" width="0.1524" layer="91"/>
<label x="43.18" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="4"/>
<wire x1="-78.74" y1="53.34" x2="-93.98" y2="53.34" width="0.1524" layer="91"/>
<label x="-93.98" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB5"/>
<wire x1="40.64" y1="-10.16" x2="55.88" y2="-10.16" width="0.1524" layer="91"/>
<label x="43.18" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="3"/>
<wire x1="-63.5" y1="53.34" x2="-53.34" y2="53.34" width="0.1524" layer="91"/>
<label x="-60.96" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB7"/>
<wire x1="40.64" y1="-15.24" x2="55.88" y2="-15.24" width="0.1524" layer="91"/>
<label x="43.18" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="93.98" y1="-10.16" x2="116.84" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="116.84" y1="-10.16" x2="119.38" y2="-10.16" width="0.1524" layer="91"/>
<junction x="116.84" y="-10.16"/>
<label x="93.98" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="5"/>
<wire x1="-63.5" y1="55.88" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<label x="-60.96" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="OSC32_IN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC14-OSC32_IN"/>
<wire x1="-12.7" y1="-38.1" x2="-33.02" y2="-38.1" width="0.1524" layer="91"/>
<label x="-30.48" y="-38.1" size="1.778" layer="95"/>
<wire x1="-33.02" y1="-33.02" x2="-33.02" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-48.26" y1="-33.02" x2="-40.64" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="-33.02" x2="-33.02" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-34.29" x2="-40.64" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-40.64" y="-33.02"/>
</segment>
</net>
<net name="OSC32_OUT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC15-OSC32-OUT"/>
<wire x1="-12.7" y1="-40.64" x2="-33.02" y2="-40.64" width="0.1524" layer="91"/>
<label x="-30.48" y="-40.64" size="1.778" layer="95"/>
<wire x1="-33.02" y1="-40.64" x2="-33.02" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-48.26" y1="-43.18" x2="-40.64" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="4"/>
<wire x1="-40.64" y1="-43.18" x2="-33.02" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-41.91" x2="-40.64" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-40.64" y="-43.18"/>
</segment>
</net>
<net name="OSC_IN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PD0-OSC_IN"/>
<wire x1="40.64" y1="-40.64" x2="60.96" y2="-40.64" width="0.1524" layer="91"/>
<label x="43.18" y="-40.64" size="1.778" layer="95"/>
<wire x1="60.96" y1="-40.64" x2="60.96" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="3"/>
<wire x1="60.96" y1="-35.56" x2="71.12" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSC_OUT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PD1-OSC_OUT"/>
<wire x1="40.64" y1="-43.18" x2="60.96" y2="-43.18" width="0.1524" layer="91"/>
<label x="43.18" y="-43.18" size="1.778" layer="95"/>
<pinref part="X3" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-50.8" x2="60.96" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-50.8" x2="60.96" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ESP_ADC" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<junction x="-5.08" y="68.58"/>
<wire x1="-5.08" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<label x="15.24" y="68.58" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="ADC"/>
</segment>
</net>
<net name="LINK_LED" class="0">
<segment>
<wire x1="76.2" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<label x="63.5" y="45.72" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="GPIO12"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ESP_GPIO0" class="0">
<segment>
<wire x1="63.5" y1="68.58" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
<pinref part="RJ2" gate="1" pin="2"/>
<wire x1="93.98" y1="68.58" x2="93.98" y2="66.04" width="0.1524" layer="91"/>
<label x="63.5" y="68.58" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="GPIO0"/>
</segment>
<segment>
<wire x1="160.02" y1="58.42" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GPIO0"/>
<label x="160.02" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB14"/>
<wire x1="40.64" y1="-33.02" x2="58.42" y2="-33.02" width="0.1524" layer="91"/>
<label x="43.18" y="-33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ESP_CHEN" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="15.24" y1="73.66" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
<label x="15.24" y="73.66" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="CH_PD"/>
</segment>
<segment>
<wire x1="111.76" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="CH_PD"/>
<label x="111.76" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="ESP_GPIO15" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="76.2" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<label x="63.5" y="58.42" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="GPIO15"/>
</segment>
</net>
<net name="ESP_RST" class="0">
<segment>
<wire x1="15.24" y1="78.74" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<label x="15.24" y="78.74" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="RST"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB9"/>
<wire x1="40.64" y1="-20.32" x2="58.42" y2="-20.32" width="0.1524" layer="91"/>
<label x="43.18" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="111.76" y1="68.58" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="RST"/>
<label x="111.76" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="ESP_GPIO2" class="0">
<segment>
<pinref part="VDD15" gate="G$1" pin="VDD"/>
<wire x1="86.36" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="63.5" y="63.5" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="GPIO2"/>
</segment>
<segment>
<wire x1="175.26" y1="53.34" x2="160.02" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GPIO2"/>
<label x="160.02" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-22.86" x2="119.38" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-22.86" x2="119.38" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-25.4" x2="114.3" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-25.4" x2="114.3" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-27.94" x2="109.22" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-27.94" x2="109.22" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-30.48" x2="104.14" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-30.48" x2="104.14" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="66.04" y1="0" x2="71.12" y2="0" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-5.08" x2="71.12" y2="0" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="T2"/>
<wire x1="71.12" y1="0" x2="76.2" y2="0" width="0.1524" layer="91"/>
<wire x1="76.2" y1="0" x2="78.74" y2="0" width="0.1524" layer="91"/>
<wire x1="78.74" y1="0" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="T1"/>
<wire x1="76.2" y1="-2.54" x2="76.2" y2="0" width="0.1524" layer="91"/>
<junction x="71.12" y="0"/>
<junction x="76.2" y="0"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="3"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-144.78" y1="-27.94" x2="-134.62" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="4"/>
<wire x1="-144.78" y1="-30.48" x2="-134.62" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="-30.48" x2="-134.62" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="RJ1" gate="1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="22.86" x2="-111.76" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="22.86" x2="-111.76" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VFB"/>
<wire x1="-111.76" y1="25.4" x2="-119.38" y2="25.4" width="0.1524" layer="91"/>
<junction x="-104.14" y="22.86"/>
</segment>
</net>
<net name="V+" class="0">
<segment>
<label x="-22.86" y="78.74" size="1.778" layer="95"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="78.74" x2="-40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="78.74" x2="-40.64" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PA0-WKUP"/>
<wire x1="-12.7" y1="2.54" x2="-40.64" y2="2.54" width="0.1524" layer="91"/>
<label x="-33.02" y="2.54" size="1.778" layer="95"/>
<pinref part="P+2" gate="1" pin="V+"/>
<wire x1="-40.64" y1="78.74" x2="-40.64" y2="81.28" width="0.1524" layer="91"/>
<junction x="-40.64" y="78.74"/>
</segment>
<segment>
<pinref part="P+1" gate="1" pin="V+"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-167.64" y1="35.56" x2="-167.64" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="-167.64" y1="33.02" x2="-167.64" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="33.02" x2="-154.94" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="-149.86" y1="25.4" x2="-154.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="25.4" x2="-154.94" y2="33.02" width="0.1524" layer="91"/>
<junction x="-154.94" y="33.02"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-149.86" y1="45.72" x2="-154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="45.72" x2="-154.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="33.02" x2="-160.02" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-160.02" y1="33.02" x2="-167.64" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="30.48" x2="-160.02" y2="33.02" width="0.1524" layer="91"/>
<junction x="-167.64" y="33.02"/>
<junction x="-160.02" y="33.02"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="-142.24" y1="-22.86" x2="-144.78" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-22.86" x2="-142.24" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="-142.24" y1="-7.62" x2="-137.16" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-137.16" y1="-7.62" x2="-132.08" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-137.16" y="-7.62"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="SW"/>
<wire x1="-137.16" y1="45.72" x2="-134.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="45.72" x2="-134.62" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VCC"/>
<pinref part="U5" gate="G$1" pin="VCC"/>
<wire x1="45.72" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<wire x1="104.14" y1="86.36" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<wire x1="111.76" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="142.24" y1="86.36" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="C"/>
<junction x="45.72" y="86.36"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="104.14" y="86.36"/>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="111.76" y="86.36"/>
</segment>
</net>
<net name="ESP_POWER" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA7"/>
<wire x1="-12.7" y1="-15.24" x2="-30.48" y2="-15.24" width="0.1524" layer="91"/>
<label x="-30.48" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="30.48" y1="91.44" x2="27.94" y2="91.44" width="0.1524" layer="91"/>
<label x="12.7" y="91.44" size="1.778" layer="95"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="27.94" y1="91.44" x2="12.7" y2="91.44" width="0.1524" layer="91"/>
<junction x="27.94" y="91.44"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="T1" gate="G$1" pin="B"/>
</segment>
</net>
<net name="VDDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="C"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="-63.5" x2="-91.44" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-91.44" y1="-53.34" x2="-91.44" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="-53.34" x2="-91.44" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="-53.34" x2="-73.66" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-53.34" x2="-73.66" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-91.44" y="-53.34"/>
<junction x="-78.74" y="-53.34"/>
<wire x1="-73.66" y1="-53.34" x2="-63.5" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-53.34" x2="-63.5" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDDA"/>
<wire x1="-17.78" y1="-60.96" x2="-12.7" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-78.74" x2="-17.78" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-78.74" x2="-17.78" y2="-60.96" width="0.1524" layer="91"/>
<label x="-71.12" y="-53.34" size="1.778" layer="95"/>
<junction x="-73.66" y="-53.34"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="R"/>
<wire x1="-78.74" y1="-63.5" x2="-86.36" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-78.74" y="-63.5"/>
</segment>
</net>
<net name="BAT_RTC" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VBAT"/>
<wire x1="-12.7" y1="-50.8" x2="-27.94" y2="-50.8" width="0.1524" layer="91"/>
<label x="-27.94" y="-50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<wire x1="-142.24" y1="-63.5" x2="-142.24" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="-58.42" x2="-137.16" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="BAT1" gate="G$1" pin="+1"/>
<wire x1="-137.16" y1="-58.42" x2="-124.46" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-63.5" x2="-137.16" y2="-58.42" width="0.1524" layer="91"/>
<junction x="-137.16" y="-58.42"/>
<label x="-134.62" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="!NRST!" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="13"/>
<wire x1="-63.5" y1="66.04" x2="-53.34" y2="66.04" width="0.1524" layer="91"/>
<label x="-60.96" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<label x="43.18" y="12.7" size="1.778" layer="95"/>
<pinref part="SW1" gate="G$1" pin="T2"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="76.2" y1="25.4" x2="76.2" y2="22.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<wire x1="68.58" y1="25.4" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="55.88" y1="25.4" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="NRST"/>
<wire x1="55.88" y1="12.7" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="68.58" y1="20.32" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="T1"/>
<wire x1="73.66" y1="22.86" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<junction x="68.58" y="25.4"/>
<junction x="73.66" y="25.4"/>
<junction x="76.2" y="25.4"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB6"/>
<wire x1="40.64" y1="-12.7" x2="55.88" y2="-12.7" width="0.1524" layer="91"/>
<label x="43.18" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="93.98" y1="-7.62" x2="109.22" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-7.62" x2="111.76" y2="-7.62" width="0.1524" layer="91"/>
<junction x="109.22" y="-7.62"/>
<label x="93.98" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="7"/>
<wire x1="-63.5" y1="58.42" x2="-53.34" y2="58.42" width="0.1524" layer="91"/>
<label x="-60.96" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="86.36" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
